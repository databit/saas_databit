# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import itertools
from lxml import etree
from openerp.osv import fields, osv
from openerp.exceptions import except_orm, Warning, RedirectWarning
import openerp.addons.decimal_precision as dp

class res_partner(osv.osv):
    _inherit = 'res.partner'
    _description = 'res partner'
    
    _columns = {
        'npwp'          : fields.char('NPWP', size=20),
        'pkp'           : fields.boolean("PKP"),
        'kawasan'       : fields.selection([('yes','YES'),('no','NO')], 'Kawasan', help=''),
        'kode_transaksi': fields.selection([('010','010 Normal'),('020','020 Bendaharawan (Tdk Terlampir)'),('030','030 Bendaharawan (Terlampir)'),('080','080 Tanpa PPN')], 'No. Seri Faktur'),
#         'kode_status': fields.char('kode status', size=1),
        'rt_rw'         : fields.char('RT/RW', size=8),
        'kabupaten_id'  : fields.many2one('res.kabupaten',"Kabupaten"),
        'kecamatan_id'  : fields.many2one('res.kecamatan',"Kecamatan"),
        'code'          : fields.char('Code Customer',size=16),
        'birthday'      : fields.datetime('Birthday'),
        'akta_file'     : fields.binary('Akta Perusahaan'),
        'siup_file'     : fields.binary('SIUP'),
        'tdp_file'      : fields.binary('TDP'),
        'legal_ids'     : fields.one2many('company.legal','partner_id', "Legal"),
        
    }
    
    _defaults = {
        'npwp': '00.000.000.0-000.000',
        'kode_transaksi': '010',
        'code'       : '/',
        'user_id'    : lambda self, cr, uid, context: uid,
    }
    
    
    def create(self, cr, uid, vals, context=None):
        #if vals.get('is_company',True)==True :
           
        if vals.get('parent_id',False)==False :
            if vals.get('is_company',True)==True :  
               print ">>>>>>>>>>>> kesini"  
               vals['code'] = self.pool.get('ir.sequence').get(cr, uid,'res.partner') or '/'
            else : 
               vals['code'] = self.pool.get('ir.sequence').get(cr, uid,'res.partner') or '/'
        else :
            
               print ">>>>>>>>>>>> kesono"    
               parent_code = self.pool.get('res.partner').browse(cr,uid,vals['parent_id'],context=context).code
               vals['code'] = parent_code
        order =  super(res_partner, self).create(cr, uid, vals, context=context)
        return order
    
    
    def onchange_kabupaten_id(self, cr, uid, ids, kabupaten_id, context=None):
        state_id = country_id = False
        if not kabupaten_id:
            return {'value': {'state_id': False, 'country_id': False}}
        kabupaten_obj = self.pool.get('res.kabupaten').browse(cr, uid, kabupaten_id, context=context)
        if kabupaten_obj.state_id:
            state_id = kabupaten_obj.state_id.id
        if kabupaten_obj.state_id and kabupaten_obj.state_id.country_id:
            country_id = kabupaten_obj.state_id.country_id.id
        return {'value': {'state_id': state_id, 'country_id': country_id}}
    
    def onchange_kecamatan_id(self, cr, uid, ids, kecamatan_id, context=None):
        kab_id = state_id = country_id = False
        if not kecamatan_id:
            return {'value': {'kabupaten_id': False, 'state_id': False, 'country_id': False}}
        kecamatan_obj = self.pool.get('res.kecamatan').browse(cr, uid, kecamatan_id, context=context)
        if kecamatan_obj.kabupaten_id:
            kab_id = kecamatan_obj.kabupaten_id.id
        if kecamatan_obj.kabupaten_id and kecamatan_obj.kabupaten_id.state_id:
            state_id = kecamatan_obj.kabupaten_id.state_id.id
        if kecamatan_obj.kabupaten_id and kecamatan_obj.kabupaten_id.state_id and kecamatan_obj.kabupaten_id.state_id.country_id:
            country_id = kecamatan_obj.kabupaten_id.state_id.country_id.id
        return {'value': {'kabupaten_id': kab_id, 'state_id': state_id, 'country_id': country_id}}
    
    def onchange_npwp(self, cr, uid, ids, npwp, context=None):
        res = {}
        vals = {}
        if npwp == False:
            return res
        elif len(npwp)==20:
            return {"value":npwp}
        elif len(npwp)==15:
            formatted_npwp = npwp[:2]+'.'+npwp[2:5]+'.'+npwp[5:8]+'.'+npwp[8:9]+'-'+npwp[9:12]+'.'+npwp[12:15]
            vals = {"npwp" : formatted_npwp}
            return {"value":vals}
        else:
            warning = {
                'title': _('Warning'),
                'message': _('Wrong Format must 15 digit'),
            }
            return {'warning': warning, 'value' : {'npwp' : False}}
        return res
 
class company_legal(osv.osv):
    _name = "company.legal"
    _columns = {

            'number' : fields.char('Number', size=24),
            'name'              : fields.char('Description', size=64),
            'date_release'                 : fields.date('Date Release'),
            'date_end'      : fields.date('Valid Until'),
            'file'                   : fields.binary('Attachment'),
            'note'                  : fields.char('Notes'),
            'partner_id'    : fields.many2one('res.partner','partner'),
            'lembaga_penerbit' : fields.char('Lembaga Penerbit'),
    }
company_legal()   
    


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
