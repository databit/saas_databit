{
    "name"          : "Databit Partner & Company",
    "version"       : "1.0",
    "depends"       : ['base','dos_base'],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add standart partner & company for databit
                        * NPWP""",
    "website"       : "https://www.databit.co.id/",
    'category'      : 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                       'security/ir.model.access.csv',
                       'partner_sequence.xml',
                       "company_view.xml",
                       "partner_view.xml",
                       ],
    'installable': True,
    'auto_install': True,
}