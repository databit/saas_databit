# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#     Copyright (C) 2012 Cubic ERP - Teradata SAC (<http://cubicerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from mx import DateTime
from lxml import etree

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import time
from openerp import pooler
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
import openerp.addons.decimal_precision as dp
from openerp import netsvc

from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import api

class account_transfer(osv.osv):

    def _get_balance(self, src_journal, dst_journal, company):
        src_balance = dst_balance = 0.0
        #import pdb; pdb.set_trace()
        if src_journal.default_credit_account_id.id == src_journal.default_debit_account_id.id:
            if not src_journal.currency or company.currency_id.id == src_journal.currency.id:
                src_balance = src_journal.default_credit_account_id.balance
            else:
                src_balance = src_journal.default_credit_account_id.foreign_balance
        else:
            if not src_journal.currency or company.currency_id.id == src_journal.currency.id:
                src_balance = src_journal.default_debit_account_id.balance - src_journal.default_credit_account_id.balance
            else:
                src_balance = src_journal.default_debit_account_id.foreign_balance - src_journal.default_credit_account_id.foreign_balance
        if dst_journal.default_credit_account_id.id == dst_journal.default_debit_account_id.id:
            if not dst_journal.currency or company.currency_id.id == dst_journal.currency.id:
                dst_balance = dst_journal.default_credit_account_id.balance
            else:
                dst_balance = dst_journal.default_credit_account_id.foreign_balance
        else:
            if not dst_journal.currency or company.currency_id.id == dst_journal.currency.id:
                dst_balance = dst_journal.default_debit_account_id.balance - dst_journal.default_credit_account_id.balance
            else:
                dst_balance = dst_journal.default_debit_account_id.foreign_balance - dst_journal.default_credit_account_id.foreign_balance
        
        return (src_balance, dst_balance)

    def _balance(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for trans in self.browse(cr, uid, ids, context=context):
            src_balance, dst_balance = self._get_balance(trans.src_journal_id,trans.dst_journal_id,trans.company_id)
            exchange = False
            if trans.dst_journal_id.currency.id != trans.src_journal_id.currency.id:
                exchange = True
            res[trans.id] = {
                    'src_balance':src_balance,
                    'dst_balance':dst_balance,
                    'exchange':exchange,
                    'exchange_inv': (trans.exchange_rate and 1.0 / trans.exchange_rate or 0.0)
                }
        return res

    STATE_SELECTION = [
        ('draft','Ready to Transfer'),
        ('confirm','Transfered'),
        ('done','Receipt'),
        ('cancel','Cancel'),
    ]

    _columns = {
            'company_id' : fields.many2one('res.company','Company', required=True, readonly=True, states={'draft':[('readonly',False)]}),
            'name': fields.char('Number', size=32, required=True, readonly=True, states={'draft':[('readonly',False)]}),
            
            'date': fields.date('Date', required=True, readonly=True, states={'draft':[('readonly',False)]}),
            'date_receive': fields.date('Date Receive'),
            'origin': fields.char('Origin', size=128, readonly=True, states={'draft':[('readonly',False)]},help="Origin Document"),
            'account_analytic_id': fields.many2one('account.analytic.account', 'Analytic Account', readonly=True, states={'draft':[('readonly',False)]}),
            #'voucher_ids': fields.one2many('account.voucher','transfer_id', string='Payments', readonly=True, states={'draft':[('readonly',False)], 'confirm':[('readonly',False)]}),
            'src_journal_id': fields.many2one('account.journal','Source Journal',required=True, domain=[('type','in',['cash','bank'])], select=True, readonly=True, states={'draft':[('readonly',False)]}),
            'src_partner_id': fields.many2one('res.partner','Partner Name', select=True, readonly=True, states={'draft':[('readonly',False)]}),
            'src_balance': fields.function(_balance, digits_compute=dp.get_precision('Account'), string='Current Source Balance', type='float', readonly=True, multi='balance', help="Include all account moves in draft and confirmed state"),
            'src_amount': fields.float('Source Amount',required=True, readonly=True, states={'draft':[('readonly',False)]}),
            'src_have_partner': fields.related('src_journal_id','have_partner',type='boolean',string='Have Partner',readonly=True),
            'dst_journal_id': fields.many2one('account.journal','Destination Journal',required=True, domain=[('type','in',['cash','bank'])], select=True, readonly=True, states={'draft':[('readonly',False)]}),
            'dst_partner_id': fields.many2one('res.partner','Destination Partner', select=True),
            'dst_balance': fields.function(_balance, digits_compute=dp.get_precision('Account'), string='Current Destinity Balance', type='float', readonly=True, multi='balance', help="Include all account moves in draft and confirmed state"),
            'dst_amount': fields.float('Destination Amount',required=True, readonly=True, states={'draft':[('readonly',False)]}),
            'dst_have_partner': fields.related('dst_journal_id','have_partner',type='boolean',string='Have Partner',readonly=True),
            'exchange_src_rate' : fields.float('Exchange Rate', digits_compute=dp.get_precision('Exchange'), readonly=True, states={'draft':[('readonly',False)]}),
            'exchange_rate'     : fields.float('Exchange Rate', digits_compute=dp.get_precision('Exchange'), readonly=True, states={'draft':[('readonly',False)]}),
            'exchange': fields.function(_balance, string='Have Exchange', type='boolean', readonly=True, multi='balance'),
            'exchange_inv': fields.function(_balance, string='1 / Exchange Rate', type='float', digits_compute=dp.get_precision('Exchange'), readonly=True, multi='balance'),
            'adjust_move': fields.many2one('account.move','Adjust Move', readonly=True, help="Adjust move usually by difference in the money exchange"),
            'state': fields.selection(STATE_SELECTION,string='State',readonly=True),
            'memo': fields.char('Description', size=264, required=True, readonly=True, states={'draft':[('readonly',False)]}),
            #'distrik_id' : fields.many2one('hr.distrik', "Distrik", readonly=True, states={'draft':[('readonly',False)]}),
            'type' : fields.selection([('Pindah Buku','Pindah Buku'),
                                       ('Valas','Valas'),
                                       ('Deposito', 'Deposito'),
                                       ],'Type', readonly=True, states={'draft':[('readonly',False)]}),
            
            'move_ids': fields.many2many('account.move', 'transfer_move_rel', 'transfer_id',
                                        'move_id', 'Invoices', copy=False,),
            
            'payment_adm': fields.selection([
                    #('cash','Cash'),
                    #('free_transfer','Non Payment Administration Transfer'),
                    #('transfer','Transfer'),
                    ('check','Cheque/ Giro'),
                    #('cc','Credit Card'),
                    #('debit','Debit Card'),
                    ],'Payment Adm', readonly=True, select=True, states={'draft': [('readonly', False)],'proforma': [('readonly', False)]}),
            "check_registered" : fields.many2one('account.check.line', "Check Registered"),
            "employee_id": fields.many2one("hr.employee","Receive by"),
            'bank_id': fields.related("check_registered", "bank_id", relation='res.bank', type="many2one", readonly=True, string="Bank"),
            'check_number': fields.char('Check No', size=128, required=False, readonly=True, states={'draft': [('readonly', False)],'proforma': [('readonly', False)]}),
            "check_start_date": fields.date("Check Date", required=False, readonly=True, states={"draft":[("readonly", False)],'proforma': [('readonly', False)]}),
            "check_end_date": fields.date("Check Expire Date", required=False, readonly=True, states={"draft":[("readonly", False)],'proforma': [('readonly', False)]}),
           
            ####----valas
            'valas_date_confirm' : fields.date("Valas Date Confirm", required=False, readonly=True, states={"draft":[("readonly", False)],'proforma': [('readonly', False)]}),
            'bank_id'       : fields.many2one('res.bank', 'Bank Name'),
            
            'src_bank_id'       : fields.many2one('res.partner.bank', 'Bank Source'),
            'dst_bank_id'       : fields.many2one('res.partner.bank', 'Bank Destination'),
            'bank_contact' : fields.char("Bank Contact", size=128),
            'contact_confirm' : fields.char("Confirm By", size=128),
            'number_confirm'    : fields.char('Number'),
            'valas_type'        : fields.selection([('Today','Today'),
                                                    ('Tomorrow','Tomorrow'),
                                                    ('Spot','Spot'),
                                                    ],"Valas Type"),
            ####----deposito
            'depo_date_start' : fields.date("Start Date", required=False, readonly=True, states={"draft":[("readonly", False)]}),
            'depo_date_end' : fields.date("Maturity Date", required=False, readonly=True, states={"draft":[("readonly", False)]}),
            'interest'      : fields.float('Interest Rate',required=False, readonly=True, states={"draft":[("readonly", False)]}),
            'notes'         : fields.text('Notes'),
        }
    _defaults = {
            'name': '/',
            'type':'Pindah Buku',
            'company_id': lambda s,cr,u,c: s.pool.get('res.users').browse(cr,u,u).company_id.id,
            'date': lambda *a: time.strftime('%Y-%m-%d'),
            'exchange_rate': 1.0,
            'exchange_src_rate':1.0,
            'exchange_inv': 1.0,
            'state': 'draft',
            'valas_type' : 'Today'
        }
    _sql_constraints = [('name_unique','unique(company_id,name)',_('The number must be unique!'))]
    _name = 'account.transfer'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Account Cash and Bank Transfer'
    _order = 'name desc'
    
    def onchange_bank(self, cr, uid, ids, bank_id):
        res = {'value':{}}
        bank_contact = self.pool.get('res.bank').browse(cr, uid, [bank_id], context=None)[0].contact_name
        res['value']['bank_contact'] = bank_contact
        return res
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('name','/')=='/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'account.transfer') or '/'
        order =  super(account_transfer, self).create(cr, uid, vals, context=context)
        return order
    
    def check_checking(self, cr, uid, ids, context=None):
        for voc in self.browse(cr, uid, ids, context=None):
            if voc.check_registered.check_id:
                if voc.check_registered.check_id.state <> "confirm":
                    raise osv.except_osv(_('Warning !'), _('This Check is not Confirm.'))
        return True
    
    def update_check(self, cr, uid, ids, context=None):
        for tr in self.browse(cr, uid, ids, context=None):
            val = {
                   'move_id'        : tr.voucher_ids[0].move_id.id,
                   'voucher_id'     : False,
                   'partner_id'     : tr.company_id.partner_id.id or False,
                   'employee_id'    : tr.employee_id.id or False,
                   'amount'         : tr.src_amount,
                   'date'           : tr.check_start_date or tr.check_end_date,
                   'date_end'       : tr.check_end_date,
                   'state'          :'released'
                    }
            
            self.pool.get('account.check.line').write(cr, uid, [tr.check_registered.id], val)
            #self.pool.get('account.check').write(cr, uid, [voc.check_registered.check_id.id], {'state':'used'})
        return True
    
    def cancel_check(self, cr, uid, ids, context=None):
        for tr in self.browse(cr, uid, ids, context=None):
            if tr.payment_adm == 'check':
                val = {
                   'move_id'        : False,
                   'voucher_id'     : False,
                   'partner_id'     : False,
                   'employee_id'    : False,
                   'amount'         : 0.0,
                   'date'           : False,
                   'date_end'       : False,
                   'state'          :'hold'
                    }
                self.pool.get('account.check.line').write(cr, uid, [tr.check_registered.id], val)
        return True

    def unlink(self, cr, uid, ids, context=None):
        for trans in self.browse(cr, uid, ids, context=context):
            if trans.state not in ('draft'):
                raise osv.except_osv(_('User Error!'),_('You cannot delete a not draft transfer "%s"') % trans.name)
        return super(account_transfer, self).unlink(cr, uid, ids, context=context)

    def copy(self, cr, uid, id, defaults, context=None):
        defaults['name'] = self.pool.get('ir.sequence').get(cr, uid, 'account.transfer')
        defaults['voucher_ids'] = []
        return super(account_transfer, self).copy(cr, uid, id, defaults, context=context)
    
    def onchange_src_amount(self, cr, uid, ids, exchange_rate, src_amount):
        res = {'value':{}}
        print "exchange_rate, src_amount", exchange_rate, src_amount
        
        res['value']['src_amount'] = src_amount * exchange_rate
        
        return res

    
    def onchange_amount(self, cr, uid, ids, field, src_amount, dst_amount, exchange_rate):
        res = {'value':{}}
        if field == 'src_amount':
            res['value']['src_amount'] = src_amount
            res['value']['dst_amount'] = src_amount / exchange_rate
            res['value']['exchange_rate'] = exchange_rate
            res['value']['exchange_inv'] = exchange_rate and 1.0 / exchange_rate or 0.0
        elif field == 'dst_amount':
            res['value']['src_amount'] = exchange_rate and dst_amount * exchange_rate or 0.0
            res['value']['dst_amount'] = dst_amount
            res['value']['exchange_rate'] = exchange_rate
            res['value']['exchange_inv'] = exchange_rate and 1.0 / exchange_rate or 0.0
        elif field == 'exchange_rate':
            res['value']['src_amount'] = src_amount
            res['value']['dst_amount'] = src_amount / exchange_rate
            res['value']['exchange_rate'] = exchange_rate
            res['value']['exchange_inv'] = exchange_rate and 1.0 / exchange_rate or 0.0
        return res

    def onchange_journal(self, cr, uid, ids, src_journal_id, dst_journal_id, date, exchange_rate, src_amount):
        res = {'value':{}}
        if not(src_journal_id and dst_journal_id):
            return res
        src_journal = self.pool.get('account.journal').browse(cr, uid, src_journal_id)
        dst_journal = self.pool.get('account.journal').browse(cr, uid, dst_journal_id)
        res['value']['src_balance'], res['value']['dst_balance'] = self._get_balance(src_journal,dst_journal,src_journal.company_id)
        res['value']['exchange'] = (src_journal.currency.id != dst_journal.currency.id)
        res['value']['src_have_partner'], res['value']['dst_have_partner'] = src_journal.have_partner, dst_journal.have_partner
        res['value']['exchange_rate'] = exchange_rate
        if res['value']['exchange']:
            res['value']['exchange_rate'] = (src_journal.currency and src_journal.currency.rate or src_journal.company_id.currency_id.rate) and ((dst_journal.currency and dst_journal.currency.rate or dst_journal.company_id.currency_id.rate) / (src_journal.currency and src_journal.currency.rate or src_journal.company_id.currency_id.rate)) or 0.0
        else:
            res['value']['exchange_rate'] = 1.0
        res['value']['exchange_inv'] = res['value']['exchange_rate'] and (1.0 / res['value']['exchange_rate']) or 0.0
        res['value']['dst_amount'] = res['value']['exchange_rate'] * src_amount
        return res

    def transfer_confirm(self, cr, uid, ids, context=None):
        move_obj        = self.pool.get('account.move')
        move_line_obj   = self.pool.get('account.move.line')
        move_ids        = []
        for trans in self.browse(cr, uid, ids, context=context):          
            cr.execute("select id from account_period where date_start <= %s and date_stop >= %s and company_id = %s", (trans.date, trans.date, trans.company_id.id))
            period_id = cr.fetchone()[0]
            seq_obj = self.pool.get('ir.sequence')  
            company_currency    = trans.company_id.currency_id.id
            src_currency        = trans.src_journal_id.currency.id or company_currency
            #--sequence--
            
            if trans.src_journal_id.sequence_id:
                names = seq_obj.get_id(cr, uid, trans.src_journal_id.sequence_id.id)
            else:
                raise osv.except_osv(_('Error !'), _('Please define a sequence on the journal !'))
            
            move = {
                'name'      : names,
                'journal_id': trans.src_journal_id.id,
                'narration' : 'Transfer-' + trans.type +'' + trans.memo,
                'date'      : trans.date,
                'ref'       : trans.name,
                'period_id' : period_id,
                #'district'   : trans.src_journal_id.distrik_id.id,
                }
            move_id = move_obj.create(cr, uid, move)
            move_ids.append(move_id)
            
            move_line = {
                    'name'              : trans.memo or '/',
                    'debit'             : 0.0,
                    'credit'            : src_currency and trans.src_amount*trans.exchange_src_rate or trans.src_amount*trans.exchange_src_rate,
                    'account_id'        : trans.src_journal_id.default_credit_account_id.id,
                    'move_id'           : move_id,
                    'journal_id'        : trans.src_journal_id.id,
                    'period_id'         : period_id,
                    #'partner_id'        : ext_pay_line.partner_id.id,
                    'currency_id'       : company_currency <> src_currency and src_currency or False,
                    'amount_currency'   : company_currency <> src_currency and -trans.src_amount,#company_currency <> ext_pay.currency_id.id and ext_pay_line.debit or company_currency <> ext_pay.currency_id.id and -ext_pay_line.credit or 0.0,
                    'date'              : trans.date,
                    #'district'   : trans.src_journal_id.distrik_id.id,
                    }
            move_line_obj.create(cr, uid, move_line, context=None)
            
            move_line = {
                    'name'              : trans.memo or '/',
                    'debit'             : src_currency and trans.src_amount*trans.exchange_src_rate or trans.src_amount*trans.exchange_src_rate,
                    'credit'            : 0.0,
                    'account_id'        : trans.src_journal_id.account_transit.id,
                    'move_id'           : move_id,
                    'journal_id'        : trans.src_journal_id.id,
                    'period_id'         : period_id,
                    #'partner_id'        : ext_pay_line.partner_id.id,
                    'currency_id'       : company_currency <> src_currency and src_currency or False,
                    'amount_currency'   : company_currency <> src_currency and trans.src_amount,#company_currency <> ext_pay.currency_id.id and ext_pay_line.debit or company_currency <> ext_pay.currency_id.id and -ext_pay_line.credit or 0.0,
                    'date'              : trans.date,
                    #'district'   : trans.src_journal_id.distrik_id.id,
                    }
            move_line_obj.create(cr, uid, move_line, context=None)
            move_obj.post(cr, uid, [move_id], context={})
            
        print "move_idsmove_idsmove_idsmove_idsmove_ids", move_ids
        return self.write(cr, uid, ids, {'state':'confirm', 'move_ids':[(6, 0, move_ids)]},context=context)
    
    def transfer_done(self, cr, uid, ids, context=None):
        move_obj        = self.pool.get('account.move')
        move_line_obj   = self.pool.get('account.move.line')
        move_ids        = []
        for trans in self.browse(cr, uid, ids, context=context):          
            cr.execute("select id from account_period where date_start <= %s and date_stop >= %s and company_id = %s", (trans.date, trans.date, trans.company_id.id))
            period_id = cr.fetchone()[0]
            seq_obj = self.pool.get('ir.sequence')  
            company_currency    = trans.company_id.currency_id.id
            dst_currency        = trans.dst_journal_id.currency.id or company_currency
            
            if trans.dst_journal_id.sequence_id:
                name2 = seq_obj.get_id(cr, uid, trans.dst_journal_id.sequence_id.id)
            else:
                raise osv.except_osv(_('Error !'), _('Please define a sequence on the journal !'))
            ###Old Move###
            move_ids.append(trans.move_ids[0].id)
            ###
            move = {
                'name'      : name2,
                'journal_id': trans.dst_journal_id.id,
                'narration' : 'Receive-' + trans.type +'' + trans.memo,
                'date'      : trans.date_receive,
                'ref'       : trans.name,
                'period_id' : period_id,
                #'district'   : trans.dst_journal_id.distrik_id.id,
                
                }
            move_id = move_obj.create(cr, uid, move)
            move_ids.append(move_id)
            
            move_line = {
                    'name'              : trans.memo or '/',
                    'debit'             : dst_currency and trans.dst_amount*trans.exchange_rate or trans.dst_amount*trans.exchange_rate,
                    'credit'            : 0.0,
                    'account_id'        : trans.dst_journal_id.default_debit_account_id.id,
                    'move_id'           : move_id,
                    'journal_id'        : trans.dst_journal_id.id,
                    'period_id'         : period_id,
                    #'partner_id'        : ext_pay_line.partner_id.id,
                    'currency_id'       : company_currency <> dst_currency and dst_currency or False,
                    'amount_currency'   : company_currency <> dst_currency and trans.dst_amount,#company_currency <> ext_pay.currency_id.id and ext_pay_line.debit or company_currency <> ext_pay.currency_id.id and -ext_pay_line.credit or 0.0,
                    'date'              : trans.date_receive,
                    #'district'   : trans.dst_journal_id.distrik_id.id,
                    }
            move_line_obj.create(cr, uid, move_line, context=None)
            
            move_line = {
                    'name'              : trans.memo or '/',
                    'debit'             : 0.0,
                    'credit'            : dst_currency and trans.dst_amount*trans.exchange_rate or trans.dst_amount*trans.exchange_rate,
                    'account_id'        : trans.dst_journal_id.account_transit.id,
                    'move_id'           : move_id,
                    'journal_id'        : trans.dst_journal_id.id,
                    'period_id'         : period_id,
                    #'partner_id'        : ext_pay_line.partner_id.id,
                    'currency_id'       : company_currency <> dst_currency and dst_currency or False,
                    'amount_currency'   : company_currency <> dst_currency and -trans.dst_amount,#company_currency <> ext_pay.currency_id.id and ext_pay_line.debit or company_currency <> ext_pay.currency_id.id and -ext_pay_line.credit or 0.0,
                    'date'              : trans.date_receive,
                    #'district'   : trans.dst_journal_id.distrik_id.id,
                    }
            move_line_obj.create(cr, uid, move_line, context=None)
            move_obj.post(cr, uid, [move_id], context={})
            
        return self.write(cr, uid, ids, {'state':'done', 'move_ids':[(6, 0, move_ids)]},context=context)

    def action_done(self, cr, uid, ids, context=None):
        print "#####################action_done####################"
        voucher_obj = self.pool.get('account.voucher')
        move_obj = self.pool.get('account.move')
        for trans in self.browse(cr, uid, ids, context=context):
            paid_amount = []
#             import pdb; pdb.set_trace()
            for voucher in trans.voucher_ids:
                voucher.state=='draft' and voucher_obj.proforma_voucher(cr, uid, [voucher.id], context=context)
                sign = (voucher.journal_id.id == trans.src_journal_id.id) and 1 or -1
                paid_amount.append(sign * voucher_obj._paid_amount_in_company_currency(cr, uid, [voucher.id], '', '')[voucher.id])
                #paid_amount.append(sign * voucher.paid_amount_in_company_currency)
            sum_amount = sum(paid_amount)
            if len(paid_amount) > 1 and sum_amount != 0.0:
                periods = self.pool.get('account.period').find(cr, uid)
                print "periods>>>>>>>>>>>>>>>>>>>>", periods
                move = {}
                move['journal_id'] = trans.dst_journal_id.id
                move['period_id'] = periods and periods[0] or False
                move['ref'] = trans.name + str(trans.origin and (' - ' + trans.origin) or '')
                move['date'] = trans.date
                move['line_id'] = [(0,0,{}),(0,0,{})]
                move['line_id'][0][2]['name'] = trans.name
                move['line_id'][1][2]['name'] = trans.name
                if sum_amount > 0:
                    move['line_id'][0][2]['account_id'] = trans.dst_journal_id.default_debit_account_id.id
                    move['line_id'][1][2]['account_id'] = trans.src_journal_id.account_transit.id #trans.company_id.income_currency_exchange_account_id.id
                    move['line_id'][0][2]['debit'] = sum_amount
                    move['line_id'][1][2]['credit'] = sum_amount
                else:
                    move['line_id'][0][2]['account_id'] = trans.dst_journal_id.default_credit_account_id.id
                    move['line_id'][1][2]['account_id'] = trans.src_journal_id.account_transit.id #trans.company_id.expense_currency_exchange_account_id.id
                    move['line_id'][1][2]['debit'] = -1 * sum_amount
                    move['line_id'][0][2]['credit'] = -1 * sum_amount
                move_id = move_obj.create(cr, uid, move, context=context)
                self.write(cr, uid, [trans.id], {'adjust_move':move_id}, context=context)
            
            if trans.payment_adm == 'check':
                self.check_checking(cr, uid, ids, context)
                self.update_check(cr, uid, ids, context)
        return self.write(cr, uid, ids, {'state':'done'},context=context)

    def transfer_cancel(self, cr, uid, ids, context=None):
        voucher_obj = self.pool.get('account.voucher')
        move_obj = self.pool.get('account.move')
        #import pdb; pdb.set_trace()
        for trans in self.browse(cr, uid, ids, context=context):
            for move in trans.move_ids:            
                move_obj.button_cancel(cr, uid, [move.id], context=None)
                move_obj.unlink(cr, uid, [move.id], context=context)
        self.cancel_check(cr, uid, ids, context)
        return self.write(cr, uid, ids, {'state':'cancel'},context=context)
    
    def transfer_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'},context=context)

account_transfer()