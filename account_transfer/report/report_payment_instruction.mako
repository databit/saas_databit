<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<style>
			.vtop
			{
				vertical-align: top;
			}

			.vbottom
			{
				vertical-align: bottom;
			}
		
			.hright
			{
				text-align: right;
				padding-right: 3px;
			}
			.hleft
			{
				text-align: left;
			}
			.hmid
			{
				text-align: center;
			}
			.content
			{
				font-size: 12px;
			}
			.border_grey
			{
				border: 1px solid lightGrey;
			}
			.border_black
			{
				border: 1px solid black;
			}
			.space
			{
				min-height: 25px;
			}
			.note
			{
				width: 500px;
				padding: 5px;
				float:right;
				min-width: 100px;
				border:1px solid black;
			}
			.padding
			{
				padding: 5px;
			}
			.paddingtop
			{
				padding-top: 10px;
			}
			.paddingright
			{
				padding-right: 10px;
			}
			th
			{
				font-size: 12px;
				border-bottom: 1px solid black;
			}
			.border_bottom_grey
			{
				border-bottom: 1px solid lightGrey;
			}
			.background_color
			{
				background-color: lightGrey
			}
			.border_top
			{
				border-top: 1px solid black;
			}
			.border_bottom
			{
				border-bottom: 1px solid black;
			}
			.border_right
			{
				border-right: 1px solid black;
			}
			.border_top_bottom
			{
				border-top: 1px solid lightGrey;
				border-bottom: 1px solid black;
			}
			.border_left_right
			{
				border-right: 1px solid black;
				border-left: 1px solid black;
			}
			.fright
			{
				float: right; 
			}
			.fleft
			{
				float: left; 
			}
			.font12px
			{
				font-size: 12px;
			}
			.font10px
			{
				font-size: 10px;
			}
			.font14px
			{
				font-size: 14px;
			}
			.font22px
			{
				font-size: 22px;
			}
			.font30px
			{
				font-size: 30px;
			}
			.title 
			{
				font-size: 22px;
				text-align: center;
				padding: 5px;
			}
			.title-table 
			{
				font-size: 12px;
				text-align:center;
				padding-top:20px;
			}
			.title-form 
			{
				font-size: 22px;
				text-align:center;
				padding-top:20px;
				padding-bottom:10px;
			}
			
         	table.one 
         	{border-collapse:collapse;}
			
		</style>
	</head>
	%for o in objects:
	 <body style="border:0; margin: 0;" onload="subst()">
        
        <table class="header" style="border-bottom: 0px solid black; width: 100%">
           <tr>
                <td style="width: 20%">${helper.embed_company_logo()|safe}</td>
                <td style="width: 80%">
	                <table class="font12px" style="border:0; margin: 0;" >
	                	<tr>
	                		<td style="width: 80%">${company.partner_id.name}</td>
	                	</tr>
	                	<tr>
	                		<td style="width: 85%">${company.partner_id.street or ''}</td>
	                	</tr>
	                	<tr>
		               		 <td style="width: 80%">Phone: ${company.partner_id.phone or ''} </td>
		               </tr>
		               </table>
	             </td>
            </tr>
            	
        	</table> ${_debug or ''|safe}
        	
        	<table cellpadding="1px" width="100%" class="font12px">
				<tr >
					<td align="center"></td>
					<td align="center"  class="title-form font22px"><b>URGENT</b></td>
					
				</tr>
				<tr >
					<td align="left" class="font12px">Nomor : ${o.name}</td>
					<td align="center">Jakarta,  ${time.strftime('%d %B %Y', time.strptime( o.date,'%Y-%m-%d'))}</td>
				</tr>
				<tr>
					<td><br/> <br/>
					</td>
				</tr>
				<tr >
					<td align="left" class="font12px">To :</td>
				</tr>
				<tr >
					<td align="left" class="font12px">${o.bank_id.name}</td>
				</tr>
				<tr >
					<td align="left" class="font12px">${o.bank_id.street or ''}</td>
				</tr>
				<tr >
					<td align="left" class="font12px">${o.bank_id.street2 or ''}</td>
				</tr>
				<tr >
					<td align="left" class="font12px">${o.bank_id.city or ''}</td>
				</tr>
				<tr >
					<td align="left" class="font12px">${o.bank_id.zip or ''}</td><br/>
				</tr>
				<tr >
					<td align="left" class="font12px"><b>Attn : ${o.bank_id.contact_name} -  fax no: ${o.bank_id.fax}</b></td>
				</tr>
				
			</table>
			<table cellpadding="2px" class="font12px">
				<tr>
					<td><br/> <br/>
					</td>
				</tr>
				
				
				<tr>
					<td colspan="2">Dear Sir/Madam,</td>
					
				</tr>
				<tr>
					<td><br/>
					</td>
				</tr>
				<tr>
					<td colspan="2">Today we confirm to buy <b>${o.dst_journal_id.currency.name or o.dst_journal_id.company_id.currency_id.name } ${ formatLang(o.dst_amount) or formatLang(0)}</b> at rate <b>${o.exchange_rate}</b> equivalent with <b>${o.src_journal_id.currency.name or o.dst_journal_id.company_id.currency_id.name} ${ formatLang(o.src_amount) or formatLang(0)}</b> -value ${o.valas_type}, ${time.strftime('%B %d, %Y', time.strptime( o.date,'%Y-%m-%d'))}
					confirmed by <b>${o.contact_confirm}</b> number <b>${o.number_confirm}</b>.
					
					</td>
				</tr>
				
				<tr>
					<td><br/>
					</td>
				</tr>
				<tr>
					<td colspan="2">Therefor please debit our account with you no ${o.src_bank_id.acc_number} for the above ${o.src_journal_id.currency.name or o.dst_journal_id.company_id.currency_id.name} amount
					and credited the above ${o.dst_journal_id.currency.name or o.dst_journal_id.company_id.currency_id.name } amount to our ${o.dst_journal_id.currency.name or o.dst_journal_id.company_id.currency_id.name } account with you no ${o.dst_bank_id.acc_number}
					
					</td>
				</tr>
				<tr>
					<td><br/> 
					</td>
				</tr>
				<tr>
					<td colspan="2">We appreciate your prompt response to this request.
					</td>
				</tr>
				<tr>
					<td><br/> <br/>
					</td>
				</tr>
				<table>
				<tr>
					<td class="font12px">Yours faithfully,
					</td>
				</tr>
				<tr>
					<td><br/> <br/>
					<br/> <br/>
					</td>
				</tr>
				<tr>
				
					<td width="40%" class="border_bottom">
					</td>
					<td width="5%">
					</td>
					<td width="40%" class="border_bottom">
					</td>
					<td width="5%">
					</td>
				</tr>
				</table>
				
			</table>
       
       %endfor 	
       </body>
   </html>	