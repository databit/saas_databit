# -*- encoding: utf-8 -*-
# Python source code encoding : https://www.python.org/dev/peps/pep-0263/
##############################################################################
#
#    OpenERP, Odoo Source Management Solution
#    Copyright (c) 2015 Databit Solusi Indonesia (http://www.databit.co.id)
#    Ade Anshori <adeanshori@databit.co.id>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################



{
    'name': 'Stock Account Extension',
    'version': '1.0',
    'author': 'Databit Solusi Indonesia',
    'website': 'https://www.databit.co.id',
    'description': """
This module extension from the stock_account which are aim to.
=====================================================================================================================
* add fields cogs_price and cogs_price_line (as record for historical average price)
* 
""",
    'depends': ['product', 'stock_account'],
    'category': 'SME PACKAGE,ENTERPRICE PACKAGE',
    'demo': [],
    'data': [
             #'wizard/stock_transfer_details.xml',
             'product_view.xml'
             ],
    'test': [],
    'auto_install': False,
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
