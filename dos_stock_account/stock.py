# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import logging
_logger = logging.getLogger(__name__)

class stock_quant(osv.osv):
    """
    Quants are the smallest unit of stock physical instances
    """
    _inherit = "stock.quant"
    _description = "Quants"
    
    def _get_accounting_data_for_valuation(self, cr, uid, move, context=None):
        """
        Return the accounts and journal to use to post Journal Entries for the real-time
        valuation of the quant.

        :param context: context dictionary that can explicitly mention the company to consider via the 'force_company' key
        :returns: journal_id, source account, destination account, valuation account
        :raise: osv.except_osv() is any mandatory account or journal is not defined.
        """
        product_obj = self.pool.get('product.template')
        accounts = product_obj.get_product_accounts(cr, uid, move.product_id.product_tmpl_id.id, context)
        if move.location_id.valuation_out_account_id:
            acc_src = move.location_id.valuation_out_account_id.id
        #take expense account for HPP
        elif move.product_id.categ_id.type == 'product' and (not move.purchase_line_id) and move.product_id.property_account_expense_categ:
            acc_src = move.product_id.categ_id.type == 'product' and move.product_id.categ_id.property_account_expense_categ.id
        else:
            acc_src = accounts['stock_account_input']

        if move.location_dest_id.valuation_in_account_id:
            acc_dest = move.location_dest_id.valuation_in_account_id.id
        #take expense account for HPP
        elif move.product_id.categ_id.type == 'product' and (not move.sale_line_id) and move.product_id.categ_id.property_account_expense_categ:
            acc_dest = move.product_id.categ_id.type == 'product' and move.product_id.categ_id.property_account_expense_categ.id
        else:
            acc_dest = accounts['stock_account_output']

        acc_valuation = accounts.get('property_stock_valuation_account_id', False)
        journal_id = accounts['stock_journal']
        return journal_id, acc_src, acc_dest, acc_valuation

    def _create_account_move_line(self, cr, uid, quants, move, credit_account_id, debit_account_id, journal_id, context=None):
        #group quants by cost
        quant_cost_qty = {}
        for quant in quants:
            if quant_cost_qty.get(quant.cost):
                quant_cost_qty[quant.cost] += quant.qty
            else:
                quant_cost_qty[quant.cost] = quant.qty
        stock_move_obj = self.pool.get('stock.move')
        move_obj = self.pool.get('account.move')
        for cost, qty in quant_cost_qty.items():
            move_lines = self._prepare_account_move_line(cr, uid, move, qty, cost, credit_account_id, debit_account_id, context=context)
            print "_create_account_move_line====quant_cost_qty",cost, qty
            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            period_id = context.get('force_period', self.pool.get('account.period').find(cr, uid, context=context)[0])
            move_obj.create(cr, uid, {'journal_id': journal_id,
                                      'line_id': move_lines,
                                      'period_id': period_id,
                                      'date': fields.date.context_today(self, cr, uid, context=context),
                                      'ref': move.picking_id.name}, context=context)
            #===================make journal if it difference cost price========
            stock_move_obj._anglo_saxon_stock_move_diff(cr, uid, move, cost, context=context)
            #===================================================================
            
    
class stock_move(osv.Model):
    _inherit = "stock.move"
    
#     def action_done(self, cr, uid, ids, context=None):
#         #context.update({'date'})
#         #self.product_price_update_before_done(cr, uid, ids, context=context)
#         res = super(stock_move, self).action_done(cr, uid, ids, context=context)
#         #self.product_price_update_after_done(cr, uid, ids, context=context)
#         print "action_done",res
#         return res
    
    def product_price_update_before_done(self, cr, uid, ids, context=None):
        product_obj = self.pool.get('product.product')
        product_cogs_obj = self.pool.get('product.cogs.price')
        tmpl_dict = {}
        for move in self.browse(cr, uid, ids, context=context):
            #print "product_price_update_before_done",move,context
            #adapt standard price on incomming moves if the product cost_method is 'average'
            if (move.location_id.usage == 'supplier') and (move.product_id.cost_method == 'average'):
                product = move.product_id
                prod_tmpl_id = move.product_id.product_tmpl_id.id
                qty_available = move.product_id.product_tmpl_id.qty_available
                amount_unit = product.standard_price
                if tmpl_dict.get(prod_tmpl_id):
                    product_avail = qty_available + tmpl_dict[prod_tmpl_id]
                else:
                    tmpl_dict[prod_tmpl_id] = 0
                    product_avail = qty_available
                if product_avail <= 0:
                    new_std_price = move.price_unit
                else:
                    # Get the standard price
                    amount_unit = product.standard_price
                    new_std_price = ((amount_unit * product_avail) + (move.price_unit * move.product_qty)) / (product_avail + move.product_qty)
                tmpl_dict[prod_tmpl_id] += move.product_qty
                # Write the standard price, as SUPERUSER_ID because a warehouse manager may not have the right to write on products
                ctx = dict(context or {}, force_company=move.company_id.id)
                #updatenya tidak ke standard price lagi
                #product_obj.write(cr, SUPERUSER_ID, [product.id], {'standard_price': new_std_price}, context=ctx)
                vals_cogs = {
                    'product_tmpl_id': product.product_tmpl_id and product.product_tmpl_id.id,
                    'stock_move_id': move.id,
                    'name':fields.date.context_today(self, cr, uid, context=context), #date seharusnya bisa backdate
                    'qty_before':product_avail,
                    'qty_new':move.product_qty,
                    'cogs_before':amount_unit,
                    'cogs_new':move.price_unit,
                }
                product_cogs_obj.create(cr, SUPERUSER_ID, vals_cogs)
                
    def _anglo_saxon_stock_move_diff(self, cr, uid, i_line, cost, context=None):
        """Return the additional move lines for purchase invoices and refunds.

        i_line: An account.invoice.line object.
        res: The move line entries produced so far by the parent move_line_get.
        """
        pick = i_line.picking_id
        company_currency = pick.company_id.currency_id.id
        if i_line.product_id and i_line.product_id.valuation == 'real_time':
            if i_line.product_id.type != 'service':
                # get the price difference account at the product
                acc = i_line.product_id.property_account_creditor_price_difference and i_line.product_id.property_account_creditor_price_difference.id
                if not acc:
                    # if not found on the product get the price difference account at the category
                    acc = i_line.product_id.categ_id.property_account_creditor_price_difference_categ and i_line.product_id.categ_id.property_account_creditor_price_difference_categ.id
                a = None

                # oa will be the stock input account
                # first check the product, if empty check the category
                oa = i_line.product_id.property_stock_account_input and i_line.product_id.property_stock_account_input.id
                if not oa:
                    oa = i_line.product_id.categ_id.property_stock_account_input_categ and i_line.product_id.categ_id.property_stock_account_input_categ.id
                if oa:
                    # get the fiscal position
                    fpos = i_line.invoice_id.fiscal_position or False
                    a = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, oa)
                diff_res = []
                decimal_precision = self.pool.get('decimal.precision')
                account_prec = decimal_precision.precision_get(cr, uid, 'Account')
                # calculate and write down the possible price difference between invoice price and product price
#                 for line in res:
#                     if line.get('invl_id', 0) == i_line.id and a == line['account_id']:
#                         uom = i_line.product_id.uos_id or i_line.product_id.uom_id
#                         #valuation_price_unit = self.pool.get('product.uom')._compute_price(cr, uid, uom.id, i_line.product_id.standard_price, i_line.uos_id.id)
#                         valuation_price_unit = self.pool.get('product.uom')._compute_price(cr, uid, uom.id, i_line.product_id.cogs_price, i_line.uos_id.id)
#                         if i_line.product_id.cost_method != 'standard' and i_line.purchase_line_id:
#                             #for average/fifo/lifo costing method, fetch real cost price from incomming moves
#                             stock_move_obj = self.pool.get('stock.move')
#                             valuation_stock_move = stock_move_obj.search(cr, uid, [('purchase_line_id', '=', i_line.purchase_line_id.id)], limit=1, context=context)
#                             if valuation_stock_move:
#                                 valuation_price_unit = stock_move_obj.browse(cr, uid, valuation_stock_move[0], context=context).price_unit
#                         if inv.currency_id.id != company_currency:
#                             valuation_price_unit = self.pool.get('res.currency').compute(cr, uid, company_currency, inv.currency_id.id, valuation_price_unit, context={'date': inv.date_invoice})
#                         if valuation_price_unit != i_line.price_unit and line['price_unit'] == i_line.price_unit and acc:
#                             # price with discount and without tax included
#                             price_unit = self.pool['account.tax'].compute_all(cr, uid, line['taxes'],
#                                 i_line.price_unit * (1-(i_line.discount or 0.0)/100.0), line['quantity'])['total']
#                             price_line = round(valuation_price_unit * line['quantity'], account_prec)
#                             price_diff = round(price_unit - price_line, account_prec)
#                             line.update({'price': price_line})
#                             diff_res.append({
#                                 'type': 'src',
#                                 'name': i_line.name[:64],
#                                 'price_unit': round(price_diff / line['quantity'], account_prec),
#                                 'quantity': line['quantity'],
#                                 'price': price_diff,
#                                 'account_id': acc,
#                                 'product_id': line['product_id'],
#                                 'uos_id': line['uos_id'],
#                                 'account_analytic_id': line['account_analytic_id'],
#                                 'taxes': line.get('taxes', []),
#                                 })
                return diff_res
        return []
                