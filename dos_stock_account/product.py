import re
import time
import math

from openerp import api, fields as fields2
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools import float_round, float_is_zero, float_compare
from openerp.tools.translate import _
import simplejson as json
import openerp.addons.decimal_precision as dp

class product_template(osv.osv):
    def _current_cogs(self, cr, uid, ids, name, arg, context=None):
        return self._get_current_cogs(cr, uid, ids, context=context)
    
    def _get_current_cogs(self, cr, uid, ids, raise_on_no_rate=True, context=None):
        #print "_get_current_cogs",context
        if context is None:
            context = {}
        res = {}
        date = context.get('date') or time.strftime('%Y-%m-%d')
        for id in ids:
            cr.execute('SELECT cogs_after FROM product_cogs_price '
                       'WHERE product_tmpl_id = %s '
                         'AND name <= %s '
                       'ORDER BY name desc, id desc LIMIT 1',
                       (id, date))
            if cr.rowcount:
                res[id] = cr.fetchone()[0]
            elif not raise_on_no_rate:
                res[id] = 0
            else:
                product = self.browse(cr, uid, id, context=context)
                res[id] = product.standard_price
                #raise osv.except_osv(_('Error!'),_("No cogs price associated for '%s' for the given period" % (product.name)))
        return res
        
    _inherit = "product.template"
    _description = "Product Template"    
    _columns = {
        #'cogs_price': fields.float('CoGS Price', required=True, help="The cogs price based on product price and landing cost"),
        'cogs_price': fields.function(_current_cogs, string='CoGS Price', digits_compute= dp.get_precision('Product Price'),
            help='The cogs price based on product price and landing cost.'),
        'cogs_price_line': fields.one2many('product.cogs.price', 'product_tmpl_id', 'CoGS Lines'),
    }
    
    def _get_conversion_cogs_rate(self, cr, uid, from_product_tmpl_id, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        from_product_tmpl_id = self.browse(cr, uid, from_product_tmpl_id.id, context=ctx)
        #print "v=====ssss====",from_product_tmpl_id
        return from_product_tmpl_id.cogs_price

    def _compute(self, cr, uid, from_product_tmpl_id, from_amount, round=True, context=None):
        cogs_rate = self._get_conversion_cogs_rate(cr, uid, from_product_tmpl_id, context=context)
        #print "v=====_compute====",cogs_rate
        if cogs_rate:
            return cogs_rate
        else:
            return from_amount

    @api.v7
    def compute(self, cr, uid, product_tmpl_id, from_amount, round=True, context=None):
        context = context or {}
        if not product_tmpl_id:
            product_tmpl_id = product_tmpl_id
        xc = self.browse(cr, uid, [product_tmpl_id], context=context)
        from_product_tmpl_id = (xc[0].id == product_tmpl_id and xc[0]) or xc[1]
        return self._compute(cr, uid, from_product_tmpl_id, from_amount, round, context)
    
class product_cogs_price(osv.osv):
    
    def _calc_line_cogs_price(self, cr, uid, line, context=None):
        print "_calc_line_cogs_price",line.qty_before,line.qty_new
        return line.qty_before+line.qty_new != 0 and ((line.qty_before * (line.cogs_after or line.product_tmpl_id.standard_price)) + (line.qty_new * line.cogs_new)) / (line.qty_before + line.qty_new)
    
    def _calc_line_qty(self, cr, uid, line, context=None):
        return line.qty_before + line.qty_new
    
    def _cogs_line_after(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {
                'qty_after': 0,
                'cogs_after': 0,
            }
            qty_cogs = self._calc_line_qty(cr, uid, line, context=context)
            #new_std_price = ((line.qty_before * line.product_tmpl_id.cogs_price or line.product_tmpl_id.standard_price) + (line.qty_new * line.cogs_new)) / (qty_cogs)
            new_std_price = self._calc_line_cogs_price(cr, uid, line, context=context)
            #print "new_std_price",new_std_price
            res[line.id]['qty_after'] = qty_cogs
            res[line.id]['cogs_after'] = new_std_price
        return res

    _name = "product.cogs.price"
    _description = "Product CoGS Price Information"

    _columns = {
        'stock_move_id': fields.many2one('stock.move', 'Stock Move'),
        'product_tmpl_id' : fields.many2one('product.template', 'Product Template', required=True, ondelete='cascade', select=True, oldname='product_id'),
        'name' : fields.date('Date', help="Assigns date computed CoGS Price."),
        'qty_before': fields.float('Qty Before', digits_compute= dp.get_precision('Product UoS'), required=True),
        'qty_new': fields.float('Qty New', digits_compute= dp.get_precision('Product UoS'), required=True),
        'qty_after': fields.function(_cogs_line_after, string='Qty After', digits_compute= dp.get_precision('Product UoS'), multi='sums', store=True),
        'cogs_before': fields.float('CoGS Before', digits_compute= dp.get_precision('Product Price'), required=True),
        'cogs_new': fields.float('CoGS New', digits_compute= dp.get_precision('Product Price'), required=True),
        'cogs_after': fields.function(_cogs_line_after, string='CoGS After', digits_compute= dp.get_precision('Account'), multi='sums', store=True),
    }
    _order = 'name desc, id desc'
    
#     _sql_constraints = [
#         ('name_uniq', 'unique (product_tmpl_id, name)', 'Sequence of discount suppliers should be unique!')
#     ]