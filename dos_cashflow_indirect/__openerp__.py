# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution    
#   Copyright (C) 2013 ADSOft (<http://www.adsoft.co.id>). All Rights Reserved
#
##############################################################################

{
    'name': 'Report Cash Flow Indirect',
    'description': """Cash Flow Report Indirect""",
    'version': '1.0',
    'author' : 'ADSOFT',
    'category': 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    'depends' : [
                    'base',
                    'account',
                ],
    'init_xml' : [],
    'update_xml': [
#         'cash_flow_category_view.xml',
#         'account_view.xml',
        'wizard/cash_flow_view.xml',
        'cash_flow_category_view.xml',
        'account_view.xml',
        
                ],
    'demo_xml': [
                ],
    'active': False,
    'installable': True,
    'certificate': '',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: