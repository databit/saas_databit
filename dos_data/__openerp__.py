{
    "name"          : "Databit Master Data",
    "version"       : "1.0",
    "depends"       : ['base','dos_base'],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add master data for databit   
    Depends with dos_base                     
        * Add data province/state in Indonesia
        * Add data kabupaten in Indonesia
        * Add data kecamatan in Indonesia
        """,
    "website"       : "https://www.databit.co.id/",
    'category'      : 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                        "res_country_state.xml",
                        "res_kabupaten.xml",
                        "res_kecamatan.xml",
                       ],
    'installable': True,
    'auto_install': False,
}