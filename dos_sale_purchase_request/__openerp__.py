# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 BroadTech IT Solutions.
#    (http://wwww.broadtech-innovations.com)
#    contact@boradtech-innovations.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': "Sales & Purchase Requests",    
    'version': '1.0',    
    'category': 'SME PACKAGE,ENTERPRICE PACKAGE',    
    'summary': 'Sales & Purchase Requests, Invoicing',    
    'description': """
Manage sales requests and orders
==================================
This application allows you to manage your sales goals in an effective and efficient manner by keeping track of all sales orders and history.
It handles the full sales requests:
* **Quotation** -> **Sales Requests** -> **Invoice** """,
    'author': "Databit Solusi Indonesia",    
    'website': "http://databit.co.id/",    
    'depends': ['base',
                'account', 
                'sale', 
                'purchase',
                'hr',
                'dos_base',
                'dos_partner',
                #'dos_partner_epn',
                #'dos_hr_distrik',
                #'hr',
                #'pama_asset_management', 
                #'dos_ext_payment'
                ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'sequence/sale_sequence.xml',
        'sequence/purchase_sequence.xml',
        'report/report_invoice.xml',
        'sales_request_view.xml',
        'purchase_request_view.xml',
        'account_invoice_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'installable': False,
    'application': False,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: