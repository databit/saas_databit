# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 BroadTech IT Solutions.
#    (http://wwww.broadtech-innovations.com)
#    contact@boradtech-innovations.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime, timedelta
import time
from openerp import models, fields, api, _
from openerp import fields, models
from openerp.exceptions import except_orm, Warning, RedirectWarning
#from openerp.osv import fields, osv
#from pygments.lexer import _inherit

class account_invoice(models.Model):
    _inherit = 'account.invoice'
    
    state = fields.Selection([
            ('draft','Draft'),
            #======================add new======================================
            ('received', 'Receive'),
            ('tax_pass', 'Tax Pass'),
            ('validated', 'Validated'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            #===================================================================
            ('open','Open'),
            ('paid','Paid'),
            ('cancel','Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Pro-forma' when invoice is in Pro-forma status,invoice does not have an invoice number.\n"
             " * The 'Open' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice."
             " * The 'Tax Pass' status is used when a user is starting a draft invoice with tax."
             " * The 'Validated' status is used when user validate the invoice")
    date_received = fields.Date(string='Received Date',
        default=lambda *a: time.strftime('%Y-%m-%d'),                        
        readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Keep empty to use the current date", copy=False)
    date_supplier = fields.Date(string='Supplier Invoice Date',
        readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Keep empty to use the current date", copy=False)
    tax_date = fields.Date(string='Tax Date',
        readonly=True, states={'received': [('readonly', False)]}, index=True,
        help="Keep empty to use the current date", copy=False)
    npwp_no = fields.Char(string='NPWP No', size=63, readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="NPWP Number", copy=False)
    bukti_potong = fields.Char(string='No Bukti Potong', size=63, readonly=True, states={'received': [('readonly', False)]}, index=True,
        help="Nomor Bukti Potong", copy=False)
    vat_supplier = fields.Char(string='Faktur Pajak No', size=63, readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Nomor Bukti Potong", copy=False)
    date_invoice = fields.Date(string='Invoice Date',
        readonly=True, states={'draft': [('readonly', False)], 'received': [('readonly', False)]}, index=True,
        help="Keep empty to use the current date", copy=False)
    invoice_line = fields.One2many('account.invoice.line', 'invoice_id', string='Invoice Lines',
        readonly=True, states={'draft': [('readonly', False)],'received': [('readonly', False)]}, copy=True)
    tax_line = fields.One2many('account.invoice.tax', 'invoice_id', string='Tax Lines',
        readonly=True, states={'draft': [('readonly', False)],'received': [('readonly', False)]}, copy=True)
    ###
    invoice_receive_date = fields.Date(string='Receive Date')
    trade_type    = fields.Selection([('trade', 'Trading'),('nontrade', 'Non Trading')], string='Trading Type')
    nontrade_type = fields.Selection([('srcub', 'Scrub'),('backcharge', 'Back Charge'),('other', 'Other')], string='Non Trading Type')
    sale_request_id = fields.Many2one('sale.request', string='Sale Request')
    purchase_request_id = fields.Many2one('purchase.request', string='NOI Number')
    ba_number     = fields.Char(string='Berita Acara Number', related='sale_request_id.ba_number')
    ###
    
    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False, npwp=False):
        account_id = False
        payment_term_id = False
        fiscal_position = False
        bank_id = False
        npwp = False

        if partner_id:
            p = self.env['res.partner'].browse(partner_id)
            rec_account = p.property_account_receivable
            pay_account = p.property_account_payable
            if company_id:
                if p.property_account_receivable.company_id and \
                        p.property_account_receivable.company_id.id != company_id and \
                        p.property_account_payable.company_id and \
                        p.property_account_payable.company_id.id != company_id:
                    prop = self.env['ir.property']
                    rec_dom = [('name', '=', 'property_account_receivable'), ('company_id', '=', company_id)]
                    pay_dom = [('name', '=', 'property_account_payable'), ('company_id', '=', company_id)]
                    res_dom = [('res_id', '=', 'res.partner,%s' % partner_id)]
                    rec_prop = prop.search(rec_dom + res_dom) or prop.search(rec_dom)
                    pay_prop = prop.search(pay_dom + res_dom) or prop.search(pay_dom)
                    rec_account = rec_prop.get_by_record(rec_prop)
                    pay_account = pay_prop.get_by_record(pay_prop)
                    if not rec_account and not pay_account:
                        action = self.env.ref('account.action_account_config')
                        msg = _('Cannot find a chart of accounts for this company, You should configure it. \nPlease go to Account Configuration.')
                        raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))

            if type in ('out_invoice', 'out_refund'):
                account_id = rec_account.id
                payment_term_id = p.property_payment_term.id
            else:
                account_id = pay_account.id
                payment_term_id = p.property_supplier_payment_term.id
            fiscal_position = p.property_account_position.id
            bank_id = p.bank_ids and p.bank_ids[0].id or False
            npwp = p.npwp or False

        result = {'value': {
            'account_id': account_id,
            'payment_term': payment_term_id,
            'fiscal_position': fiscal_position,
            'npwp_no' : npwp,
        }}

        if type in ('in_invoice', 'in_refund'):
            result['value']['partner_bank_id'] = bank_id

        if payment_term != payment_term_id:
            if payment_term_id:
                to_update = self.onchange_payment_term_date_invoice(payment_term_id, date_invoice)
                result['value'].update(to_update.get('value', {}))
            else:
                result['value']['date_due'] = False

        if partner_bank_id != bank_id:
            to_update = self.onchange_partner_bank(bank_id)
            result['value'].update(to_update.get('value', {}))

        return result
    
    @api.multi
    def onchange_partner_bank(self, partner_bank_id=False):
        return {'value': {}}
    
    
    @api.multi
    def invoice_tax_pass(self):
        return self.write({'state': 'tax_pass'})
    
    @api.multi
    def invoice_validated(self):
        return self.write({'state': 'validated'})
    
    @api.multi
    def action_recieve(self):
        if self.warranty and self.warranty_pay == False:
            raise except_orm(_('Warning!'), _('This is PO warranty, need Purchasing confirmation for process this invoice !'))
        return self.write({'state': 'received'})
    
    
    
class account_invoice_line(models.Model):
    _inherit = "account.invoice.line"
    _description = "Invoice Line"
    
    date_fp = fields.Date(string=' Faktur Pajak Date')
    date_bukti_potong = fields.Date(string='Bukti Potong Date')
    fp_no = fields.Char('Faktur Pajak No')
    bp_no = fields.Char('Bukti Potong No')
    fp_bol = fields.Boolean('FP')
    bp_bol = fields.Boolean('Bp')
    
    _defaults = {
        'fp_no' : '010.000.015',
        'bp_no' : '/EPN/07/2014/PPh23',
     }
    

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: