# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 BroadTech IT Solutions.
#    (http://wwww.broadtech-innovations.com)
#    contact@boradtech-innovations.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import http

# class PamaCustomerInvoice(http.Controller):
#     @http.route('/pama_customer_invoice/pama_customer_invoice/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/pama_customer_invoice/pama_customer_invoice/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('pama_customer_invoice.listing', {
#             'root': '/pama_customer_invoice/pama_customer_invoice',
#             'objects': http.request.env['pama_customer_invoice.pama_customer_invoice'].search([]),
#         })

#     @http.route('/pama_customer_invoice/pama_customer_invoice/objects/<model("pama_customer_invoice.pama_customer_invoice"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('pama_customer_invoice.object', {
#             'object': obj
#         })

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: