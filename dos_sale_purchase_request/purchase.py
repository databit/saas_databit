from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp

class purchase_order(osv.osv):
    _inherit = 'purchase.order'
    
    _columns = {
            'dp_status'     : fields.selection([('created','Created'),('used','Used')], 'DP Status'),
            'prepayment_id' : fields.many2one('purchase.request', 'NOI Prepayment'),
    }

purchase_order()