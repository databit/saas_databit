import time
from openerp.report import report_sxw
from openerp.tools.translate import _
from openerp.osv import osv
#from dos_amount2text_idr import amount_to_text_id

from openerp.addons.dos_amount2text_idr import amount_to_text_id
from openerp.addons.report_webkit import webkit_report
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.addons.report_webkit.webkit_report import webkit_report_extender
from openerp import SUPERUSER_ID

class DosWebkitParser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(DosWebkitParser, self).__init__(cr, uid, name, context=context)
        self.line_no = 0
        self.localcontext.update({
#             'time': time,
            'convert':self.convert,
            'invoice_obj':self.invoice_obj,
            'get_approval_user':self.get_approval_user,
        })
     
    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id
    
    def invoice_obj(self, inv_id): 
        if not inv_id:
            raise osv.except_osv(_('Error!'), _('Not invoice Created'))
        invoice = self.pool.get('account.invoice').browse(self.cr,self.uid,[inv_id],context=None)
        return invoice
    
    def get_approval_user(self,job_id):
        if job_id:
            job_obj = self.pool.get('hr.job')
            approval_user = job_obj.browse(self.cr,self.uid,[job_id],context=None)[0].superior_job_id.employee_ids[0].name
        else:
            approval_user = ""
        return approval_user
         
webkit_report.WebKitParser('report.report.payment.request1',
                       'purchase.request', 
                       'addons/dos_sale_purchase_request/report/report_payment_request.mako',
                       parser=DosWebkitParser)