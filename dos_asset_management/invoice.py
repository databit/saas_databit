import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
import calendar
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _



from openerp.osv import fields, osv

class account_invoice(osv.osv):

    _inherit = 'account.invoice'
    
    def action_number(self, cr, uid, ids, *args, **kargs):
    
        result = super(account_invoice, self).action_number(cr, uid, ids, *args, **kargs)
        for inv in self.browse(cr, uid, ids):
            self.pool.get('account.invoice.line').asset_reg(cr, uid, inv.invoice_line)
        return result

    def line_get_convert(self, cr, uid, x, part, date, context=None):
        res = super(account_invoice, self).line_get_convert(cr, uid, x, part, date, context=context)
        res['asset_id'] = x.get('asset_id', False)
        return res

    



class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'
    _columns = {
                'is_asset' : fields.boolean('Asset'),
                }
    
    def asset_reg(self, cr, uid, lines, context=None):
        context = context or {}
        asset_obj = self.pool.get('asset.register')
        for line in lines :
            if line.is_asset:
                vals = {
                    'name': line.name,
                    'origin': line.invoice_id.number or False,
                    'purchase_value': line.price_subtotal,
                    'cip_account_id' : line.account_id.id,
                    #'period_id': line.invoice_id.period_id.id,
                    'partner_id': line.invoice_id.partner_id.id,
                    'company_id': line.invoice_id.company_id.id,
                    'currency_id': line.invoice_id.currency_id.id,
                    'purchase_date' : line.invoice_id.date_invoice,
                    'state':'draft',
                    'type' : 'fixasset',
                }
                #changed_vals = asset_obj.onchange_category_id(cr, uid, [], vals['category_id'], context=context)
                #vals.update(changed_vals['value'])
                asset_id = asset_obj.create(cr, uid, vals, context=context)
#                 if line.asset_category_id.open_asset:
#                     asset_obj.validate(cr, uid, [asset_id], context=context)
        return True
    
    def onchange_asset_category(self, cr, uid, ids, asset_category):
        print "asset_category", asset_category
        if asset_category == False:
            result = {'value': {}}
            return result
        
        asset_categ_obj = self.pool.get('account.asset.category')
        account_asset = asset_categ_obj.browse(cr, uid, [asset_category], context=None)[0].account_asset_id.id or False
        if account_asset:
            result = {'value': {
                    'account_id': account_asset,
                    }
                }
        else:
            result = {'value': {}}
        
        return result
    
account_invoice_line()