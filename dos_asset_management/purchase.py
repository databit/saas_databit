from openerp.osv import osv,fields

class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"
    _columns = {
        'is_asset': fields.boolean('Asset'),
    }

class purchase_order(osv.osv):
    _inherit = "purchase.order"
    
    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        vals = super(purchase_order,self)._prepare_inv_line(cr,uid,account_id,order_line)
        print "ngapdate apa ke gitu disini...."
        vals['is_asset'] = order_line.is_asset
        
        """Collects require data from purchase order line that is used to create invoice line
        for that purchase order line
        :param account_id: Expense account of the product of PO line if any.
        :param browse_record order_line: Purchase order line browse record
        :return: Value for fields of invoice lines.
        :rtype: dict
        """
        
        return {
            'name': order_line.name,
            'account_id': account_id,
            'price_unit': order_line.price_unit or 0.0,
            'discount': order_line.discount or 0.0,
            'quantity': order_line.product_qty,
            'product_id': order_line.product_id.id or False,
            'uos_id': order_line.product_uom.id or False,
            'invoice_line_tax_id': [(6, 0, [x.id for x in order_line.taxes_id])],
            'account_analytic_id': order_line.account_analytic_id.id or False,
            'purchase_line_id': order_line.id,
            
            'is_asset': order_line.is_asset,
        }