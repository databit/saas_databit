import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
import calendar
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _

class account_asset_category(osv.osv):
    _inherit = 'account.asset.category'
    _description = 'Asset category'

    _columns = {
        'code': fields.char('Code', required=True),
        'type': fields.selection([("asset_i","Ingtangible Asset"),("asset","Asset")], 'Type Asset', required=False, select=True),
    }
