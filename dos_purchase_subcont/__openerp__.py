{
    "name"          : "Purchase Subcont",
    "version"       : "1.0",
    "depends"       : ['purchase','dos_stock_account'],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add flow purchase subcontractor
                        * Add line product which will be return & will be consume
                        """,
    "website"       : "https://www.databit.co.id/",
    'category'      : 'ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                       "stock_sequence.xml",
                       "purchase_workflow.xml",
                       "purchase_view.xml",
                       "stock_view.xml",
                       ],
    'installable': True,
    'auto_install': False,
}