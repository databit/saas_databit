# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import pytz
from openerp import SUPERUSER_ID, workflow
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare

    
class stock_picking_type(osv.osv):
    _inherit = "stock.picking.type"
    _description = "The picking type determines the picking view"
    _columns = {
        'subcont': fields.boolean('Subcont'),
    }
    
class purchase_order(osv.osv):
    
    def _get_subcont_picking_ids(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for po_id in ids:
            res[po_id] = []
        query = """
        SELECT picking_id, po.id FROM stock_picking p, stock_move m, purchase_subcont_order_line psol, purchase_finished_subcont_order_line pfsol, purchase_order po
            WHERE po.id in %s and ((po.id = psol.order_id and psol.id = m.purchase_subcont_line_id) or (po.id = pfsol.order_id and pfsol.id = m.purchase_finished_subcont_line_id)) and m.picking_id = p.id
            GROUP BY picking_id, po.id
        """
        cr.execute(query, (tuple(ids), ))
        picks = cr.fetchall()
        for pick_id, po_id in picks:
            res[po_id].append(pick_id)
        #print "_get_subcont_picking_ids",res
        return res
    
    def _count_all(self, cr, uid, ids, field_name, arg, context=None):
        return {
            purchase.id: {
                'shipment_count': len(purchase.picking_ids),
                'invoice_count': len(purchase.invoice_ids),            
                'subcont_shipment_count': len(purchase.subcont_picking_ids),       
            }
            for purchase in self.browse(cr, uid, ids, context=context)
        }

    
    _inherit = "purchase.order"
    _columns = {
        'subcont_picking_ids': fields.function(_get_subcont_picking_ids, method=True, type='one2many', relation='stock.picking', string='Picking List', help="This is the list of receipts that have been generated for this purchase order."),
        'subcont_shipment_count': fields.function(_count_all, type='integer', string='Incoming Shipments', multi=True),
        'shipment_count': fields.function(_count_all, type='integer', string='Incoming Shipments', multi=True),
        'invoice_count': fields.function(_count_all, type='integer', string='Invoices', multi=True),
        'date_planned_return':fields.datetime('Date Estimated Return', required=False, states={'confirmed':[('readonly',True)],
                                  'approved':[('readonly',True)]},
                                 select=True, help="Depicts the date where the Quotation should be validated and converted into a Purchase Order, by default it's the creation date.",
                                 copy=False),
        #'type': fields.selection([('local','Local'),('subcont','Subcont')], 'Type'),
        'purchase_type': fields.selection([('import','Purchase Import'),('local','Purchase Local'),('subcont','Subcont')],'Origin'),
        'picking_subcont_type_id': fields.many2one('stock.picking.type', 'Deliver Subcont To', help="This will determine picking type of incoming shipment", required=False,
                                           states={'confirmed': [('readonly', True)], 'approved': [('readonly', True)], 'done': [('readonly', True)]}),
        'subcont_order_line': fields.one2many('purchase.subcont.order.line', 'order_id', 'Subcont Order Lines',
                                      states={'approved':[('readonly',True)], 'done':[('readonly',True)]}, copy=True),
        'finished_subcont_order_line': fields.one2many('purchase.finished.subcont.order.line', 'order_id', 'Expected Order Lines',
                                      states={'approved':[('readonly',True)], 'done':[('readonly',True)]}, copy=True),
    }
    
    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}
        return context.get('purchase_type', 'local')
    
    def _get_picking_subcont_internal(self, cr, uid, context=None):
        obj_data = self.pool.get('ir.model.data')
        type_obj = self.pool.get('stock.picking.type')
        user_obj = self.pool.get('res.users')
        company_id = user_obj.browse(cr, uid, uid, context=context).company_id.id
        types = type_obj.search(cr, uid, [('code', '=', 'supplier'), ('subcont','=',True), ('warehouse_id.company_id', '=', company_id)], context=context)
        if not types:
            types = type_obj.search(cr, uid, [('code', '=', 'supplier'), ('subcont','=',True), ('warehouse_id', '=', False)], context=context)
            if not types:
                return False
                #raise osv.except_osv(_('Error!'), _("Make sure you have at least an incoming picking type defined"))
        return types[0]
    
    _defaults = {
        'purchase_type': _get_type,
        'picking_subcont_type_id': _get_picking_subcont_internal,
    }
    
    def view_subcont_picking(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        '''
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        dummy, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree'))
        action = self.pool.get('ir.actions.act_window').read(cr, uid, action_id, context=context)

        pick_ids = []
        for po in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in po.subcont_picking_ids]

        #override the context to get rid of the default filtering on picking type
        action['context'] = {}
        #choose the view_mode accordingly
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = pick_ids and pick_ids[0] or False
        return action
    
    def has_stockable_product(self, cr, uid, ids, *args):
        for order in self.browse(cr, uid, ids):
            for order_line in order.order_line:
                if order_line.product_id and order_line.product_id.type in ('product', 'consu'):
                    return True
            for subcont_order_line in order.subcont_order_line:
                if subcont_order_line.product_id and subcont_order_line.product_id.type in ('product', 'consu'):
                    return True
            for finished_subcont_order_line in order.finished_subcont_order_line:
                if finished_subcont_order_line.product_id and finished_subcont_order_line.product_id.type in ('product', 'consu'):
                    return True
        return False
    
    def _prepare_subcont_order_line_move(self, cr, uid, order, order_line, picking_id, group_id, context=None):
        ''' prepare the stock move data from the PO line. This function returns a list of dictionary ready to be used in stock.move's create()'''
        product_uom = self.pool.get('product.uom')
        price_unit = order_line.price_unit
        if context.get('type', False) == 'finished':
            purchase_finished_subcont_line_id = order_line.id
            purchase_subcont_line_id = False
            location_id = order.picking_subcont_type_id.default_location_dest_id and order.picking_subcont_type_id.default_location_dest_id.id
            location_dest_id = order.location_id and order.location_id.id or order.picking_subcont_type_id.default_location_src_id and order.picking_subcont_type_id.default_location_src_id.id
        else:
            purchase_subcont_line_id = order_line.id
            purchase_finished_subcont_line_id = False           
            location_id = order.location_id and order.location_id.id or order.picking_subcont_type_id.default_location_src_id and order.picking_subcont_type_id.default_location_src_id.id
            location_dest_id = order.picking_subcont_type_id.default_location_dest_id and order.picking_subcont_type_id.default_location_dest_id.id
        if order_line.product_uom.id != order_line.product_id.uom_id.id:
            price_unit *= order_line.product_uom.factor / order_line.product_id.uom_id.factor
        if order.currency_id.id != order.company_id.currency_id.id:
            #we don't round the price_unit, as we may want to store the standard price with more digits than allowed by the currency
            price_unit = self.pool.get('res.currency').compute(cr, uid, order.currency_id.id, order.company_id.currency_id.id, price_unit, round=False, context=context)
        res = []
        move_template = {
            'name': order_line.name or '',
            'product_id': order_line.product_id.id,
            'product_uom': order_line.product_uom.id,
            'product_uos': order_line.product_uom.id,
            'date': order.date_order,
            'date_expected': fields.date.date_to_datetime(self, cr, uid, order_line.date_planned, context),
            #'location_id': order.partner_id.property_stock_supplier.id,
            #'location_dest_id': order.location_id.id,
            'location_id': location_id,
            'location_dest_id': location_dest_id,
            'picking_id': picking_id,
            'partner_id': order.dest_address_id.id,
            'move_dest_id': False,
            'state': 'draft',
            'purchase_subcont_line_id': purchase_subcont_line_id,
            'purchase_finished_subcont_line_id': purchase_finished_subcont_line_id,
            'company_id': order.company_id.id,
            'price_unit': price_unit,
            'picking_type_id': order.picking_subcont_type_id.id,
            'group_id': group_id,
            'procurement_id': False,
            'origin': order.name,
            'route_ids': order.picking_type_id.warehouse_id and [(6, 0, [x.id for x in order.picking_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id':order.picking_type_id.warehouse_id.id,
            'invoice_state': 'none',
            #'invoice_state': order.invoice_method == 'picking' and '2binvoiced' or 'none',
        }

        diff_quantity = order_line.product_qty
#         for procurement in order_line.procurement_ids:
#             procurement_qty = product_uom._compute_qty(cr, uid, procurement.product_uom.id, procurement.product_qty, to_uom_id=order_line.product_uom.id)
#             tmp = move_template.copy()
#             tmp.update({
#                 'product_uom_qty': min(procurement_qty, diff_quantity),
#                 'product_uos_qty': min(procurement_qty, diff_quantity),
#                 'move_dest_id': procurement.move_dest_id.id,  #move destination is same as procurement destination
#                 'group_id': procurement.group_id.id or group_id,  #move group is same as group of procurements if it exists, otherwise take another group
#                 'procurement_id': procurement.id,
#                 'invoice_state': procurement.rule_id.invoice_state or (procurement.location_id and procurement.location_id.usage == 'customer' and procurement.invoice_state=='2binvoiced' and '2binvoiced') or (order.invoice_method == 'picking' and '2binvoiced') or 'none', #dropship case takes from sale
#                 'propagate': procurement.rule_id.propagate,
#             })
#             diff_quantity -= min(procurement_qty, diff_quantity)
#             res.append(tmp)
        #if the order line has a bigger quantity than the procurement it was for (manually changed or minimal quantity), then
        #split the future stock move in two because the route followed may be different.
        if float_compare(diff_quantity, 0.0, precision_rounding=order_line.product_uom.rounding) > 0:
            move_template['product_uom_qty'] = diff_quantity
            move_template['product_uos_qty'] = diff_quantity
            res.append(move_template)
        return res
    
    def _create_stock_subcont_moves(self, cr, uid, order, order_lines, picking_id=False, context=None):
        """Creates appropriate stock moves for given order lines, whose can optionally create a
        picking if none is given or no suitable is found, then confirms the moves, makes them
        available, and confirms the pickings.

        If ``picking_id`` is provided, the stock moves will be added to it, otherwise a standard
        incoming picking will be created to wrap the stock moves (default behavior of the stock.move)

        Modules that wish to customize the procurements or partition the stock moves over
        multiple stock pickings may override this method and call ``super()`` with
        different subsets of ``order_lines`` and/or preset ``picking_id`` values.

        :param browse_record order: purchase order to which the order lines belong
        :param list(browse_record) order_lines: purchase order line records for which picking
                                                and moves should be created.
        :param int picking_id: optional ID of a stock picking to which the created stock moves
                               will be added. A new picking will be created if omitted.
        :return: None
        """
        stock_move = self.pool.get('stock.move')
        todo_moves = []
        new_group = self.pool.get("procurement.group").create(cr, uid, {'name': order.name, 'partner_id': order.partner_id.id}, context=context)

        for order_line in order_lines:
            if not order_line.product_id:
                continue

            if order_line.product_id.type in ('product', 'consu'):
                context.update({'type': context.get('type', False)})
                for vals in self._prepare_subcont_order_line_move(cr, uid, order, order_line, picking_id, new_group, context=context):
                    move = stock_move.create(cr, uid, vals, context=context)
                    todo_moves.append(move)

        todo_moves = stock_move.action_confirm(cr, uid, todo_moves)
        stock_move.force_assign(cr, uid, todo_moves)
    
    def action_picking_create(self, cr, uid, ids, context=None):
        #res = super(purchase_order, self).action_picking_create(cr, uid, ids, context=context)
        #print "resaction_picking_create",res
        for order in self.browse(cr, uid, ids):
            picking_vals = {
                'picking_type_id': order.picking_type_id.id,
                'partner_id': order.partner_id.id,
                'date': order.date_order,
                'origin': order.name
            }
            #print "action_picking_create",order.type
            has_stockable = False
            for order_line in order.order_line:
                if order.purchase_type != 'subcont' and order_line.product_id and order_line.product_id.type in ('product', 'consu'):
                    has_stockable = True
            if has_stockable:
                picking_id = self.pool.get('stock.picking').create(cr, uid, picking_vals, context=context)
                self._create_stock_moves(cr, uid, order, order.order_line, picking_id, context=context)
    
    def action_picking_finished_subcont_create(self, cr, uid, ids, context=None):
        context = {}
        for order in self.browse(cr, uid, ids):
            picking_vals = {
                'picking_type_id': order.picking_subcont_type_id.id,
                'partner_id': order.partner_id.id,
                'date': order.date_order,
                'origin': order.name
            }
            context.update({'type': 'finished'})
            has_stockable = False
            for order_line in order.finished_subcont_order_line:
                if order_line.product_id and order_line.product_id.type in ('product', 'consu'):
                    has_stockable = True
            if has_stockable:
                picking_id = self.pool.get('stock.picking').create(cr, uid, picking_vals, context=context)
                self._create_stock_subcont_moves(cr, uid, order, order.finished_subcont_order_line, picking_id, context=context)
        
    def action_picking_subcont_create(self, cr, uid, ids, context=None):
        context = {}
        for order in self.browse(cr, uid, ids):
            picking_vals = {
                'picking_type_id': order.picking_subcont_type_id.id,
                'partner_id': order.partner_id.id,
                'date': order.date_order,
                'origin': order.name
            }
            context.update({'type': 'lent_consumed'})
            has_stockable = False
            for order_line in order.subcont_order_line:
                if order_line.product_id and order_line.product_id.type in ('product', 'consu'):
                    has_stockable = True
            if has_stockable:
                picking_id = self.pool.get('stock.picking').create(cr, uid, picking_vals, context=context)
                self._create_stock_subcont_moves(cr, uid, order, order.subcont_order_line, picking_id, context=context)
                
    def test_moves_done(self, cr, uid, ids, context=None):
        print '''PO is done at the delivery side if all the incoming shipments are done'''
        for purchase in self.browse(cr, uid, ids, context=context):
            for picking in purchase.picking_ids:
                if picking.state != 'done':
                    return False
            #===================================================================
            #wkf for subcont if picking done
            for subcont_picking in purchase.subcont_picking_ids:
                if subcont_picking.state != 'done':
                    return False                         
            #===================================================================
        return True

    def test_moves_except(self, cr, uid, ids, context=None):
        print ''' PO is in exception at the delivery side if one of the picking is canceled
            and the other pickings are completed (done or canceled)
        '''
        at_least_one_canceled = False
        alldoneorcancel = True
        for purchase in self.browse(cr, uid, ids, context=context):
            for picking in purchase.picking_ids:
                if picking.state == 'cancel':
                    at_least_one_canceled = True
                if picking.state not in ['done', 'cancel']:
                    alldoneorcancel = False
            #===================================================================
            #wkf for subcont if picking cancel
            for subcont_picking in purchase.subcont_picking_ids:
                if subcont_picking.state == 'cancel':
                    at_least_one_canceled = True
                if subcont_picking.state not in ['done', 'cancel']:
                    alldoneorcancel = False
            #===================================================================
        return at_least_one_canceled and alldoneorcancel
            
class purchase_finished_subcont_order_line(osv.osv):
    def _amount_line(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        #tax_obj = self.pool.get('account.tax')
        for line in self.browse(cr, uid, ids, context=context):
            #taxes = tax_obj.compute_all(cr, uid, line.taxes_id, line.price_unit, line.product_qty, line.product_id, line.order_id.partner_id)
            price_subtotal = line.price_unit*line.product_qty
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, price_subtotal)
        return res

    def _get_uom_id(self, cr, uid, context=None):
        try:
            proxy = self.pool.get('ir.model.data')
            result = proxy.get_object_reference(cr, uid, 'product', 'product_uom_unit')
            return result[1]
        except Exception, ex:
            return False
        
    _columns = {
        'name': fields.text('Description', required=True),
        'type': fields.selection([('finished','Finished Product'),('lent','To be Lent'),('consu','To be Consumed')], 'Type', required=True),
        'order_id': fields.many2one('purchase.order', 'Order Reference', select=True, required=True, ondelete='cascade'),
        'product_qty': fields.float('Qty', digits_compute=dp.get_precision('Product Unit of Measure'), required=True),
        'date_planned': fields.date('Lead Time', required=True, select=True),
        #'taxes_id': fields.many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes'),
        'product_uom': fields.many2one('product.uom', 'Product Unit of Measure', required=True),
        'product_id': fields.many2one('product.product', 'Product', domain=[('purchase_ok','=',True)], change_default=True),
        'move_ids': fields.one2many('stock.move', 'purchase_finished_subcont_line_id', 'Reservation', readonly=True, ondelete='set null'),
        'price_unit': fields.float('Unit Price', required=True, digits_compute= dp.get_precision('Product Price')),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        #'account_analytic_id':fields.many2one('account.analytic.account', 'Analytic Account',),
        'company_id': fields.related('order_id','company_id',type='many2one',relation='res.company',string='Company', store=True, readonly=True),
        'state': fields.selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), ('done', 'Done'), ('cancel', 'Cancelled')],
                                  'Status', required=True, readonly=True, copy=False,
                                  help=' * The \'Draft\' status is set automatically when purchase order in draft status. \
                                       \n* The \'Confirmed\' status is set automatically as confirm when purchase order in confirm status. \
                                       \n* The \'Done\' status is set automatically when purchase order is set as done. \
                                       \n* The \'Cancelled\' status is set automatically when user cancel purchase order.'),
#         'invoice_lines': fields.many2many('account.invoice.line', 'purchase_order_line_invoice_rel',
#                                           'order_line_id', 'invoice_id', 'Invoice Lines',
#                                           readonly=True, copy=False),
#         'invoiced': fields.boolean('Invoiced', readonly=True, copy=False),
#         'partner_id': fields.related('order_id', 'partner_id', string='Partner', readonly=True, type="many2one", relation="res.partner", store=True),
#         'date_order': fields.related('order_id', 'date_order', string='Order Date', readonly=True, type="datetime"),
#         'procurement_ids': fields.one2many('procurement.order', 'purchase_line_id', string='Associated procurements'),
    }
    _defaults = {
        'product_uom' : _get_uom_id,
        'product_qty': lambda *a: 1.0,
        'state': lambda *args: 'draft',
        'type': lambda *args: 'finished',
        #'invoiced': lambda *a: 0,
    }
    _table = 'purchase_finished_subcont_order_line'
    _name = 'purchase.finished.subcont.order.line'
    _description = 'Purchase Expected Subcont Order Line'
    
    
    def _get_date_planned(self, cr, uid, supplier_info, date_planned_return_str, context=None):
        """Return the datetime value to use as Schedule Date (``date_planned``) for
           PO Lines that correspond to the given product.supplierinfo,
           when ordered at `date_order_str`.

           :param browse_record | False supplier_info: product.supplierinfo, used to
               determine delivery delay (if False, default delay = 0)
           :param str date_order_str: date of order field, as a string in
               DEFAULT_SERVER_DATETIME_FORMAT
           :rtype: datetime
           :return: desired Schedule Date for the PO line
        """
        supplier_delay = int(supplier_info.delay) if supplier_info else 0
        return datetime.strptime(date_planned_return_str, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(days=supplier_delay)
    
    def onchange_product_uom(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        """
        onchange handler of product_uom.
        """
        if context is None:
            context = {}
        if not uom_id:
            return {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        context = dict(context, purchase_uom_check=True)
        return self.onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=date_planned_return, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state=state, context=context)
    
    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        """
        onchange handler of product_id.
        """
        if context is None:
            context = {}

        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        #product_pricelist = self.pool.get('product.pricelist')
        #account_fiscal_position = self.pool.get('account.fiscal.position')
        #account_tax = self.pool.get('account.tax')

        # - check for the presence of partner_id and pricelist_id
        #if not partner_id:
        #    raise osv.except_osv(_('No Partner!'), _('Select a partner in purchase order to choose a product.'))
        #if not pricelist_id:
        #    raise osv.except_osv(_('No Pricelist !'), _('Select a price list in the purchase order form before choosing a product.'))

        # - determine name and notes based on product in partner lang.
        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        #call name_get() with partner in the context to eventually match name and description in the seller_ids field
        if not name or not uom_id:
            # The 'or not uom_id' part of the above condition can be removed in master. See commit message of the rev. introducing this line.
            dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
            if product.description_purchase:
                name += '\n' + product.description_purchase
            res['value'].update({'name': name})

        # - set a domain on product_uom
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}

        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_planned_return:
            date_planned_return = fields.datetime.now()


        supplierinfo = False
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if supplierinfo.product_uom.id != uom_id:
                    res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
                    if qty:
                        res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    qty = min_qty
        dt = self._get_date_planned(cr, uid, supplierinfo, date_planned_return, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        qty = qty or 1.0
        res['value'].update({'date_planned': date_planned or dt})
        if qty:
            res['value'].update({'product_qty': qty})

        price = price_unit
        if price_unit is False or price_unit is None:
            # - determine price_unit and taxes_id
#             if pricelist_id:
#                 date_order_str = datetime.strptime(date_planned_return, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
#                 price = product_pricelist.price_get(cr, uid, [pricelist_id],
#                         product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
#             else:
            price = product.cogs_price

        #taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
        #fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        #taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
        res['value'].update({'price_unit': price})

        return res

    product_id_change = onchange_product_id
    product_uom_change = onchange_product_uom 

    def action_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'confirmed'}, context=context)
        return True
    
class purchase_subcont_order_line(osv.osv):
    def _amount_line(self, cr, uid, ids, prop, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        #tax_obj = self.pool.get('account.tax')
        for line in self.browse(cr, uid, ids, context=context):
            #taxes = tax_obj.compute_all(cr, uid, line.taxes_id, line.price_unit, line.product_qty, line.product_id, line.order_id.partner_id)
            price_subtotal = line.price_unit*line.product_qty
            cur = line.order_id.pricelist_id.currency_id
            res[line.id] = cur_obj.round(cr, uid, cur, price_subtotal)
        return res

    def _get_uom_id(self, cr, uid, context=None):
        try:
            proxy = self.pool.get('ir.model.data')
            result = proxy.get_object_reference(cr, uid, 'product', 'product_uom_unit')
            return result[1]
        except Exception, ex:
            return False
        
    _columns = {
        'name': fields.text('Description', required=True),
        'type': fields.selection([('lent','To be Lent'),('consu','To be Consumed')], 'Type', required=True),
        'order_id': fields.many2one('purchase.order', 'Order Reference', select=True, required=True, ondelete='cascade'),
        'product_qty': fields.float('Qty', digits_compute=dp.get_precision('Product Unit of Measure'), required=True),
        'date_planned': fields.date('Lead Time', required=True, select=True),
        #'taxes_id': fields.many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes'),
        'product_uom': fields.many2one('product.uom', 'Product Unit of Measure', required=True),
        'product_id': fields.many2one('product.product', 'Product', domain=[('purchase_ok','=',True)], change_default=True),
        'move_ids': fields.one2many('stock.move', 'purchase_subcont_line_id', 'Reservation', readonly=True, ondelete='set null'),
        'price_unit': fields.float('Unit Price', required=True, digits_compute= dp.get_precision('Product Price')),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        #'account_analytic_id':fields.many2one('account.analytic.account', 'Analytic Account',),
        'company_id': fields.related('order_id','company_id',type='many2one',relation='res.company',string='Company', store=True, readonly=True),
        'state': fields.selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), ('done', 'Done'), ('cancel', 'Cancelled')],
                                  'Status', required=True, readonly=True, copy=False,
                                  help=' * The \'Draft\' status is set automatically when purchase order in draft status. \
                                       \n* The \'Confirmed\' status is set automatically as confirm when purchase order in confirm status. \
                                       \n* The \'Done\' status is set automatically when purchase order is set as done. \
                                       \n* The \'Cancelled\' status is set automatically when user cancel purchase order.'),
#         'invoice_lines': fields.many2many('account.invoice.line', 'purchase_order_line_invoice_rel',
#                                           'order_line_id', 'invoice_id', 'Invoice Lines',
#                                           readonly=True, copy=False),
#         'invoiced': fields.boolean('Invoiced', readonly=True, copy=False),
#         'partner_id': fields.related('order_id', 'partner_id', string='Partner', readonly=True, type="many2one", relation="res.partner", store=True),
#         'date_order': fields.related('order_id', 'date_order', string='Order Date', readonly=True, type="datetime"),
#         'procurement_ids': fields.one2many('procurement.order', 'purchase_line_id', string='Associated procurements'),
    }
    _defaults = {
        'product_uom' : _get_uom_id,
        'product_qty': lambda *a: 1.0,
        'state': lambda *args: 'draft',
        'type': lambda *args: 'lent',
        #'invoiced': lambda *a: 0,
    }
    _table = 'purchase_subcont_order_line'
    _name = 'purchase.subcont.order.line'
    _description = 'Purchase Subcont Order Line'
    
    
    def _get_date_planned(self, cr, uid, supplier_info, date_planned_return_str, context=None):
        """Return the datetime value to use as Schedule Date (``date_planned``) for
           PO Lines that correspond to the given product.supplierinfo,
           when ordered at `date_order_str`.

           :param browse_record | False supplier_info: product.supplierinfo, used to
               determine delivery delay (if False, default delay = 0)
           :param str date_order_str: date of order field, as a string in
               DEFAULT_SERVER_DATETIME_FORMAT
           :rtype: datetime
           :return: desired Schedule Date for the PO line
        """
        supplier_delay = int(supplier_info.delay) if supplier_info else 0
        return datetime.strptime(date_planned_return_str, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(days=supplier_delay)
    
    def onchange_product_uom(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        """
        onchange handler of product_uom.
        """
        if context is None:
            context = {}
        if not uom_id:
            return {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        context = dict(context, purchase_uom_check=True)
        return self.onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=date_planned_return, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state=state, context=context)
    
    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_planned_return=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        """
        onchange handler of product_id.
        """
        if context is None:
            context = {}

        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        #product_pricelist = self.pool.get('product.pricelist')
        #account_fiscal_position = self.pool.get('account.fiscal.position')
        #account_tax = self.pool.get('account.tax')

        # - check for the presence of partner_id and pricelist_id
        #if not partner_id:
        #    raise osv.except_osv(_('No Partner!'), _('Select a partner in purchase order to choose a product.'))
        #if not pricelist_id:
        #    raise osv.except_osv(_('No Pricelist !'), _('Select a price list in the purchase order form before choosing a product.'))

        # - determine name and notes based on product in partner lang.
        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        #call name_get() with partner in the context to eventually match name and description in the seller_ids field
        if not name or not uom_id:
            # The 'or not uom_id' part of the above condition can be removed in master. See commit message of the rev. introducing this line.
            dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
            if product.description_purchase:
                name += '\n' + product.description_purchase
            res['value'].update({'name': name})

        # - set a domain on product_uom
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}

        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_planned_return:
            date_planned_return = fields.datetime.now()


        supplierinfo = False
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if supplierinfo.product_uom.id != uom_id:
                    res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
                    if qty:
                        res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    qty = min_qty
        dt = self._get_date_planned(cr, uid, supplierinfo, date_planned_return, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        qty = qty or 1.0
        res['value'].update({'date_planned': date_planned or dt})
        if qty:
            res['value'].update({'product_qty': qty})

        price = price_unit
        if price_unit is False or price_unit is None:
            # - determine price_unit and taxes_id
#             if pricelist_id:
#                 date_order_str = datetime.strptime(date_planned_return, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
#                 price = product_pricelist.price_get(cr, uid, [pricelist_id],
#                         product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
#             else:
            price = product.cogs_price

        #taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
        #fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        #taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
        res['value'].update({'price_unit': price})

        return res

    product_id_change = onchange_product_id
    product_uom_change = onchange_product_uom 

    def action_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'confirmed'}, context=context)
        return True