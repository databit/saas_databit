# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _

#----------------------------------------------------------
# Quants
#----------------------------------------------------------

class stock_quant(osv.osv):
    _inherit = "stock.quant"
    def _get_accounting_data_for_valuation(self, cr, uid, move, context=None):
        """
        Return the accounts and journal to use to post Journal Entries for the real-time
        valuation of the quant.

        :param context: context dictionary that can explicitly mention the company to consider via the 'force_company' key
        :returns: journal_id, source account, destination account, valuation account
        :raise: osv.except_osv() is any mandatory account or journal is not defined.
        """
        product_obj = self.pool.get('product.template')
        accounts = product_obj.get_product_accounts(cr, uid, move.product_id.product_tmpl_id.id, context)
        if move.location_id.valuation_out_account_id:
            acc_src = move.location_id.valuation_out_account_id.id
        else:
            acc_src = accounts['stock_account_input']

        if move.location_dest_id.valuation_in_account_id:
            acc_dest = move.location_dest_id.valuation_in_account_id.id
        else:
            acc_dest = accounts['stock_account_output']

        acc_valuation = accounts.get('property_stock_valuation_account_id', False)
        journal_id = accounts['stock_journal']
        return journal_id, acc_src, acc_dest, acc_valuation
    

class stock_move(osv.osv):
    _inherit = 'stock.move'
    _columns = {
        'purchase_subcont_line_id': fields.many2one('purchase.subcont.order.line',
            'Purchase Subcont Order Line', ondelete='set null', select=True,
            readonly=True),
        'purchase_finished_subcont_line_id': fields.many2one('purchase.finished.subcont.order.line',
            'Purchase Finished Subcont Order Line', ondelete='set null', select=True,
            readonly=True),
    }
    
    def write(self, cr, uid, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = super(stock_move, self).write(cr, uid, ids, vals, context=context)
        from openerp import workflow
        if vals.get('state') in ['done', 'cancel']:
            for move in self.browse(cr, uid, ids, context=context):
                if move.purchase_subcont_line_id and move.purchase_subcont_line_id.order_id:
                    order_id = move.purchase_subcont_line_id.order_id.id
                    # update linked purchase order as superuser as the warehouse
                    # user may not have rights to access purchase.order
                    if self.pool.get('purchase.order').test_moves_done(cr, uid, [order_id], context=context):
                        workflow.trg_validate(SUPERUSER_ID, 'purchase.order', order_id, 'picking_done', cr)
                    if self.pool.get('purchase.order').test_moves_except(cr, uid, [order_id], context=context):
                        workflow.trg_validate(SUPERUSER_ID, 'purchase.order', order_id, 'picking_cancel', cr)
                if move.purchase_finished_subcont_line_id and move.purchase_finished_subcont_line_id.order_id:
                    order_id = move.purchase_finished_subcont_line_id.order_id.id
                    # update linked purchase order as superuser as the warehouse
                    # user may not have rights to access purchase.order
                    if self.pool.get('purchase.order').test_moves_done(cr, uid, [order_id], context=context):
                        workflow.trg_validate(SUPERUSER_ID, 'purchase.order', order_id, 'picking_done', cr)
                    if self.pool.get('purchase.order').test_moves_except(cr, uid, [order_id], context=context):
                        workflow.trg_validate(SUPERUSER_ID, 'purchase.order', order_id, 'picking_cancel', cr)
        return res