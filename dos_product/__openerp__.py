{
    "name"          : "Product",
    "version"       : "1.0",
    "depends"       : ['product','account'],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add configuration on product
                        * Product Category (add type Stockable Product, Consumable, Service, Asset)
                        * Product (add type asset)
                        """,
    "website"       : "https://www.databit.co.id/",
    'category'      : 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                       "product_view.xml",
                       ],
    'installable': True,
    'auto_install': False,
}