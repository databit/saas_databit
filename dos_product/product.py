# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import math
import re
import time


from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_compare
#----------------------------------------------------------
# Categories
#----------------------------------------------------------
class product_category(osv.osv):

    _inherit = "product.category"
    _description = "Product Category"
    _columns = {
        'type': fields.selection([('view','View'), ('normal', 'Normal'), ('product', 'Stockable Product'), ('consu', 'Consumable'), ('service', 'Service')], 'Category Type', required=True, help="A category of the view type is a virtual category that can be used as the parent of another category to create a hierarchical structure."),
        'property_account_retur_income_categ': fields.property(
            type='many2one',
            relation='account.account',
            string="Retur Income Account",
            help="This account will be used for invoices to value sales."),
        'property_account_retur_expense_categ': fields.property(
            type='many2one',
            relation='account.account',
            string="Retur Expense Account",
            help="This account will be used for invoices to value expenses."),
    }
    
    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}
        return context.get('type', 'product')
    
    _defaults = {
        'type': _get_type,
    }
    
class product_template(osv.osv):
    _inherit = "product.template"
    _columns = {
        'type': fields.selection([('product', 'Stockable Product'), ('consu', 'Consumable'), ('service', 'Service')], 'Product Type', required=True, help="Consumable: Will not imply stock management for this product. \nStockable product: Will imply stock management for this product."),
        'categ_id': fields.many2one('product.category','Internal Category', required=True, change_default=True, domain="[('type','=',type)]" ,help="Select category for the current product"),
        'property_account_retur_income': fields.property(
            type='many2one',
            relation='account.account',
            string="Retur Income Account",
            help="This account will be used for invoices instead of the default one to value sales for the current product."),
        'property_account_retur_expense': fields.property(
            type='many2one',
            relation='account.account',
            string="Retur Expense Account",
            help="This account will be used for invoices instead of the default one to value expenses for the current product."),
    }
    
    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}
        return context.get('type', 'product')
    
    _defaults = {
        'type': _get_type,
    }