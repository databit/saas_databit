{
    'name'      : 'Email Notification',
    'version'   : '0.1',
    'author'    : 'Databit Solusi Indonesia',
    'category'  : 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    'depends'   : [
                "purchase",
                "stock",
                "dos_purchase_epn",
                "dos_warehouse_request",
    ],
    'description': """
    This module aim to manage the supplier contract and project administration for PT. Energia Prima Nusantara.
    """,
    'website'   : 'http://www.databit.co.id/',
    'data'      : [],
    'tests'     : [],
    "update_xml": [
                   
                    ],
    'installable': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'application': False,
}