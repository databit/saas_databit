import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time

import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
from openerp.tools.safe_eval import safe_eval as eval

import openerp.addons.decimal_precision as dp
from __builtin__ import object

class notification_remainder(osv.osv):
    _name = 'notification.remainder'
    
    _columns = {}
    
    def notification_remainder(self,cr,uid,context=None):
        print "###Email Notification SENT>>>>>>>>>>>>>>"
        
        cr.execute('select approval_user from purchase_requisition where approval_user is not null group by approval_user')
        approval_user_list = map(lambda x: x[0],cr.fetchall())
        email_ids =[]
        if approval_user_list:
            for approval_user in approval_user_list:
                no = 0
                line_mails = ''
                pr_obj = self.pool.get('purchase.requisition')
                pr_search = pr_obj.search(cr,uid,[('approval_user','=',approval_user)])
                message = ''
                
                for pr in pr_obj.browse(cr,uid,pr_search,context=None):
                    no += 1
                    addrs       = 'odoo-pro'
                    db_name     = 'DB_PROD'
                    id_record   =  pr.id
                    object      = 'purchase.requisition'
                    
                    url     = str('http://%s/web?db=%s#id=%s&view_type=form&model=%s' % (addrs, db_name, id_record, object))
                    href    = str('<a href=%s>Open Link</a>')%  url
                    #href    = str('<a href=http://localhost:8069/web?db=EPN#id=14&view_type=form&model=purchase.order>Open Link</a>')
                    print "href>>>>>>>>>>", href
                    
                    line_mail   =   '''<tr>
                                        <td>%s</td>
                                        <td>%s</td>
                                        <td>%s</td>
                                        <td>%s</td>
                                        <td>%s</td>
                                        <td>%s</td>
                                        <td>IDR</td>
                                        <td>%s</td>
                                        <td>%s</td>
                                    </tr>'''% (no,pr.name,
                                               pr.request_date,
                                               pr.request_by.name,
                                               pr.approval_user.name,
                                               pr.summary_name,pr.amount_total,href)
                    
                    line_mails = line_mails +" "+line_mail
                    
                    
                message = '''
                    <table border="1">
                        <tr>
                            <td bgcolor="#CCCCCC">No.</td>
                            <td bgcolor="#CCCCCC">Referensi</td>
                            <td bgcolor="#CCCCCC">Request Date</td>
                            <td bgcolor="#CCCCCC">Request by</td>
                            <td bgcolor="#CCCCCC">Approval Untuk</td>
                            <td bgcolor="#CCCCCC">Description</td>
                            <td bgcolor="#CCCCCC">Currency</td>
                            <td bgcolor="#CCCCCC">Jumlah</td>
                            <td bgcolor="#CCCCCC">URL Link</td>
                        </tr>
                        %s
                    </table>'''% line_mails
                
                vals = { 'state': 'outgoing',
                         'subject': 'Approval PR Remainder - Odoo ERP System EPN',
                         'body_html': message,
                         'email_to': pr.approval_user.partner_id.email,
                         #'email_to': 'aryalemon.mail@gmail.com',
                         'email_from': pr.company_id.email,
                         #'attachment_ids': [(6, 0, attachment_ids)],
                         }
                email_ids.append(self.pool.get('mail.mail').create(cr, uid, vals, context=context))
        if email_ids:
            self.pool.get('mail.mail').send(cr, uid, email_ids, context=context)
        return True
    
notification_remainder()