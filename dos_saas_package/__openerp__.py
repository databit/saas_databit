{
    "name"          : "Databit SaaS Packages",
    "version"       : "1.0",
    "depends"       : [],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to package configuration for saas databit
                        * UKM
                        * SME
                        * ENTERPRICE
                      """,
    "website"       : "https://www.databit.co.id/",
    'category'      : 'DATABIT PACKAGES',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                       "ukm_res_config_view.xml",
                       "sme_res_config_view.xml",
                       "ent_res_config_view.xml",
                       ],
    'installable': True,
    'auto_install': True,
}