# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import datetime
from dateutil.relativedelta import relativedelta

import openerp
from openerp import SUPERUSER_ID
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv

class dos_saas_packages(osv.osv_memory):
    _name = 'dos.saas.packages'
    _inherit = 'res.config.settings'

    _columns = {
        #base
        'module_dos_base': fields.boolean('Base', help='With new object provinsi, kabupaten, kecamatan, etc.'),
        'module_dos_partner': fields.boolean('Partner', help='Added data NPWP, No. Serial Faktur, etc'),
        'module_dos_data': fields.boolean('Data', help='Added data to object provinsi, kabupaten, kecamatan, etc.'),
        #accounting
        'module_dos_accounting': fields.boolean('Accounting Standard (ready to QC)',
            help='With functionality Inverse Multi Currency, Generate Faktur Pajak (Keluaran/Masukan), Purchase Prepayment (DP) with Gain Loss Journal,  Amount to Words (Bahasa),etc.'),
        'module_dos_amount2text_idr': fields.boolean('Amount to Text IDR',
            help='With this module, you will have translator Amount to IDR'),
        'module_dos_rate_pajak': fields.boolean('Tax Rate',
            help='Added rate pajak within currency from goverment policy.'),
        'module_account_financial_report_webkit_xls': fields.boolean('Report XLS',
            help='With this module, you will have report BS, GL, PL, TB to excel'),
        'module_dos_petty_cash': fields.boolean('Petty Cash In and Out (ready to QC)',
            help='With this module, you will have Petty Cash In and Out (ready to QC)'),
        'module_dos_advance_settlement': fields.boolean('Cash Advance & Settlement (ready to QC)',
            help='With this module, you will have Cash Advance & Settlement'),
        'module_account_transfer': fields.boolean('Bank Transfer (ready to QC)',
            help='With this module, you will have Bank Transfer Internal Company'),
        'module_dos_bank_reconciliation': fields.boolean('Bank Reconciliation (ready to QC)',
            help='With this module, you will have Bank Reconciliation'),
        'module_dos_account_payment': fields.boolean('Payment Administration (ready to QC)',
            help='With this module, you will have payment administration with Cash, Non Payment Administration, Transfer, Check, Giro, Credit Card, Debit Card'),
        #asset
        'module_dos_asset_management': fields.boolean('Asset Management (ready to QC)',
            help='With this module, you will have Asset Management'),
        #hr
        'module_dos_hr_indonesia': fields.boolean('Human Resource Management (ready to QC)',
            help='With this module, you will have Human Resource Management'),
        #sale
        'module_dos_sale': fields.boolean('Sales Standard (ready to QC)',
            help='With this module, you will have standard sales Indonesia'),
        'module_dos_multiple_discount': fields.boolean('Multiple Discount Plus',
            help='With this module, you can set multiple discount plus on product which is for Sales Order Line and Customer Invoice Line'),
        #purchase
        'module_dos_account_anglo_saxon_ext': fields.boolean('CoGS Methodology',
            help='With this module, you will have methodology by changing the accounting logic with stock transactions'),
        'module_dos_purchase_subcont': fields.boolean('Purchase Subcontrator',
            help='With this module, you will add flow purchase subcontractor'),
        #stock
        'module_dos_stock': fields.boolean('Warehouse Standard (ready to QC)',
            help='With this module, you will have standard Warehouse Indonesia'),
        #groups
        'group_multi_currency': fields.boolean("Allow Multi Currency",
            implied_group='base.group_multi_currency',
            help="Allows you to apply Allow Multi Currency."),        
    }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
