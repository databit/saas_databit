#!/usr/bin/python
# -*- coding: utf-8 -*-
##############################################################################
#
#   Databit Solusi Indonesia, PT    
#   Copyright (C) 2010-2013 ADSOft (<http://www.databit.co.id>). 
#   All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


# from datetime import datetime
# from datetime import timedelta
from dateutil.relativedelta import relativedelta
import time
import datetime
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp import netsvc, workflow
from openerp import tools

def str_to_datetime(strdate):
    return datetime.datetime.strptime(strdate, tools.DEFAULT_SERVER_DATE_FORMAT)


class cash_advance_type(osv.osv):
    _name = 'cash.advance.type'
    _description = 'Cash Advance Type'
    _columns = {
        'code'          : fields.char("Code", size=12),
        'name'          : fields.char('Name', size=256, required=True),
        'account_id'    : fields.many2one('account.account','Account', required=True),
    }
    
cash_advance_type()

class cash_advance(osv.osv):
    _name = 'cash.advance'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Advance Request and Settlements'
    _order = "date desc, id desc"
    
    def _invoice_state_update(self, cr, uid, ids, field_name, arg, context=None):
       
        invoice_state = ""
        res = {}
        record = {'invoice_state_update' : invoice_state}
        for val in self.browse(cr, uid, ids, context=None):
            if val.invoice_id:
                
                print "------------------+++", val.invoice_id
                
                invoice_state = val.invoice_id.state
                if invoice_state == 'paid':
                    print "111111111111"
                    self.write(cr, uid, val.id, {'state' : 'paid'}, context=None)
                elif invoice_state == 'cancel':
                    print "111111111111"
                    self.write(cr, uid, val.id, {'state' : 'cancel'}, context=None)
                else:
                    print "222222222222"
                    self.write(cr, uid, val.id, {'state' : 'invoiced'}, context=None)
            print ">>>>>>>>>", invoice_state
            res[val.id] = invoice_state
        return res
    
    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}
        return context.get('type', False)
    
    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        if context.get('invoice_id', False):
            company_id = self.pool.get('account.invoice').browse(cr, uid, context['invoice_id'], context=context).company_id.id
            context.update({'company_id': company_id})
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False
    
    
    def _get_journal(self, cr, uid, context=None):
        if context is None: context = {}
        journal_pool = self.pool.get('account.journal')
        invoice_pool = self.pool.get('account.invoice')
        if context.get('invoice_id', False):
            currency_id = invoice_pool.browse(cr, uid, context['invoice_id'], context=context).currency_id.id
            journal_id = journal_pool.search(cr, uid, [('currency', '=', currency_id)], limit=1)
            return journal_id and journal_id[0] or False
        if context.get('journal_id', False):
            return context.get('journal_id')
        if not context.get('journal_id', False) and context.get('search_default_journal_id', False):
            return context.get('search_default_journal_id')

        ttype = context.get('type', 'bank')
        if ttype in ('payment', 'receipt'):
            ttype = 'bank'
        res = journal_pool.search(cr, uid, [('type', '=', ttype)], limit=1)
        return res and res[0] or False

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('cash.advance.line').browse(cr, uid, ids, context=context):
            result[line.advance_id.id] = True
        return result.keys()
    
    def amount_sub(self, cr, uid, ids, context=None):
        print "KKKKKKKK", ids
        ids = self.pool.get('cash.advance.line').browse(cr, uid, ids, context=None)
        res = {}
        cur_obj=self.pool.get('res.currency')
        #for order in self.browse(cr, uid, ids, context=context):
        amount_untaxed  = 0.0
        amount_tax      = 0.0
        amount_total    = 0.0
        
        val = val1 = 0.0
        cur = ids.advance_id.currency_id
        for line in ids:
           val1 += line.amount
           for c in self.pool.get('account.tax').compute_all(cr, uid, line.invoice_line_tax_id, line.amount, 1, False, ids.advance_id.partner_id)['taxes']:
                val += c.get('amount', 0.0)
        amount_untaxed  = cur_obj.round(cr, uid, cur, val)
        amount_tax      = cur_obj.round(cr, uid, cur, val1)
        amount_total    = amount_untaxed + amount_tax
        return amount_total
       
       
    def amount_all(self, cr, uid, ids, field_name, arg, context=None):
        print "LLLLLLLLLLLL"
        res = {}
        cur_obj=self.pool.get('res.currency')
        for order in self.browse(cr, uid, ids, context=context):
            amount_untaxed  = 0.0
            amount_tax      = 0.0
            amount_total    = 0.0
            
            val = val1 = 0.0
            cur = order.currency_id
            for line in order.line_ids:
               val1 += line.amount
               for c in self.pool.get('account.tax').compute_all(cr, uid, line.invoice_line_tax_id, line.amount, 1, False, order.partner_id)['taxes']:
                    val += c.get('amount', 0.0)
            amount_untaxed  = cur_obj.round(cr, uid, cur, val)
            amount_tax      = cur_obj.round(cr, uid, cur, val1)
            amount_total    = amount_untaxed + amount_tax
        return amount_total
       
    def _compute_total_line(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_total': 0.0,
            }
            val = 0.0
            
            val = self.amount_all(cr, uid, order.id, field_name, arg, context)
#             for line in order.line_ids:
#                 print "TAX------------>>", line.invoice_line_tax_id
#                 if line.invoice_line_tax_id:
#                     for tax in 
#                 else:
#                     val += line.amount
            
                
            res[order.id]['amount_total'] = val
            
        return res
    
    def _get_currency_base(self, cr, uid, context=None):
        currency_id = False
        currency_search = self.pool.get('res.currency').search(cr, uid, [('base', '=', True)])
        currency_browse = self.pool.get('res.currency').browse(cr, uid, currency_search)
        
        for cur_id in currency_browse:
            currency_id = cur_id.id
        
        return currency_id
            
    def _get_partner(self, cr, uid, context=None):
        if context is None: context = {}
        return context.get('partner_id', False)
    
     #----untuk lempar data ke supplier invoice---
    
    def _prepare_inv_line(self, cr, uid, order_line, context=None):
        """Collects require data from purchase order line that is used to create invoice line
        for that purchase order line
        :param account_id: Expense account of the product of PO line if any.
        :param browse_record order_line: Purchase order line browse record
        :return: Value for fields of invoice lines.
        :rtype: dict
        """
        
        account = self.pool('cash.advance').browse(cr, uid, uid, context=context).account_advance_id.id
        
        return {
            'name': order_line.name,
            'account_id': account,
            'price_unit': order_line.amount or 0.0,
            #'quantity': order_line.product_qty,
            #'product_id': order_line.product_id.id or False,
            #'uos_id': order_line.product_uom.id or False,
            'invoice_line_tax_id': [(6, 0, [x.id for x in order_line.invoice_line_tax_id])],
            'account_analytic_id': order_line.account_analytic_id.id or False,
            #'advance_id': order_line.id,
        }

    def _prepare_invoice(self, cr, uid, order, line_ids, context=None):
        """Prepare the dict of values to create the new invoice for a
           purchase order. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record order: purchase.order record to invoice
           :param list(int) line_ids: list of invoice line IDs that must be
                                      attached to the invoice
           :return: dict of value to create() the invoice
        """
        journal_ids = self.pool['account.journal'].search(
                            cr, uid, [('type', '=', 'purchase'),
                                      ('company_id', '=', order.company_id.id)],
                            limit=1)
        if not journal_ids:
            raise osv.except_osv(
                _('Error!'),
                _('Define purchase journal for this company: "%s" (id:%d).') % \
                    (order.company_id.name, order.company_id.id))
        return {
            'name': order.name,
             'reference': order.memo,
             'account_id': order.partner_id.property_account_payable.id,
             'type': 'in_invoice',
             'partner_id': order.partner_id.id,
             'currency_id': order.currency_id.id,
             'journal_id': len(journal_ids) and journal_ids[0] or False,
             'invoice_line': [(6, 0, line_ids)],
             'origin': order.name,
#            'fiscal_position': order.fiscal_position.id or False,
#             'payment_term': order.payment_term_id.id or False,
             'company_id': order.company_id.id,
             'advance_id': order.id
        }
        
    def action_create(self, cr, uid, ids, context=None):
        for check in self.browse(cr, uid, ids):
            self.write(cr, uid, ids, {'state':'proforma','name':self.pool.get('ir.sequence').get(cr, uid, 'cash.advance')})
        return True
    
    def action_settlement_create(self, cr, uid, ids, context=None):
        obj_cash_settlement = self.pool.get('cash.settlement')
        for inv in self.browse(cr, uid, ids, context=None):
            req_date = inv.date
            
            lines = []
            lines_history = []
            
            for advance_line in inv.line_ids:
                vals_line={
                            "name": advance_line.name,
                            #"account_id": inv.account_advance_id.id,
                            #"account_id": advance_line.account_id.id,
                            #"account_id": line.account_id.id,
                            "amount": advance_line.amount,
                            "price_unit": advance_line.amount,
                            "product_qty": 1.0
                            #"invoice_line_tax_id": [(6,0,[t.id for t in line.taxes])],
                            #"product_id": line.product_id.id,
                }
                lines.append((0,0,vals_line))
                
                vals_history_line={
                            "name_history": advance_line.name,
                            #"account_id": advance_line.account_id.id,
                            #"account_id": line.account_id.id,
                            "amount_history": advance_line.amount,
                            #"quantity": line.quantity,
                            #"invoice_line_tax_id": [(6,0,[t.id for t in line.taxes])],
                            #"product_id": line.product_id.id,
                }
                lines_history.append((0,0,vals_history_line))
            
            vals={
                    #"employee_id": inv.employee_id.id,
                    "partner_id": inv.partner_id.id,
                    #"journal_id": inv.journal_id.id,
                    "type": 'purchase',
                    "line_dr_ids": lines,
                    "line_history_ids": lines_history,
                    #"account_id": 191,
                    "account_advance_id": inv.account_advance_id.id,
                    "account_id": inv.account_advance_id.id,
                    "memo": inv.memo,
                   
                    "amount":inv.amount_total,
                    "reserved": inv.amount_total,
                    "date_req": req_date,
                    "cash_advance_ref": inv.name,
                    "cash_advance_id": inv.id,
                    "currency_id" : inv.currency_id.id,
                    "name": self.pool.get('ir.sequence').get(cr, uid, 'cash.settlement')
                    #"amount":total,
                    #"account_expense_id":account_journal_debit,
                    #"origin": replen.name,
                    #"state": "approved",
                    #partner.property_account_payable.id
                    #partner.id
            }
        
        #if inv.payment_adm == 'check':
        #    self.check_checking(cr, uid, ids, context)
        #    self.update_check(cr, uid, ids, context)
        cash_settlement = obj_cash_settlement.create(cr, uid, vals)
        self.write(cr, uid, ids, {'settlement_id' : cash_settlement}, context=None)
        return True
        
    def action_invoice_create(self, cr, uid, ids, context=None):
        invoice_obj         =   self.pool.get('account.invoice')
        invoice_line_obj    =   self.pool.get('account.invoice.line')
        
        for val in self.browse(cr, uid, ids, context=context):
            invoice = {
                    'partner_id'    : val.partner_id.id,
                    'journal_id'    : val.journal_id.id,
                    'account_id'    : val.partner_id.property_account_payable.id,
                    'currency_id'   : val.currency_id.id,
                    'reference'     : val.memo,
                    'origin'        : val.name,
                    'type'          : "in_invoice",
                    'advance_id'    : val.id,
                    #'employee_id'   : val.employee_id.id,
            }
            print " XXXXXXXXXXXXXX",invoice
            invoice_id = invoice_obj.create(cr, uid, invoice)
            for line in val.line_ids:
                invoice_line = {
                        'invoice_id'    : invoice_id,
                        'name'          : line.name,
                        'account_id'    : line.advance_id.account_advance_id.id,
                        'quantity'      : 1,
                        'price_unit'    : self.amount_sub(cr, uid, line.id, context),  #line.amount,
                        #'invoice_line_tax_id'      : [(6, 0, [x.id for x in line.invoice_line_tax_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line)
        #self.action_settlement_create(cr, uid, ids, context=None)
        self.write(cr, uid, ids, {'invoice_id' : invoice_id}, context=None)
        return True
    
    def action_invoice_create2(self, cr, uid, ids, context=None):
        """Generates invoice for given ids of purchase orders and links that invoice ID to purchase order.
        :param ids: list of ids of purchase orders.
        :return: ID of created invoice.
        :rtype: int
        """
        context = dict(context or {})
         
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')
  
        res = False
        uid_company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        for order in self.browse(cr, uid, ids, context=context):
            #self.pool.get('cash.advance.line').write(cr, uid, [line.id for line in order.line_ids if line.state != 'cancel'], {'state': 'done'}, context=context)
            context.pop('force_company', None)
            if order.company_id.id != uid_company_id:
                #if the company of the document is different than the current user company, force the company in the context
                #then re-do a browse to read the property fields for the good company.
                 context['force_company'] = order.company_id.id
            order = self.browse(cr, uid, order.id, context=context)
             
            # generate invoice line correspond to PO line and link that to created invoice (inv_id) and PO line
            inv_lines = []
            for po_line in order.line_ids:
                 
                inv_line_data = self._prepare_inv_line(cr, uid, po_line, context=context)
                inv_line_id = inv_line_obj.create(cr, uid, inv_line_data, context=context)
                inv_lines.append(inv_line_id)
                po_line.write({'invoice_lines': [(4, inv_line_id)]})
# 
            # get invoice data and create invoice
            inv_data = self._prepare_invoice(cr, uid, order, inv_lines, context=context)
            inv_id = inv_obj.create(cr, uid, inv_data, context=context)
 
            # compute the invoice
            inv_obj.button_compute(cr, uid, [inv_id], context=context, set_total=True)
 
            # Link this new invoice to related purchase order
            order.write({'invoice_ids': [(4, inv_id)]})
            self.write(cr, uid, ids, {'state': 'invoiced'}, context=context)
            res = inv_id
        return res
    
    def _get_aging_payment(self, cr, uid, ids, name, arg, context=None):
        print "1111111111111111"
        res = {}
        diff_days = 0
        for val in self.browse(cr, uid, ids, context=context):
            print "222222222222222222"
            if val.invoice_id and val.invoice_id.state == 'paid':
                print "33333333333333333333333" 
                request_date = val.date
                payment_date = val.invoice_id.payment_ids[0].date
                
                diff_days = (datetime.datetime.strptime(payment_date, '%Y-%m-%d')-datetime.datetime.strptime(request_date, '%Y-%m-%d')).days
        res[val.id] = diff_days
        return res
    
    _columns = {
        'number': fields.char('Number', size=32, readonly=True, states={'draft': [('readonly', False)]}),
        'name': fields.char('Number', size=32 ,readonly=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date Create', readonly=True, select=True, states={'draft': [('readonly', False)]}, help="Effective date for accounting entries"),
        'partner_id':fields.many2one('res.partner', 'Responsible By', change_default=1, readonly=True, states={'draft': [('readonly', False)]}),
        #'employee_id': fields.many2one("hr.employee", "Responsible", required=True, readonly=True, states={"draft": [("readonly", False)]}),
        'department_id' : fields.many2one("hr.department", "Department", readonly=True, states={'draft': [('readonly', False)]}),
        'memo'      : fields.char("Memo", size=256, required=True ,readonly=True, states={'draft': [('readonly', False)]}),
        'account_advance_id':fields.many2one('account.account', 'Advance Account', required=True, readonly=True, states={"draft": [("readonly", False)]}),
        'user_id'   : fields.many2one('res.users', "Created by", readonly=True, states={"draft": [("readonly", False)]}),
        'date_event_start' : fields.date('Date Start', readonly=True, select=True, states={'draft': [('readonly', False)]}, help="Effective date for accounting entries"),
        'date_event_end'   : fields.date('Date End', readonly=True, select=True, states={'draft': [('readonly', False)]}, help="Effective date for accounting entries"),
        'date_estimate_settle'  : fields.date("Date Settlement Estimation", readonly=True, states={"draft": [("readonly", False)]}),
        'tipe'  : fields.selection ([('draft', 'Draft'),
                                     ('sip', 'SIP'),
                                     ('other', 'Other'),
                                     ('other2', 'Other 2'),
                                     ('other3', 'Other 3'),
                                     ], "Type"),
        'tipe_id' : fields.many2one("cash.advance.type", "Type", readonly=True, states={'draft': [('readonly', False)]}),
        'type': fields.selection([
            ('sale', 'Sale'),
            ('purchase', 'Purchase'),
            ('payment', 'Payment'),
            ('receipt', 'Receipt')
            ], 'Default Type', readonly=True, states={'draft': [('readonly', False)]}),        
        'line_ids': fields.one2many('cash.advance.line', 'advance_id', 'Advance Lines', readonly=True, states={'draft': [('readonly', False)]}),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'account_id': fields.many2one('account.account', 'Account', required=False, readonly=True, states={'draft': [('readonly', False)]}),
        'period_id': fields.many2one('account.period', 'Period', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'narration': fields.text('Notes', readonly=True, states={'draft': [('readonly', False)]}),
        'currency_id': fields.many2one('res.currency', 'Currency', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'company_id': fields.many2one('res.company', 'Company', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('proforma', 'Confirmed'),
            ('approve', 'Approved'),
            ('approve2', 'Head Department'),
            ('invoiced', 'Invoiced'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled')
            ], 'State', readonly=True, size=32,
            help=' * The \'Draft\' state is used when a user is encoding a new and unconfirmed Voucher. \
                        \n* The \'Pro-forma\' when voucher is in Pro-forma state,voucher does not have an voucher number. \
                        \n* The \'Posted\' state is used when user create voucher,a voucher number is generated and voucher entries are created in account \
                        \n* The \'Cancelled\' state is used when user cancel voucher.'),
       
        
        'amount_total': fields.function(_compute_total_line, digits_compute=dp.get_precision('Product Price'), method=True, multi='dc', type='float', string='Total'),
                
        'amount': fields.float('Amount', digits_compute=dp.get_precision('Product Price')),
        
        'reference': fields.char('Ref #', size=64, readonly=True, states={'draft': [('readonly', False)]}, help="Transaction reference number."),
        
        
        'tax_id': fields.many2one('account.tax', 'Tax', readonly=True, states={'draft': [('readonly', False)]}),
        'pre_line': fields.boolean('Previous Payments ?', required=False),
        'date_due': fields.date('Due Date', readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'analytic_id': fields.many2one('account.analytic.account','Analytic Account', readonly=True, states={'draft': [('readonly', False)]}),
        
        #'employee_id': fields.many2one("hr.employee", "Employee", required=True, readonly=True, states={"draft": [("readonly", False)]}),
        'account_advance_id':fields.many2one('account.account', 'Advance Account', required=True, readonly=True, states={"draft": [("readonly", False)]}),
        'invoice_id':fields.many2one('account.invoice', 'Invoice', readonly=True,),
        'invoice_ids': fields.many2many('account.invoice', 'advance_invoice_rel', 'advance_id',
                                        'invoice_id', 'Invoices', copy=False,
                                        help="Invoices generated for a purchase order"),        
        'inv_state_update': fields.function(_invoice_state_update, method=True, type='char', string='Invoice State Update'),
        'settlement_id' : fields.many2one("cash.settlement", "Settlement No", readonly=True,),        
        'aging_payment': fields.function(
            _get_aging_payment,
            type='integer',
            precision=dp.get_precision('Account'),
            string="Aging Payment",
            store=True),
       }
    
    _defaults = {
        'period_id': _get_period,
        'partner_id': _get_partner,
        'journal_id':_get_journal,
        #INI yang Aslinya >>>>>>>>>>>'currency_id': _get_currency,
        'currency_id': _get_currency_base,
        'state': 'draft',
        'name':'/',
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'user_id': lambda s, cr, uid, c: uid,
        'company_id': lambda self,cr,uid,c: self.pool.get('res.company')._company_default_get(cr, uid, 'cash.advance', context=c),
        }
    
    def onchange_type_id(self, cr, uid, ids, type_id, context=None):
        res = {'value':{}}
        x = self.pool.get('cash.advance.type')
        if type_id:
            line = x.browse(cr, uid, type_id, context=context)
            res['value'] = {
                'account_advance_id': line.account_id,                            
            }
        return res

    def settlement(self, cr, uid, ids, context=None):        
        return True
    
    def onchange_event_date(self, cr, uid, ids, date_event_start):
        res = {'value':{}}
        date_event_end  = False
        if date_event_start:
            #date_event_end = datetime.strptime(date_event_start, '%Y-%m-%d')+(datetime.timedelta(days=7)).strftime('%Y-%m-%d')
            date_event_end = datetime.datetime.strftime(str_to_datetime(date_event_start) + datetime.timedelta(days=30), tools.DEFAULT_SERVER_DATE_FORMAT)
        res['value']['date_event_end']      = date_event_end
        return res

    def onchange_event_date_end(self, cr, uid, ids, date_event_end):
        res = {'value':{}}
        date_estimate_settle  = False
        if date_event_end:
            #date_event_end = datetime.strptime(date_event_start, '%Y-%m-%d')+(datetime.timedelta(days=7)).strftime('%Y-%m-%d')
            date_estimate_settle = datetime.datetime.strftime(str_to_datetime(date_event_end) + datetime.timedelta(days=7), tools.DEFAULT_SERVER_DATE_FORMAT)
        res['value']['date_estimate_settle']      = date_estimate_settle
        return res
    
    def cancel_advance(self, cr, uid, ids, context=None):
        for adv in self.browse(cr, uid, ids):
            if adv.invoice_id and adv.invoice_id.state != 'draft':
                raise osv.except_osv(
                _('Error!'),
                _('You Cannot cancel if invoiced and invoice on progress'))
            if adv.invoice_id:
                self.pool.get('account.invoice').unlink(cr, uid, [adv.invoice_id.id])
            self.write(cr, uid, ids, {'state':'cancel'})
        return True
        
    def action_cancel_draft2(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for voucher_id in ids:
            wf_service.trg_create(uid, 'cash.advance', ids, cr)
        self.write(cr, uid, ids, {'state': 'draft'})
        return True
    
    def action_cancel_draft3(self, cr, uid, ids, context=None):
        for adv in self.browse(cr, uid, ids):
            self.write(cr, uid, ids, {'state':'draft'})
        return True
    
    def action_cancel_draft(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {'state':'draft'})
        wf_service = netsvc.LocalService("workflow")
        for inv_id in ids:
            wf_service.trg_delete(uid, 'cash.advance', inv_id, cr)
            wf_service.trg_create(uid, 'cash.advance', inv_id, cr)
            self.write(cr, uid, ids, {'state':'draft'})
        return True

    
cash_advance()


class cash_advance_line(osv.osv):
    _name = 'cash.advance.line'
    _description = 'Voucher Lines'    

    def _compute_balance(self, cr, uid, ids, name, args, context=None):
        currency_pool = self.pool.get('res.currency')
        rs_data = {}
        for line in self.browse(cr, uid, ids, context=context):
            ctx = context.copy()
            ctx.update({'date': line.advance_id.date})
            res = {}
            company_currency = line.advance_id.journal_id.company_id.currency_id.id
            voucher_currency = line.advance_id.currency_id.id
            move_line = line.move_line_id or False
            if not move_line:
                res['amount_original'] = 0.0
                res['amount_unreconciled'] = 0.0
            elif move_line.currency_id:
                res['amount_original'] = currency_pool.compute(cr, uid, move_line.currency_id.id, voucher_currency, move_line.amount_currency, context=ctx)
            elif move_line and move_line.credit > 0:
                res['amount_original'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, move_line.credit, context=ctx)
            else:
                res['amount_original'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, move_line.debit, context=ctx)

            if move_line:
                res['amount_unreconciled'] = currency_pool.compute(cr, uid, move_line.currency_id and move_line.currency_id.id or company_currency, voucher_currency, abs(move_line.amount_residual_currency), context=ctx)
            rs_data[line.id] = res
        return rs_data

    _columns = {
        'advance_id':fields.many2one('cash.advance', 'Advance', required=1, ondelete='cascade'),
        'name':fields.char('Description', size=256),
        'account_id':fields.many2one('account.account','Account', required=False),
        'partner_id':fields.related('advance_id', 'partner_id', type='many2one', relation='res.partner', string='Partner'),
        'untax_amount':fields.float('Untax Amount'),
        'amount':fields.float('Amount', digits_compute=dp.get_precision('Account')),
        'invoice_line_tax_id' : fields.many2many('account.tax','account_adv_line_tax', 'adv_line_id', 'adv_tax_id', string='Taxes', domain=[('parent_id', '=', False)]),
        'type':fields.selection([('dr','Debit'),('cr','Credit')], 'Cr/Dr'),
        'account_analytic_id':  fields.many2one('account.analytic.account', 'Analytic Account'),
        'move_line_id': fields.many2one('account.move.line', 'Journal Item'),
        'date_original': fields.related('move_line_id','date', type='date', relation='account.move.line', string='Date', readonly=1),
        'date_due': fields.related('move_line_id','date_maturity', type='date', relation='account.move.line', string='Due Date', readonly=1),
        'amount_original': fields.function(_compute_balance, method=True, multi='dc', type='float', string='Original Amount', store=True),
        'amount_unreconciled': fields.function(_compute_balance, method=True, multi='dc', type='float', string='Open Balance', store=True),
        'company_id': fields.related('advance_id','company_id', relation='res.company', type='many2one', string='Company', store=True, readonly=True),
    }
    
cash_advance_line()






