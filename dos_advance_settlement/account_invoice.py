#!/usr/bin/python
# -*- coding: utf-8 -*-
##############################################################################
#
#   Databit Solusi Indonesia, PT    
#   Copyright (C) 2010-2013 ADSOft (<http://www.databit.co.id>). 
#   All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow

class account_invoice(osv.osv):
    """ Override account_invoice to add the link to the advance it is related to"""
    _inherit = 'account.invoice'
    _columns = {
        'settlement_id'       : fields.many2one('cash.settlement','Settlement No', readonly=True),
        'advance_id'          : fields.many2one('cash.advance', 'Advance No', readonly=True),
    }