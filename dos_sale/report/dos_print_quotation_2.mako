<html>
	<head>
		<style>
			
			body {
			font-family:helvetica;
			font-size:12;
			}
			.vtop
			{
				vertical-align: top;
			}

			.vbottom
			{
				vertical-align: bottom;
			}
		
			.hright
			{
				text-align: right;
				padding-right: 3px;
			}
			.hleft
			{
				text-align: left;
			}
			.hmid
			{
				text-align: center;
			}
			.content
			{
				font-size: 12px;
			}
			.border_grey
			{
				border: 1px solid lightGrey;
			}
			.border_black
			{
				border: 1px solid black;
			}
			.space
			{
				min-height: 25px;
			}
			.note
			{
				width: 500px;
				padding: 5px;
				float:right;
				min-width: 100px;
				border:1px solid black;
			}
			.padding
			{
				padding: 5px;
			}
			.paddingtop
			{
				padding-top: 5px;
			}
			.paddingright
			{
				padding-right: 10px;
			}
			th
			{
				font-size: 12px;
				border-bottom: 1px solid black;
			}
			.border_bottom_grey
			{
				border-bottom: 1px solid lightGrey;
			}
			.background_color
			{
				background-color: lightGrey
			}
			.border_top
			{
				border-top: 1px solid black;
			}
			.border_bottom
			{
				border-bottom: 1px solid black;
			}
			.border_right
			{
				border-right: 1px solid black;
			}
			.border_top_bottom
			{
				border-top: 1px solid lightGrey;
				border-bottom: 1px solid black;
			}
			.border_left_right
			{
				border-right: 1px solid black;
				border-left: 1px solid black;
			}
			.fright
			{
				float: right; 
			}
			.fleft
			{
				float: left; 
			}
			.font12px
			{
				font-size: 12px;
			}
			.font10px
			{
				font-size: 10px;
			}
			.font14px
			{
				font-size: 14px;
			}
			.font22px
			{
				font-size: 22px;
			}
			.font30px
			{
				font-size: 30px;
			}
			.title 
			{
				font-size: 22px;
				text-align: center;
				padding: 5px;
			}
			.title-table 
			{
				font-size: 12px;
				text-align:center;
				padding-top:20px;
			}
			.title-form 
			{
				font-size: 22px;
				text-align:center;
				padding-top:20px;
				padding-bottom:10px;
			}
			
         	table.one 
         	{border-collapse:collapse;}
			
		</style>
	</head>
	% for o in objects:
	<body onload="subst()">
	
        <table width="100%" class="one font12px" cellpading="3">
        	<tr>
        		<td>
        			<br/>
        		</td>
        	</tr>
        	%if o.state == 'draft'
			<tr>
				<td colspan="2" class="font22px hmid" width="100%"><u>DRAFT SURAT PENAWARAN</u></td>
			</tr>
			%elif o.state == 'quot_approval'
			<tr>
				<td colspan="2" class="font22px hmid" width="100%"><u>DRAFT SURAT PENAWARAN (To Confirm) </u></td>
			</tr>
			%elif o.state == 'quot'
			<tr>
				<td colspan="2" class="font22px hmid" width="100%"><u>SURAT PENAWARAN</u></td>
			</tr>
			%else 
			<tr>
				<td colspan="2" class="font22px hmid" width="100%"><u>SALES ORDER</u></td>
			</tr>
			%endif
			<tr>
				<td colspan="2"class="hmid" width="100%">Nomor : ${o.name}</td>
			</tr>
			<tr>
				<td colspan="2"><br/></td>
				
			</tr>
			<tr>
				<td colspan="2" class="hleft" width="100%">Jakarta, ${time.strftime('%d %b %Y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S'))}</td>
			</tr>
			<tr>
				<td width="11%" class="hleft">Kepada Yth :</td>
			</tr>
			<tr>
				<td></td>
				<td class=""><b>${o.partner_id.name}<b/></td>
			</tr>
			<tr>
				<td></td>
				<td class="">${o.partner_id.street}</td>
				
			</tr>
			<tr>
				<td></td>
				<td class="">${o.partner_id.street2 or ''}</td>
				
			</tr>
			<tr>
				<td></td>
				<td class="">${o.partner_id.state or ''} ${o.partner_id.city or ''} ${o.partner_id.country_id.name or ''}</td>
				
			</tr>
			<tr>
				<td>UP: </td>
				<td class=""><b>${o.contact or ''}</b></td>
				
			</tr>
			<tr>
				<td><br/></td>
				
			</tr>
			<tr>
				<td  colspan="2" class="perjanjian">Dengan Hormat,</td>
			</tr>
			<tr>
				<td><br/></td>
				
			</tr>
			<tr>
				<td   colspan="2" class="perjanjian">Kami dari PT Persada Medika Solusindo selaku Sole Agent
				dan Distributor Alat Kesehatan, dengan ini bermaksud mengajukan surat penawaran harga untuk product sebagai berikut :</td>
			</tr>
			
        </table>
        <br/>
		
		<table width="100%" class="one font10px" cellpading="3">
			<tr>
				<th class="border_bottom hleft font10px" colspan="8">Details</th>
			</tr>
			<tr>
				<th width="3%" rowspan="2" class="h">No.</th>
				<th width="10%" rowspan="2" class="hleft">Produk </th>
				<th width="25%" rowspan="2" class="hleft">Deskripsi</th>
				<th width="15%" rowspan="2" class="hleft">Merk/Tipe/Negara Asal</th>
				<th width="10%" rowspan="2" class="hmid">Qty</th>
				<th width="25%" colspan="3">Harga (Rupiah)</th>
				
			</tr>
			<tr>
				<th width="6%" class="hmid" >Satuan</th>
				<th width="8%" class="hmid" >Discount</th>
				<th width="13%" class="hmid">Jumlah</th>
			</tr>
			
			<% set i=0 %>
			%for line in o.order_line:
			<% set i=i+1 %>
			%if line.main_unit == True
			<tr style="page-break-inside:avoid; page-break-after:always; padding-top:10px">
				<td class="font12px" width="3%"><b>${i}</b></td>
				<td class="font12px" width="10%"><b>${line.product_id.name or ''}</b></td>
				<td class="font12px" width="25%"><b>${line.product_id.description or ''}</b></td>
				<td  width="15%"><b>Merek   : ${line.product_id.product_brand_id.name or ''}</br>
								Model   : ${line.product_id.default_code}</br>
								Origin : ${line.product_id.product_country.name or ''}</b>
				</td>
				<td class="hmid font12px" width="10%"><b>${line.product_uom_qty} ${line.product_uom.name}</b></td>
				<td class="hright font12px" width="7%"><b>${formatLang(line.price_unit) or formatLang('0')}</b></td>
				<td class="hmid font12px" width="8%"><b>${line.discount}</b></td>
				<td  class="hright font12px" width="13%"><b>${formatLang(line.price_subtotal) or formatLang('0')}</b></td>
			</tr>
			%else
			<tr style="page-break-inside:avoid; page-break-after:auto; ">
				<td class="paddingtop" width="3%">${i}</td>
				<td class="paddingtop" width="10%">${line.product_id.name or ''}</td>
				<td class="paddingtop" width="25%">${line.product_id.description or ''}</td>
				<td class="paddingtop" width="15%">Merek   : ${line.product_id.product_brand_id.name or ''}</br>
								Model   : ${line.product_id.default_code}</br>
								Origin : ${line.product_id.product_country.name or ''}
				</td>
				<td class="hmid paddingtop" width="10%">${line.product_uom_qty} ${line.product_uom.name}</td>
				<td class="hright paddingtop" width="7%">${formatLang(line.price_unit) or formatLang('0')}</td>
				<td class="hmid paddingtop" width="8%">${line.discount}</td>
				<td  class="hright paddingtop" width="13%">${formatLang(line.price_subtotal) or formatLang('0')}</td>
			</tr>
			%endif 
			%endfor
			<tr>
				<td colspan="9" class="border_top">
				</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright">Total :</td>
				<td class="hright">${formatLang(o.gross_total)}</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright">Discount :</td>
				<td class="hright"> ${formatLang(o.discount_total)}</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright">Discount Additional :</td>
				<td class="hright"> ${formatLang(o.discount_additional)}</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright">Gross Total :</td>
				<td class="hright border_top">${formatLang(o.amount_untaxed) or formatLang('0')}</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright">PPN 10% :</td>
				<td class="hright">${formatLang(o.amount_tax) or formatLang('0')}</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" class="hright border_top">Net Total :</td>
				<td class="hright border_top">${formatLang(o.amount_total) or formatLang('0')}</td>
			</tr>
			
		</table>
		
		<table class="content" width="100%">
			<tr >
				<td class="hleft"><b>Kondisi penawaran : </b></td> 
			</tr>
			<tr>
				<td class="font11px" style="color: solid black"> ${o.ketentuan|safe or '-'}
				</td>
			</tr>
		</table>
		<table class="content" width="100%" style="page-break-inside:avoid; ">
		<tr >
				<td class="content padding"></br></td>
			</tr>	
			<tr>
				<td class="perjanjian">Demikian Surat Penawaran ini kami buat. Atas perhatian dan kerjasamanya kami ucapkan terima kasih.</td>
			</tr>
			<tr>
				<td class="perjanjian">Hormat Kami,</td>
			</tr>
			<tr>
				<td class="perjanjian"><br/><br/><br/></td>
			</tr>
			<tr>
				<td class="perjanjian">${o.user_id.name}</td>
			</tr>
			
		</table>
		<p class="content">
			Jika penawaran ini disetujui, silahkan isi form dibawah ini dan kembalikan ke ${o.company_id.name or '-'} melalui FAX ${o.company_id.fax or '-'} atau email ke ${o.company_id.email or '-'}.
		</p>
		
		<table style="page-break-inside:avoid; " class="content" border="0px" cellspacing="0px" width="100%">
			<tr >
				<td class="content padding"></br></td>
			</tr>
			<tr >
				<td class="content padding"><b>Form Konfirmasi Order</b></td>
			</tr>
			<tr>
				<td class="content padding">
					Dengan ditandatanganinya form ini, kami menyetujui penawaran yang diajukan <br/><br/>
					<table width="100%" cellpadding="10px">
						<tr>
							<td width="25%">Purchase Order #</td>
							<td width="25%">_____________________________</td>
							<td width="10%">Date</td>
							<td width="40%">______________________________</td>
						</tr>
						<tr>
							<td width="25%">Signed</td>
							<td width="25%">_____________________________</td>
							<td width="10%">Name</td>
							<td width="40%">______________________________</td>
						</tr>
					</table>
				</td>
			</tr>
       </table>
		
		
    % endfor
	
	</body>
</html>