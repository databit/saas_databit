<html>
	<head>
		<style>
			
			body {
			font-family:helvetica;
			font-size:12;
			}
			.vtop
			{
				vertical-align: top;
			}

			.vbottom
			{
				vertical-align: bottom;
			}
		
			.hright
			{
				text-align: right;
				padding-right: 3px;
			}
			.hleft
			{
				text-align: left;
			}
			.hmid
			{
				text-align: center;
			}
			.content
			{
				font-size: 12px;
			}
			.border_grey
			{
				border: 1px solid lightGrey;
			}
			.border_black
			{
				border: 1px solid black;
			}
			.space
			{
				min-height: 25px;
			}
			.note
			{
				width: 500px;
				padding: 5px;
				float:right;
				min-width: 100px;
				border:1px solid black;
			}
			.padding
			{
				padding: 5px;
			}
			.paddingtop
			{
				padding-top: 5px;
			}
			.paddingright
			{
				padding-right: 10px;
			}
			th
			{
				font-size: 12px;
				border-bottom: 1px solid black;
			}
			.border_bottom_grey
			{
				border-bottom: 1px solid lightGrey;
			}
			.background_color
			{
				background-color: lightGrey
			}
			.border_top
			{
				border-top: 1px solid black;
			}
			.border_bottom
			{
				border-bottom: 1px solid black;
			}
			.border_right
			{
				border-right: 1px solid black;
			}
			.border_top_bottom
			{
				border-top: 1px solid black;
				border-bottom: 1px solid black;
			}
			.border_left_right
			{
				border-right: 1px solid black;
				border-left: 1px solid black;
			}
			.fright
			{
				float: right; 
			}
			.fleft
			{
				float: left; 
			}
			.font12px
			{
				font-size: 12px;
			}
			.font10px
			{
				font-size: 10px;
			}
			.font11px
			{
				font-size: 11px;
			}
			.font14px
			{
				font-size: 14px;
			}
			.font16px
			{
				font-size: 16px;
			}
			.font22px
			{
				font-size: 22px;
			}
			.font30px
			{
				font-size: 30px;
			}
			.title 
			{
				font-size: 22px;
				text-align: center;
				padding: 5px;
			}
			.title-table 
			{
				font-size: 12px;
				text-align:center;
				padding-top:20px;
			}
			.title-form 
			{
				font-size: 22px;
				text-align:center;
				padding-top:20px;
				padding-bottom:10px;
			}
			
         	table.one 
         	{border-collapse:collapse;}
			
		</style>
	</head>
	<body>
	% for o in objects:
        <table width="100%" cellpadding="3" border="0" padding-top="100px" class="font12px one">
			<tr>
				<td><br/></td>
				<td><br/></td>
				<td><br/></td>
				<td><br/></td>
				<td><br/></td>
			</tr>
			
			<tr>
				<td valign="top" class="hmid font16px" colspan="5">
					
						${ o.state == 'draft' and 'DRAFT QUOTATION' or o.state == 'quot' and 'QUOTATION' or 'SALE ORDER'}
					<br/>
					<br/>
				</td>
			</tr>
			<tr>
				<td colspan="2" width="20%">Date</td>
				<td style="width:20%">: ${time.strftime('%d %b %Y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S'))}</td>
				<td width="10%"></td>
				<td width="30%"></td>
			</tr>
			<tr>
				<td colspan="2">
					% if o.state == 'draft':	
					Draft Reference No 
					% elif o.state == 'quot':						
					Quotation Reference No
					% else:	
					Sale Order Reference No
					%endif
					<br/>
					<br/>
				</td>
				<td style="width:20%">: ${o.name}</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td width="10%" colspan="2" class="hleft vtop"><b>Billing Address :</b></td>
				<td></td>
				
				<td colspan="2">
				<b>Shipping Address :</b></td>
				
				
			</tr>
			<tr>
				<td></td>
				<td width="30%" colspan="2">
						${o.partner_id and o.partner_id.name or ''}<br/>
						${o.partner_id and o.partner_id.street or ''}<br/>
						${o.partner_id and o.partner_id.city or ''}
						${o.partner_id and o.partner_id.state_id.name or ''}
						${o.partner_id and o.partner_id.zip or ''}<br/>
						${o.partner_id and o.partner_id.country_id.name or ''}<br/><br/>
						<b>Phone No.</b> ${o.partner_id and o.partner_id.phone or ''}<br/>	
						<b>&nbsp;</b><br/>
						</span>
				</td>
				<td></td>
				
				<td>
						<span class="font12px">${o.partner_shipping_id and o.partner_shipping_id.name or ''}<br/>
						${o.partner_shipping_id and o.partner_shipping_id.street or ''}<br/>
						${o.partner_shipping_id and o.partner_shipping_id.city or ''}
						${o.partner_shipping_id and o.partner_shipping_id.state_id.name or ''}
						${o.partner_shipping_id and o.partner_shipping_id.zip or ''}<br/>
						${o.partner_shipping_id and o.partner_shipping_id.country_id.name or ''}<br/><br/>
						<b>Phone No.</b> ${o.partner_shipping_id and o.partner_shipping_id.phone or ''}<br/>	
						<b>&nbsp;</b><br/>
						</span>
				</td>
				
			</tr>
        </table>
		
		<table width="100%" class="one font11px" cellpadding="3">
			<tr>
				<th colspan="6" class="content hleft">Details</th>
			</tr>
			<tr>
				<th width="5%" class="hmid font12px border_top_bottom">No</th>
				<th width="10%" class="hleft font12px border_top_bottom">Item/Product</th>
				<th width="35%" class="hleft font12px border_top_bottom">Description</th>
				<th width="5%" class="hmid font12px border_top_bottom">Qty</th>
				<th width="15%" class="hmid font12px border_top_bottom">Unit Price <br/> (${ o.currency_id.symbol or ''})</th>
				<th width="5%" class="hmid font12px border_top_bottom">Disc.</th>
				<th width="10%" class="hmid font12px border_top_bottom">Tax</th>
				<th width="15%" class="hmid font12px border_top_bottom">Price<br/> (${ o.currency_id.symbol or ''})
				</th>
			</tr>
			<% set i=0%>
			%for line in o.order_line:
			<% set i=i+1%>
			<tr>
				<td>${i}</td>
				<td class="font11px  ">${line.product_id.name}</td>
				<td class="font11px  hleft">${line.name}</td>
				<td class="font11px hmid">${formatLang(line.product_uom_qty)}</td>
				<td class="font11px  hright"> ${formatLang(line.price_unit)}</td>
				<td class="font11px  hright">${ formatLang(line.discount) or 0.0 }</td>
				<td class="font11px  hright">
				%for t in line.tax_id:
					${t.name}
				%endfor
				</td>
				<td class="font11px  hright"> ${formatLang(line.price_subtotal)}</td>
			</tr>
			%endfor
			<tr>
				<td colspan="4">&nbsp;</td>
				<td class="hright" colspan="2"> Total :</td>
				<td class=" border_top hright" colspan="2">${formatLang(o.gross_total)}</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
				<td class=" hright" colspan="2">Discount :</td>
				<td class=" hright" colspan="2">${formatLang(o.discount_total)}</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
				<td class=" hright" colspan="2">Discount Additional:</td>
				<td class=" hright" colspan="2">${formatLang(o.discount_additional)}</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
				<td class="hright" colspan="2">Untaxed :</td>
				<td class=" border_top hright" colspan="2">${formatLang(o.amount_untaxed)}</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
				<td class="hright" colspan="2">Taxes :</td>
				<td class=" hright" colspan="2">${formatLang(o.amount_tax)}</td>
			</tr>
			<tr padding-top="10px">
				<td colspan="4">&nbsp;</td>
				<td class="hright font12px"  colspan="2"><b>Net Total :</b></td>
				<td class=" border_top hright font12px" colspan="2"><b> ${o.company_id.currency_id.name or 'Rp'} ${formatLang(o.amount_total)}</b></td>
			</tr>
		</table>
		<table class="font11px" width="100%">
			<tr>
				<td width="20%">In Words</td>
				<td>: ${o.amount_in_words or ''}</td>
			</tr>
			<tr>
				<td width="20%">Payment Terms</td>
				<td>: ${o.payment_term.name or '30 Days'}</td>
			</tr>
			<tr >
				<td width="20%" class="hleft">Terms and Condition</td>
				<td>:</td> 
			</tr>
			<tr>
				<td></td>
				<td class="font11px" style="color: solid black"> ${o.note|safe or '-'}
				</td>
			</tr>
		</table>
		<table class="content" width="100%" style="page-break-inside:avoid; ">
		<tr >
				<td class="content padding"></br></td>
			</tr>	
			
			<tr>
				<td class="perjanjian">Best Regards,</td>
			</tr>
			<tr>
				<td class="perjanjian" style="height:30px"></td>
			</tr>
			<tr>
				<td class="perjanjian">${o.user_id.name}</td>
			</tr>
			
		</table>
		<p class="content">
			If you elect to accept this quotation, please complete the above and return to  ${o.company_id.name} by FAX or email to  ${o.company_id.email }
		</p>
		
		<table style="page-break-inside:avoid; " class="content" border="0px" cellspacing="0px" width="100%">
			<tr >
				<td class="content padding"></br></td>
			</tr>
			<tr >
				<td class="content padding"><b>Order Confirmation</b></td>
			</tr>
			<tr>
				<td class="content padding">
					Herewith we sign this form, as agreement of this quotation<br/><br/>
					<table width="100%" cellpadding="10px">
						<tr>
							<td width="25%">Purchase Order #</td>
							<td width="25%">_____________________________</td>
							<td width="10%">Date</td>
							<td width="40%">______________________________</td>
						</tr>
						<tr>
							<td width="25%">Signed</td>
							<td width="25%">_____________________________</td>
							<td width="10%">Name</td>
							<td width="40%">______________________________</td>
						</tr>
					</table>
				</td>
			</tr>
       </table>
		%endfor
	</body>
</html>