# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import datetime
from dateutil.relativedelta import relativedelta

import openerp
from openerp import SUPERUSER_ID
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv

class dos_config_settings(osv.osv_memory):
    _name = 'dos.config.settings'
    _inherit = 'res.config.settings'

    _columns = {
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'has_default_company': fields.boolean('Has default company', readonly=True),
        'module_dos_multiple_discount': fields.boolean('Multiple Discount Plus',
            help='With this module, you can set multiple discount plus on product which is for Sales Order Line and Customer Invoice Line'),
        'module_dos_amount2text_idr': fields.boolean('Amount to Text IDR',
            help='With this module, you will have translator Amount to IDR'),
        'module_dos_rate_pajak': fields.boolean('Tax Rate & Inverse Currency',
            help='With this module, you will have tax rate & Inverse currency with base IDR'),
        'module_dos_purchase_subcont': fields.boolean('Purchase Subcontrator',
            help='With this module, you will add flow purchase subcontractor'),
        
    }
    
    def _default_company(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return user.company_id.id
  
    def _default_has_default_company(self, cr, uid, context=None):
        count = self.pool.get('res.company').search_count(cr, uid, [], context=context)
        return bool(count == 1)
    
    _defaults = {
        'company_id': _default_company,
        'has_default_company': _default_has_default_company,
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
