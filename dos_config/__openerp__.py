{
    "name"          : "Databit Master Settings",
    "version"       : "1.0",
    "depends"       : [],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add master configuration for databit
                        * Limit Discount Sales""",
    "website"       : "https://www.databit.co.id/",
    'category'      : 'UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                       "res_config_view.xml",
                       ],
    'installable': True,
    'auto_install': True,
}