from openerp.osv import fields, osv

class stock_picking(osv.osv):
    _inherit = "stock.picking"
    
    def _total_pack(self, cr, uid, ids, name, args, context=None):
        res         = {}
        for pick in self.browse(cr, uid, ids, context=None):
            total_qty   = 0.0
            for line in pick.move_lines:
                total_qty += line.product_uom_qty
            res[pick.id] = total_qty
        return res
    
    _columns = {
        'date_transfered': fields.datetime('Date of Received', help="Date of Received by Customer", states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}, copy=False),
        'carrier_tracking_ref' : fields.char(
                'Tracking Ref.',
                readonly=False,  # updated by wizard
                track_visibility='onchange',),
       'transport_mode' : fields.selection([('internal','Internal Transport'),
                                                    ('external','External Carrier'),
                                                   
                                                    ],
                                                string='Transport Mode',
                                                readonly=False,
                                                states={'draft': [('readonly', False)]}),
        'vehicle_no': fields.char('Vehicle No', size=64),
        'driver_name' : fields.char('Driver Name', size=64),
        'pack_no': fields.float('Qty Pack'),
        'total_item' : fields.function(_total_pack, method=True, type='float', string="Total Qty"),     
    }