{
    "name": "Petty Cash In / Out",
    "version": "1.0",
    "depends": ['account','analytic'],
    "author": "Databit Solusi Indonesia",
    "category": "UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE",
    "description": """
       Petty Cash In / Out       
    """,
    "init_xml": [],
    'data': [ 
            'security/ir.model.access.csv',
            'data/petty_cash_sequence.xml',
            'data/petty_cash_data.xml',
            'data/petty_cash_notif.xml',
            'data/petty_cash_template.xml',
            'petty_cash_view.xml',
    ],
    
    'demo_xml': [],
    'installable': True,
    'active': False,
}