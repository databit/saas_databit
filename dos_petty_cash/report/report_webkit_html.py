import time
from report import report_sxw
from report.render import render
from osv import osv
import pooler
from dos_amount2text_idr import amount_to_text_id

class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(report_webkit_html, self).__init__(cr, uid, name, context=context)
        self.line_no = 0
        self.localcontext.update({
            'time': time,
            'convert':self.convert,
            'nourut': self.no_urut,
            'blank_line':self.blank_line,
            'total_amount':self.total_amount,
            'blank_line':self._blank_line,
        })
    def _blank_line(self, col):
        print "col",len(col)
        a = len(col)
        res = ""
        colu = 15 - a - 1
        print "colu", colu
        for ind in range(colu):
            res += "<tr class='inv_line'><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
        return res
    
    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id
    
    def total_amount(self, ext_lines):
        total = 0.0
        for ext in ext_lines:
            if ext.amount:
                total+=ext.amount
        return total#{'quantity':total}
    
    def no_urut(self, list, value):
        return list.index(value) + 1
    
    def blank_line(self, nlines):
        #row = len(nlines)
        row = nlines
        res = ""
        if row < 8:
            for i in range(8 - row):
                res = res + ('A<br/>')
                #res = res + ('<tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td colspan="2">&nbsp;</td></tr>')
        return res
        
report_sxw.report_sxw('report.webkit.petty.cash',
                       'petty.cash', 
                       'addons/dos_petty_cash/report/report_webkit_html.mako',
                       parser=report_webkit_html,header=False)
