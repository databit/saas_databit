<html>
<head>
    <style type="text/css">
    	body {
		font-family:Arial Black;
	}
        table #head {
        	width:100%;
        }
        .list_table0 {
			font-size:18px;
			font-weight:bold;
			padding-top:10px;
			padding-bottom:10px;
			padding-right:15%;
			width:70%;
			border-collapse:collapse;
        	border-top:1px solid white;
        	border-bottom:1px solid white;
        	border-left:1px solid white;
		}
		.list_table1{
			width:100%;
			font-size:11px;
			border-left:1px solid black;
			border-top:1px solid black;
        	border-bottom:1px solid black;
        	border-right:1px solid black;
		}
		.list_table2 {
			font-size:10px;
		}
		.list_table3 {
			font-size:10px;
		}
		.list_table4 {
			font-size:10px;
			padding-top:5px;
			padding-bottom:5px;
		}
		.cust_info
			{
			font-size:10px;
			font-weight:bold;
			border-top:1px solid black;
			border-bottom:1px solid black;
			border-left:1px solid black;
			border-right:1px solid black;
			padding-top:6px;
			padding-bottom:6px;
			}
		.inv_line td
			{
			border-top:0px;
			border-bottom:0px;
			}
    </style>
</head>
<body>
    %for ext in objects :
    <% setLang(company.partner_id.lang) %>
    
    <hr size="2px" color="white">
    
    <table border="0" width="100%">
    	<tr>
    		<td width="10%" rowspan="2"><span style="text-align:right;font-size:24">${helper.embed_image("jpg",ext.company_id.logo, width=80, height=80)}</span></td>
    		<td style="text-align: left;" align="left" valign="top"><b>${_("BUMI SIAK PUSAKO")}</b></td>
    	</tr>
    	<tr>
    		<td style="font-size:10px" text-align: left;" align="left" valign="top">
    			${ext.company_id.street or ""}<br/>
    			${ext.company_id.street2 or ""}, ${ext.company_id.city or ""}<br/>
    			Phone : ${ext.company_id.phone or ""} Fax : ${ext.company_id.fax or ""}
    		</td>
    	</tr>
    </table>
    
	<table width="100%" class="list_table1" cellpadding="3">
	    <tr valign="top">
	    	<td width="100%" colspan="6" style="font-size:18px" align="center">
	    		<b>
	    			%if ext.type == 'out':
	    				PETTY CASH VOUCHER<br/>
	    			%else:
	    				Cash In<br/>
	    			%endif
	    		</b>
	    	</td>
	    </tr>
	    <tr valign="top" style="font-size:12px">
	    	<td width="18%">Received From</td>
	    	<td width="1%">:</td>
	    	<td width="35%">${company.name or ''|entity}</td>
	    	<td width="14%">Voucher No.</td>
	    	<td width="1%">:</td>
	    	<td width="31%">${ext.number or ''|entity}</td>
	    </tr>
	    <tr valign="top">
	    	<td width="14%">Amount</td>
	    	<td width="1%">:</td>
	    	<td width="35%">${ ext.currency_id.symbol or '' } ${ formatLang(total_amount(ext.petty_line)) or 0|entity}</td>
	    	<td width="14%">Date</td>
	    	<td width="1%">:</td>
	    	
	    	%if ext.date_trans == 'False':
	    		<td width="35%"></td>
	    		
	    	%else:
	    		<td width="35%">${time.strftime('%d %b %Y', time.strptime(ext.date_trans,'%Y-%m-%d')) or ''|entity}</td>
	    	%endif
	    	
	    </tr>
	    <tr valign="top">
	    	<td width="14%"></td>
	    	<td width="1%"></td>
	    	<td width="35%"><i>( ${convert(total_amount(ext.petty_line),ext.currency_id.name) or 0|entity} )</i></td>
	    	<td width="14%">
	    		%if ext.type == 'out':
	    				Cash Out Date
	    			%else:
	    				Cash In Date
	    			%endif
	    	</td>
	    	<td width="1%">:</td>
	    	%if ext.date == 'False':
	    		<td width="35%"></td>
	    		
	    	%else:
	    		<td width="35%">${time.strftime('%d %b %Y', time.strptime(ext.date,'%Y-%m-%d')) or ''|entity}</td>
	    	%endif
	    </tr>
	    <tr>
	    	<td colspan="7">&nbsp;</td>
	    </tr>
    </table>
    <table class="list_table1" border="1" style="border-collapse:collapse;" width="100%" cellpadding="3">
        <tr style="text-align:center;">
        	<td width="12%" valign="top"><strong>${_("Account")}</strong></td>
        	<td width="23%" valign="top"><strong>${_("Department")}</strong></td>
        	<td width="50%" valign="top"><strong>${_("Payment")}</strong></td>
        	<td width="15%" valign="top"><strong>${_("Amount")}</strong></td>
        </tr>
        
        <%
        i = 1
        a = "a"
        %>
        %for line in ext.petty_line:
	        <tr class='inv_line'>
	        	<td style="text-align:center;">${line.account_id.code or ''|entity}</td>
	        	<td style="text-align:center;">${line.department_id.name or ''|entity}</td>
	        	<td style="text-align:left;">${line.name or ''|entity}</td>
	        	<td style="text-align:right;">${line.amount or ''|entity}</td>
	        </tr>
	        <%
	        i=i+1
	        a = a + "a"
	        %>
        %endfor
        %endfor
        ${blank_line(a)}
        <tr style="text-align:right;">
        	<td colspan="3"><b>TOTAL<b></td>
        	<td>${ formatLang(total_amount(ext.petty_line)) or 0|entity}</td>
        </tr>
    </table>
    <br/>
    <table class="list_table1" border="1" style="border-collapse:collapse;" width="100%" cellpadding="3">
    	<tr style="text-align:center;">
    		<td>Prepared by</td>
    		<td>Verified by</td>
    		<td>Approved by</td>
    		<td>Received by</td>
    	</tr>
    	<tr>
    		<td><br/><br/><br/></td>
    		<td><br/><br/><br/></td>
    		<td><br/><br/><br/></td>
    		<td><br/><br/><br/></td>
    	</tr>
    </table>
    
</body>
</html>
