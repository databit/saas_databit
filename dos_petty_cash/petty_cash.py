from mx import DateTime
from lxml import etree

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import time
from openerp import pooler
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
import openerp.addons.decimal_precision as dp
from openerp import netsvc

from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp import api

class petty_cash(osv.osv):
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _name = "petty.cash"
    _description = "Petty Cash"    
    _order = "id desc"
    
    _track = {
                'state': {
                    'dos_petty_cash.petty_cash_cancel': lambda self, cr, uid, obj, ctx=None: obj['state'] == 'draft',
                    'dos_petty_cash.petty_cash_confirmed': lambda self, cr, uid, obj, ctx=None: obj['state'] == 'confirmed',
                    'dos_petty_cash.petty_cash_posted': lambda self, cr, uid, obj, ctx=None: obj['state'] == 'posted',
                },
              }
    
    def log_waiting_manager_finance(self, cr, uid, ids, context=None):
        #product=self.pool.get('product.product').browse(cr, uid, ids)
        total = 0.0
        for val in self.browse(cr, uid, ids, context=None):
            for line in val.petty_line:
                total += line.amount
        desc    = val.name or ""
        date    = str(val.date) or ""
        total   = str(total)
         
        msg= ("Hi, %s need your approval with Amount : %s Date : %s") % (desc, total, date,)
            
        msg_id = self.message_post(cr, uid, ids, body=msg,context=context)
        notif_obj = self.pool.get('mail.notification')
        try:
            notids = notif_obj.create(cr, uid, {'partner_id': 29, 'read': False, 'message_id':msg_id}, context=context)
        except:
            pass
        return True
    
    def get_total(self, cr, uid, ids, name, args, context):
        result = {}
        for petty in self.browse(cr,uid,ids):
            debit=0
            for line in petty.petty_line:
                debit+=line.amount
            result[petty.id] = debit
        return result
    
    _columns = {
        'name' : fields.char('Transaction', 264, required=True, readonly=True, states={'draft':[('readonly',False)]},track_visibility='always'),
        'partner_id': fields.many2one('res.partner', string="Partner", help='The Ordering Partner'),
        'petty_line' : fields.one2many('petty.cash.line', 'petty_cash_id','Lines', required=True, readonly=True, states={'draft':[('readonly',False)]}),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True, readonly=True, states={'draft':[('readonly',False)]}),
        'ref': fields.char('Reference', size=264, readonly=True, states={'draft':[('readonly',False)]}),
        'date': fields.date('Cash Out Date', required=False, readonly=True, states={'draft':[('readonly',False),('required',True)]},track_visibility='always'),
        'date_trans': fields.date('Transaction Date', required=False, readonly=True, states={'draft':[('readonly',False),('required',True)]}),
        'state':fields.selection([('draft','Waiting TM Confirmation'), ('confirmed','Waiting Manager Approve'), ('posted','Posted')], 'State', readonly=True, track_visibility='always'),
        'move_id':fields.many2one('account.move', 'Account Entry',readonly=True, states={'draft':[('readonly',False)]}),
        'move_ids': fields.related('move_id','line_id', type='one2many', relation='account.move.line', string='Journal Items',readonly=True, states={'draft':[('readonly',False)]}),
        'currency_id':fields.many2one('res.currency', 'Currency', required=True, readonly=True, states={'draft':[('readonly',False)]}),
        'force_period': fields.many2one('account.period','Force Period', required=False, readonly=True, states={'draft':[('readonly',False),('required',True)]}),
        'number': fields.char('Number', size=32, readonly=True,),
        'type': fields.selection([('in','Petty Cash In'),('out','Petty Cash Out')],'Type'),
        'company_id': fields.many2one('res.company', 'Company', required=True, change_default=True, readonly=True, states={'draft':[('readonly',False)]}),
        'total':fields.function(get_total,method=True,type='float',string='Total'),
    }
    
    _defaults = {
        'state' : 'draft',
        'company_id': lambda self,cr,uid,c: self.pool.get('res.company')._company_default_get(cr, uid, 'petty.cash', context=c),
     }
    
   
    def posted_action(self, cr, uid, ids, context=None):
        if not context:
            context={}
        account_obj = self.pool.get('account.account')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        seq_obj = self.pool.get('ir.sequence')
        currency_obj = self.pool.get('res.currency')
        for ext_pay in self.browse(cr, uid, ids, context=context):
            #print "Name===============>>",ext_pay.name
            if ext_pay.number:
                name = ext_pay.number
            elif ext_pay.journal_id.sequence_id:
                name = seq_obj.get_id(cr, uid, ext_pay.journal_id.sequence_id.id)
            else:
                raise osv.except_osv(_('Error !'), _('Please define a sequence on the journal !'))
            move = {
                'name': name,
                'journal_id': ext_pay.journal_id.id,
                'narration': ext_pay.ref,
                'date': ext_pay.date,
                'ref': ext_pay.name,
                'period_id': ext_pay.force_period.id
            }
            move_id = move_pool.create(cr, uid, move)
            company_currency = ext_pay.company_id.currency_id.id
            total = 0.0
            for ext_pay_line in ext_pay.petty_line:
                acc = account_obj.browse(cr, uid, ext_pay_line.account_id.id, context=context)
                if ext_pay.type=='out':
                    debit = currency_obj.compute(cr, uid, ext_pay.currency_id.id, company_currency, ext_pay_line.amount, context={'date': ext_pay.date})
                    credit = 0.0
                else:
                    debit = 0.0
                    credit = currency_obj.compute(cr, uid, ext_pay.currency_id.id, company_currency, ext_pay_line.amount, context={'date': ext_pay.date})
                #print "LINE",debit,credit
                #print "NAME",ext_pay_line.name
                move_line = {
                    'name': ext_pay_line.name or '/',
                    'debit': debit,
                    'credit': credit,
                    'account_id': ext_pay_line.account_id.id,
                    'move_id': move_id,
                    'journal_id': ext_pay.journal_id.id,
                    'period_id': ext_pay.force_period.id,
                    'program_budget_id' : ext_pay_line.program_budget_id.id or False, 
                    'analytic_account_id': ext_pay_line.analytic_account_id.id,
                    'partner_id': ext_pay_line.partner_id.id,
                    'currency_id': company_currency <> ext_pay.currency_id.id and ext_pay.currency_id.id or False,
                    'amount_currency': company_currency <> ext_pay.currency_id.id and ext_pay.type=='in' and -ext_pay_line.amount or\
                                       company_currency <> ext_pay.currency_id.id and ext_pay.type=='out' and ext_pay_line.amount or 0.0,
                    'date': ext_pay.date,
                }
                move_line_pool.create(cr, uid, move_line)
                total += ext_pay_line.amount
                
            if ext_pay.type=='out':
                bank_debit = 0.0
                bank_credit = currency_obj.compute(cr, uid, ext_pay.currency_id.id, company_currency, total, context={'date': ext_pay.date})
            else:
                bank_debit = currency_obj.compute(cr, uid, ext_pay.currency_id.id, company_currency, total, context={'date': ext_pay.date})
                bank_credit = 0.0
            move_line_bank = {
                'name': ext_pay_line.petty_cash_id.name or '/',
                'debit': bank_debit,
                'credit': bank_credit,
                'account_id': ext_pay.type=='in' and ext_pay.journal_id.default_debit_account_id.id or ext_pay.journal_id.default_credit_account_id.id,
                'move_id': move_id,
                'journal_id': ext_pay.journal_id.id,
                'period_id': ext_pay.force_period.id,
                #'analytic_account_id': ext_pay_line.analytic_account_id.id,
                'partner_id': ext_pay_line.partner_id.id,
                'currency_id': company_currency <> ext_pay.currency_id.id and ext_pay.currency_id.id or False,
                'amount_currency': company_currency <> ext_pay.currency_id.id and ext_pay.type=='in' and total or\
                                   company_currency <> ext_pay.currency_id.id and ext_pay.type=='out' and -total or 0.0,
                'date': ext_pay.date,
            }
            move_line_pool.create(cr, uid, move_line_bank)
            
            self.auto_balance(cr, uid, ids, move_id, context)
            
            move_pool.post(cr, uid, [move_id], context={})
            self.write(cr, uid, ids, {
                'state': 'posted',
            })
        return self.write(cr, uid, ids, {'state':'posted','move_id':move_id, 'number':name}, context=context)
    
    def auto_balance(self, cr, uid, ids, move_id, context=None):        
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        sum_debit   = 0.0
        sum_credit  = 0.0
        move = move_pool.browse(cr, uid, move_id, context=None)
        for line in move.line_id:
            sum_debit   += line.debit
            sum_credit  += line.credit
        sum_debit   = 0.0
        sum_credit  = 0.0
        move = move_pool.browse(cr, uid, move_id, context=None)
        for line in move.line_id:
            sum_debit   += line.debit
            sum_credit  += line.credit
            
            print line.debit, line.credit        
        diff = sum_debit - sum_credit
        if abs(diff) < 0.1:
            if diff > 0:
                print "EDIT CREDIT"
                move_line_search = move_line_pool.search(cr, uid, [('move_id','=', move_id), ('credit','<>',0.0)])
                move_line_balance = move_line_pool.browse(cr, uid, move_line_search)[0]
                
                balance_update = move_line_balance.credit + abs(diff)
                
                print "move_line_balance.credit + diff", balance_update
                move_line_pool.write(cr, uid, move_line_balance.id, {'credit' : balance_update})
            
            else:
                print "EDIT DEBIT"
                move_line_search = move_line_pool.search(cr, uid, [('move_id','=', move_id), ('debit','<>',0.0)])
                move_line_balance = move_line_pool.browse(cr, uid, move_line_search)[0]
                
                balance_update = move_line_balance.debit + abs(diff)
                print "move_line_balance.credit + diff", balance_update
                
                move_line_pool.write(cr, uid, move_line_balance.id, {'debit' : balance_update,})
        return True
    
    def confirm_transaction(self, cr, uid, ids, context=None):
        #self.log_waiting_manager_finance(cr, uid, ids, context)
        return self.write(cr, uid, ids, {'state':'confirmed'}, context=context)
    
    def setdraft_transaction(self,cr,uid,ids,context={}):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context)
    
    def cancel_transaction(self, cr, uid, ids, context=None):        
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_pool_line = self.pool.get('account.move.line')
        analytic_line_pool = self.pool.get('account.analytic.line')        
        for voucher in self.browse(cr, uid, ids, context=context):
            recs = []
            for line in voucher.move_ids:
                analytic_line_search = analytic_line_pool.search(cr, uid, [('move_id','=',line.id)])
                move_pool_line.write(cr, uid, [line.id], {'analytic_account_id': ''})
                if analytic_line_search:
                    analytic_line_browse = analytic_line_pool.browse(cr, uid, analytic_line_search)
                    for line_analytic in analytic_line_browse:
                        recs.append(line_analytic.id)
            #analytic_line_pool.unlink(cr, uid, recs)
            #move_pool_line.write(cr, uid, [voucher.move_id.id], {'account_analytic_id': ''})
            move_pool.button_cancel(cr, uid, [line.move_id.id])
            move_pool.unlink(cr, uid, [line.move_id.id])
        res = {
            'state':'draft',
        }
        self.write(cr, uid, ids, res)
        return True
    
    
petty_cash()

class petty_cash_line(osv.osv):
    _name = "petty.cash.line"
    _description = "Petty Cash"
    
    _columns = {
        'name' : fields.char('Transaction', 300,required=True),
        'petty_cash_id': fields.many2one('petty.cash', 'Petty Cash'),
        'debit': fields.float('Debit'),
        'credit': fields.float('Credit'),
        'amount': fields.float('Amount'),
        'account_id' : fields.many2one('account.account', 'Account', domain="[('type','!=','view')]", required=True),
        #'department_id': fields.many2one('hr.department','Department',),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account'),
        'partner_id': fields.many2one('res.partner', string="Partner", help='The Ordering Partner'),
        #'program_id': fields.many2one('program.program', 'Program'),
        #'program_budget_id': fields.many2one('program.budget', 'Program Budget'),
        #'amount_currency': fields.float('Amount Currency', help="The amount expressed in an optional other currency if it is a multi-currency entry.", digits_compute=dp.get_precision('Account')),
        #'currency_id': fields.many2one('res.currency', 'Currency', help="The optional other currency if it is a multi-currency entry."),
    }
    def onchange_program_budget(self,cr,uid,ids,program):
        res={}
        if program:
            program=self.pool.get('program.budget').browse(cr,uid,program)
            res['value'] = {'analytic_account_id':program.budget_line_id and program.budget_line_id.analytic_account_id and program.budget_line_id.analytic_account_id.id or False}
        return res
    
    def onchange_analytic(self,cr,uid,ids,analytic_account_id):
        result = {}
        if analytic_account_id:
            analytic_id = self.pool.get('account.analytic.account').browse(cr,uid, [analytic_account_id])[0]
            print "aaa"
            
            department_id = analytic_id.department_id
            
            if department_id:
                department_id = analytic_id.department_id.id
                print "bbb"
            else:
                department_id = False
            print "department_id",department_id
            result['value'] = {
                'department_id': department_id
            }
        return result
    def onchange_debit(self, cr, uid, ids, debit, credit):
        result= {}
        if debit:
            result['value'] = {
                'debit': debit,
                'credit': 0,
            }
        else:
            result['value'] = {
                'debit': 0,
                'credit': 0,
            }
        return result
    
    def onchange_credit(self, cr, uid, ids, debit, credit):
        result= {}
        if credit:
            result['value'] = {
                'debit': 0,
                'credit': credit,
            }
        else:
            result['value'] = {
                'debit': 0,
                'credit': 0,
            }
        return result
        
    
petty_cash_line()

