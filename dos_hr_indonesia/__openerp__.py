{
    "name"          : "Human Resource",
    "version"       : "1.0",
    "depends"       : ['hr','hr_contract','dos_hr_contract',],
    "author"        : "Databit Solusi Indonesia",
    "description"   : """This module is aim to add configuration on Human Resource
                        * 
                        """,
    "website"       : "https://www.databit.co.id/",
    'category'      : 'ENTERPRICE PACKAGE',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],
    "data"          : [
                        "res_partner_view.xml",
                        "hr_employee_number_view.xml",
                        "hr_personal_info_view.xml",
                        "hr_ptkp_view.xml",
                        "hr_education_view.xml",
                         "job_view.xml",
                        "hr_resign_view.xml",
                        #"report/hr_report_view.xml"
                       ],
    'installable': True,
    'auto_install': False,
}