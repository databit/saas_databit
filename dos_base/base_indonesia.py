##############################################################################
#
#    Copyright (C) 2011 ADSOFT OpenERP Partner Indonesia
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import locale
from locale import localeconv
import logging
from openerp import tools
from openerp.osv import fields, osv

_logger = logging.getLogger(__name__)

class lang(osv.osv):
    _inherit = "res.lang"
    _description = "Languages"
    
    def _get_default_date_format(self, cursor, user, context=None):
        return '%d/%m/%Y'
    
    _columns = {
        'date_format':fields.char('Date Format', required=True),
    }
    _defaults = {
        'date_format':_get_default_date_format,
        'grouping': '[3,3,3,3,3]',
        'decimal_point': ',',
        'thousands_sep': '.',
    }
    
class res_country_state(osv.osv):
    _inherit = "res.country.state"
    _columns = {
        'name': fields.char('State/Provinsi', size=64, required=True , translate=True),
        'kabupaten_line': fields.one2many('res.kabupaten', 'state_id', 'Kabupaten'),
    }
res_country_state()

class res_kabupaten(osv.osv):
    _name = "res.kabupaten"
    _description = "List Kabupaten"
    _columns = {
        'name': fields.char('Kabupaten', size=64, required=True , translate=True),
        'state_id': fields.many2one('res.country.state',"Name"),
        'kecamatan_line': fields.one2many('res.kecamatan', 'kabupaten_id', 'Kecamatan'),
    }
res_kabupaten()

class res_kecamatan(osv.osv):
    _name = "res.kecamatan"
    _description = "List Kecamatan"
    _columns = {
        'name': fields.char('Kecamatan', size=64, required=True , translate=True),
        'state_id': fields.many2one('res.country.state',"State/Province"),
        'kabupaten_id': fields.many2one('res.kabupaten',"Kabupaten"),
    }
res_kecamatan()
