# -*- coding: utf-8 -*-
##############################################################################
#
#    account_optimization module for OpenERP, Account Optimizations
#    Copyright (C) 2011 Thamini S.à.R.L (<http://www.thamini.com) Xavier ALT
#
#    This file is a part of account_optimization
#
#    account_optimization is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    account_optimization is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Base Indonesia",
    "version": "1.0",
    "author": "Databit Solusi Indonesia",
    "category": "UKM PACKAGE,SME PACKAGE,ENTERPRICE PACKAGE",
    "description": """
    This modules aim to Base Indonesia, add functional such as:
        * Exchange Rate Currency Method Inverse
        * Visible Currency on Transaction
    """,
    "website" : "http://www.databit.co.id",
    "images" : [],
    'depends': ['base'],
    'demo_xml': [
    ],
    'data': [
        "security/ir.model.access.csv",
        "base_view.xml",
        "res_currency_view.xml",
        "res_currency_data.xml",
        "res_users_view.xml",
        "data/res.country.state.csv",
        "data/res.kabupaten.csv",
        "data/res.kecamatan.csv",
    ],
    'test': [],
    'installable': True,
    #'application': True,
    'auto_install': True,
    'certificate': '',
    "css": [],
}