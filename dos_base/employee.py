import logging

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.modules.module import get_module_resource
from openerp.osv import fields, osv
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)


class hr_employee(osv.osv):
    _inherit = "hr.employee"
    
    def _employee_default_get(self, cr, uid, object=False, field=False, context=None):
        employee_id = False
        """
        Check if the object for this company have a default value
        """
        if not context:
            context = {}
        proxy = self.pool.get('multi_company.default')
        args = [
            ('object_id.model', '=', object),
            ('field_id', '=', field),
        ]

        ids = proxy.search(cr, uid, args, context=context)
        user = self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid, context=context)
        for rule in proxy.browse(cr, uid, ids, context):
            if eval(rule.expression, {'context': context, 'user': user}):
                return rule.company_dest_id.id
        employee_search = self.pool.get('hr.employee').search(cr, uid, [('user_id','=', uid)], context=context)
        for val in self.pool.get('hr.employee').browse(cr, uid, employee_search, context=context):
            employee_id = val.id            
        return employee_id

hr_employee()