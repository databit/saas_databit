from openerp.osv import fields, osv
from openerp.tools.translate import _
import time
from openerp import api, fields as fields2
import datetime

class res_currency_tax_rate(osv.osv):
    _name = "res.currency.tax.rate"
    _description = "Currency Tax Rate"

    _columns = {
        'name': fields.datetime('Date', required=True, select=True),
        'rate': fields.float('Rate', digits=(12,6), help='The rate of the currency to the currency of rate 1'),
        'currency_id': fields.many2one('res.currency', 'Currency', readonly=True),
        #'currency_rate_type_id': fields.many2one('res.currency.rate.type', 'Currency Rate Type', help="Allow you to define your own currency rate types, like 'Average' or 'Year to Date'. Leave empty if you simply want to use the normal 'spot' rate type"),
        'kp_men': fields.char('Keputusan Menteri'),
    }
    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d'),
    }
    _order = "name desc"
    
res_currency_tax_rate()

class res_currency(osv.osv):
    _inherit = 'res.currency'
    _description = 'res currency'
    
    def _current_tax_rate(self, cr, uid, ids, name, arg, context=None):
        return self._get_current_tax_rate(cr, uid, ids, context=context)
    
    def _current_tax_rate_silent(self, cr, uid, ids, name, arg, context=None):
        return self._get_current_tax_rate(cr, uid, ids, raise_on_no_rate=False, context=context)
    
    def _get_current_tax_rate(self, cr, uid, ids, raise_on_no_rate=True, context=None):
        if context is None:
            context = {}
        res = {}
        date = context.get('date') or time.strftime('%Y-%m-%d')
        for id in ids:
            base = self.pool.get('res.currency').browse(cr, uid, [id], context=None)[0].base
            if raise_on_no_rate and base == False:
                #####Check Date Tax Rate####
                cr.execute('SELECT name FROM res_currency_tax_rate '
                           'WHERE currency_id = %s '
                             'AND name <= %s '
                           'ORDER BY name desc LIMIT 1',
                           (id, date))
                if cr.rowcount:
                    rate_tax_date = cr.fetchone()[0]
                    date            = datetime.datetime.strptime(date , '%Y-%m-%d').date()
                    rate_tax_date   = datetime.datetime.strptime(rate_tax_date, '%Y-%m-%d %H:%M:%S').date()
                    delta = (date - rate_tax_date).days
                    if delta > 6:
                        raise osv.except_osv(_('Error!'),_("Your Invalid Tax Rate"))
            cr.execute('SELECT rate FROM res_currency_tax_rate '
                       'WHERE currency_id = %s '
                         'AND name <= %s '
                       'ORDER BY name desc LIMIT 1',
                       (id, date))
            if cr.rowcount:
                res[id] = cr.fetchone()[0]
            elif not raise_on_no_rate:
                res[id] = 0
            else:
                currency = self.browse(cr, uid, id, context=context)
                raise osv.except_osv(_('Error!'),_("No currency rate associated for currency '%s' for the given period" % (currency.name)))
        return res
    
    def _get_conversion_tax_rate(self, cr, uid, from_currency, to_currency, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        from_currency = self.browse(cr, uid, from_currency.id, context=ctx)
        to_currency = self.browse(cr, uid, to_currency.id, context=ctx)

        if from_currency.tax_rate == 0 or to_currency.tax_rate == 0:
            date = context.get('date', time.strftime('%Y-%m-%d'))
            
            if from_currency.tax_rate == 0:
                currency_symbol = from_currency.symbol
            else:
                currency_symbol = to_currency.symbol
            raise osv.except_osv(_('Error'), _('No rate found \n' \
                    'for the currency: %s \n' \
                    'at the date: %s') % (currency_symbol, date))
        #return to_currency.tax_rate/from_currency.tax_rate
        return from_currency.tax_rate/to_currency.tax_rate

    def _compute_tax(self, cr, uid, from_currency, to_currency, from_amount, round=True, context=None):
        if (to_currency.id == from_currency.id):
            if round:
                return self.round(cr, uid, to_currency, from_amount)
            else:
                return from_amount
        else:
            rate = self._get_conversion_tax_rate(cr, uid, from_currency, to_currency, context=context)
            if round:
                return self.round(cr, uid, to_currency, from_amount * rate)
            else:
                return from_amount * rate

    @api.v7
    def compute_tax(self, cr, uid, from_currency_id, to_currency_id, from_amount,
                round=True, context=None):
        context = context or {}
        if not from_currency_id:
            from_currency_id = to_currency_id
        if not to_currency_id:
            to_currency_id = from_currency_id
        xc = self.browse(cr, uid, [from_currency_id,to_currency_id], context=context)
        from_currency = (xc[0].id == from_currency_id and xc[0]) or xc[1]
        to_currency = (xc[0].id == to_currency_id and xc[0]) or xc[1]
        return self._compute_tax(cr, uid, from_currency, to_currency, from_amount, round, context)
    
    @api.v8
    def compute_tax(self, from_amount, to_currency, round=True):
        print "compute_tax--------------------->>", self.name, from_amount, to_currency
        """ Convert `from_amount` from currency `self` to `to_currency`. """
        assert self, "compute from unknown currency"
        assert to_currency, "compute to unknown currency"
        # apply conversion rate
        if self == to_currency:
            to_amount = from_amount
        else:
            to_amount = from_amount * self._get_conversion_tax_rate(self, to_currency)
        # apply rounding
        return to_currency.round(to_amount) if round else to_amount
    
    def computerate(self, cr, uid, from_currency_id, to_currency_id, from_amount, round=True, currency_rate_type_from=False, currency_rate_type_to=False, context=None):
        if not context:
            context = {}
        if not from_currency_id:
            from_currency_id = to_currency_id
        if not to_currency_id:
            to_currency_id = from_currency_id
        xc = self.browse(cr, uid, [from_currency_id,to_currency_id], context=context)
        from_currency = (xc[0].id == from_currency_id and xc[0]) or xc[1]
        to_currency = (xc[0].id == to_currency_id and xc[0]) or xc[1]
        if (to_currency_id == from_currency_id) and (currency_rate_type_from == currency_rate_type_to):
            if round:
                return self.round(cr, uid, to_currency, from_amount)
            else:
                return from_amount
        else:
            context.update({'currency_rate_type_from': currency_rate_type_from, 'currency_rate_type_to': currency_rate_type_to})
            from_currency = self.pool.get('res.currency.tax.rate').search(cr, uid, [('currency_id','=',from_currency_id)], context=context)
            from_currency1 = self.pool.get('res.currency.tax.rate').browse(cr, uid, from_currency, context=context)
            rate = {}
            i = 0;
            for f in from_currency1:
                if context['date'] == f.name:
                    rate[i] = f.rate
                    i+=1
                else:
                    if context['date'] > f.name:
                        rate[i] = f.rate
                        i+=1
    
            if rate.has_key(0):          
                if round:
                    return self.round(cr, uid, to_currency, from_amount / rate[0])
                else:
                    return from_amount / rate[0]
            else:
                raise osv.except_osv(_('Warning!'), _('Please Insert Rate Pajak'))
                
    
    _columns = {
        'rate_tax_ids'  : fields.one2many('res.currency.tax.rate', 'currency_id', string='Res Currency'),
        'tax_rate'      : fields.function(_current_tax_rate, string='Current Tax Rate', digits=(12,6),
            help='The rate of the currency to the currency of rate 1.'),
        'tax_rate_silent': fields.function(_current_tax_rate_silent, string='Current Tax Rate', digits=(12,6),
            help='The rate of the currency to the currency of rate 1 (0 if no rate defined).'),
    }

res_currency()