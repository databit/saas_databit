//  @@@ web_dashboard_tile custom JS @@@
//#############################################################################
//    
//    Copyright (C) 2010-2013 OpenERP s.a. (<http://www.openerp.com>)
//    Copyright (C) 2014 initOS GmbH & Co. KG (<http://initos.com>)
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//#############################################################################

openerp.dos_portal_sale = function (openerp)
{   
    openerp.web.form.widgets.add('test', 'openerp.dos_portal_sale.Mywidget');
    openerp.dos_portal_sale.Mywidget = openerp.web.form.FieldChar.extend(
        {
        template : "test",
        init: function (view, code) {
            this._super(view, code);
            console.log('loading...');
        }
    });
}
