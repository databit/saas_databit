# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Portal Stock',
    'version' : '1.1',
    'summary': 'Portal Stock Management',
    'sequence': 1,
    "author": "Databit Solusi Indonesia",
    'description': """
Stock Portal Management
====================
    """,
    'category' : 'Portal Management',
    'website': 'https://www.databit.co.id/',
    'images' : [],
    'depends' : ['dos_stock','portal_stock'],
    'data': [
        'views/base.xml',
        'views/portal_stock_view.xml',
    ],
    'demo': [
        
    ],
    'qweb': [
        #'static/src/xml/base_custom.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    #'post_init_hook': '_auto_install_l10n',
}