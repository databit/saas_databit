<html>
	<head>
		<style>
			<style>
			.vtop
			{
				vertical-align: top;
			}

			.vbottom
			{
				vertical-align: bottom;
			}
		
			.hright
			{
				text-align: right;
				padding-right: 3px;
			}
			.hleft
			{
				text-align: left;
			}
			.hmid
			{
				text-align: center;
			}
			.content
			{
				font-size: 12px;
			}
			.border_grey
			{
				border: 1px solid lightGrey;
			}
			.border_black
			{
				border: 1px solid black;
			}
			.space
			{
				min-height: 25px;
			}
			.note
			{
				width: 500px;
				padding: 5px;
				float:right;
				min-width: 100px;
				border:1px solid black;
			}
			.padding
			{
				padding: 5px;
			}
			.paddingtop
			{
				padding-top: 10px;
			}
			.paddingright
			{
				padding-right: 10px;
			}
			th
			{
				font-size: 11px;
				border-bottom: 1px solid black;
			}
			.border_bottom_grey
			{
				border-bottom: 1px solid lightGrey;
			}
			.background_color
			{
				background-color: lightGrey
			}
			.border_top
			{
				border-top: 1px solid black;
			}
			.border_top_thin
			{
				border-top: 0.5px solid black
			}
			.border_bottom
			{
				border-bottom: 1px solid black;
			}
			.border_bottom_thin
			{
				
				border-bottom: 0.5px solid black
				
			}
			.border_right
			{
				border-right: 1px solid black;
			}
			.border_top_bottom
			{
				border-top: 1px solid black;
				border-bottom: 1px solid black;
			}
			.border_left_right
			{
				border-right: 1px solid black;
				border-left: 1px solid black;
			}
			.fright
			{
				float: right; 
			}
			.fleft
			{
				float: left; 
			}
			.font12px
			{
				font-size: 12px;
			}
			.font11px
			{
				font-size: 11px;
			}
			.font14px
			{
				font-size: 14px;
			}
			.font16px
			{
				font-size: 16px;
			}
			
			.font22px
			{
				font-size: 22px;
			}
			.font30px
			{
				font-size: 30px;
			}
			.title 
			{
				font-size: 22px;
				text-align: center;
				padding: 5px;
			}
			.title-table 
			{
				font-size: 12px;
				text-align:center;
				padding-top:20px;
			}
			.title-form 
			{
				font-size: 22px;
				text-align:center;
				padding-top:20px;
				padding-bottom:10px;
			}
			
         	table.one 
         	{border-collapse:collapse;}
			
			
		</style>
	</head>
	%for o in objects:
	<body>
		<table width="100%" border="0" class="font11px">
			<tr valign="top">
				
				<td style="width:50%" class="vtop hleft font11px">
					<span class="font11px"><b>Tanggal</b> <i>Date</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ${o.date_invoice or ''}<br/>
					<span class="font11px"><b>Nomor</b> <i>Invoice No</i> :&nbsp;&nbsp; #${o.number or 'Draft'}<br/></span>				
				</td>
				<td style="width:50%" class="hright">
					<span class="font22px"><b></b><br/>
					<span class="font22px"><b>Invoice</b><br/></span>				
				</td>
				
				
			</tr>			
			
	
			<tr>
				<td class="hleft vtop" class="font11px">

				<b>Alamat Tagihan</b> / <i>Billing Address:</i><br/>
				<span class="font11px">${o.partner_id and o.partner_id.name or ''}<br/>
				${o.partner_id and o.partner_id.street or ''}<br/>
				${o.partner_id and o.partner_id.street2 or ''}<br/>
				${o.partner_id and o.partner_id.city or ''}
				${o.partner_id and o.partner_id.state_id.name or ''}
				${o.partner_id and o.partner_id.zip or ''}<br/>
				${o.partner_id and o.partner_id.country_id.name or ''}<br/>
				 ${o.partner_id and o.partner_id.phone or ''}<br/>	
				<b>&nbsp;</b><br/>
				</span>	
				</td>
				
			</tr>
		</table>
		<table cellpadding="2px" width="100%" style="border:1px solid black;" class="font11px">
			<tr >
				<td align="left"><b><span class="font11px">Responsible : ${ o.user_id and o.user_id.name or ''}</span></b></td>
				
				<td align="left"><b><span class="font11px">PO No : ${ o.po_cust or ''}</span></b></td>
				<td align="left"><b><span class="font11px">PR No : ${ o.client_order_ref or ''}</span></b></td>
			</tr>
			
			
		</table>
		
		
		<table cellpadding="3px" width="100%" class="one font11px" >
		<tr>
				<td><br/></td>
			</tr>
			<tr>
				<th colspan="6" class="hleft"><b>Invoice Details</b><br/></th>
			</tr>
			<tr>
				<th width="5%" class="hleft background_color">No</th>
				<th width="40%" class="hleft background_color">Description</th>
				<th width="10%" class="hleft background_color" >Qty</th>
				<th width="15%" class="hleft background_color">Unit Price (${ o.currency_id.symbol or ''})</th>
				<th width="10%" class="background_color">Disc.</th>
				<th width="20%" class="background_color">Price (${ o.currency_id.symbol or ''})</th>
			</tr>
			<% set i=0%>
			%for l in o.invoice_line:
			<% set i=i+1%>
			<tr>
				<td class=" font11px content border_bottom_thin hmid">${i}</td>
				<td class="font11px content border_bottom_thin hleft">${ l.name or ''}</td>
				<td class=" font11px content border_bottom_thin hleft">${ formatLang(l.quantity) or formatLang(0)}</td>
				
				<td class="font11px content border_bottom_thin hright">${ formatLang(l.price_unit) or formatLang(0)}</td>
				<td class="font11px content border_bottom_thin hmid">${ formatLang(l.discount) or 0.0}</td>
				
				<td class="font11px content border_bottom_thin hright">${ formatLang(l.price_subtotal) or formatLang(0)}</td>
			%endfor
			</tr>
			</table>
			<table cellpadding="3px" width="100%" class="one" style="page-break-inside: avoid">
			<tr>
				<th colspan="6" class="hleft border_bottom"></b><br/></th>
			</tr>
			<tr style="width:100%">
				<td width="60%" colspan="3" rowspan="9" border="1px" class="font11px vtop">Comment:<br/>
								<span valign="top"><i>${ o.comment or ''}</i></span><br/><br/>
								

				</td>
			</tr>
			
			<tr class="font11px">
				<td class="hright" colspan="2" style="page-break-after: avoid">Total :</td>
				<td style="page-break-after: avoid" class="hright">${ formatLang(o.gross_total)}</td>
			</tr>
			<tr class="font11px">
				<td style="page-break-after: avoid" class="hright" colspan="2">Discount :</td>
				<td style="page-break-after: avoid" align="right">${ formatLang(o.discount_total)}</td>
			</tr>
			%if o.amount_add_disc > 0
				
			<tr class="font11px">
				<td style="page-break-after: avoid" class="hright" colspan="2"> Additional Discount :</td>
				%if o.add_disc_type == 'fix':
				<td style="page-break-after: avoid" align="right">${formatLang(o.amount_add_disc)}</td>
				%else :
				<td style="page-break-after: avoid" align="right">${formatLang(o.amount_add_disc*o.gross_total)}</td>
				%endif
			</tr>
			%endif
			<tr class="font11px" >
				<td style="page-break-after: avoid" class="hright" colspan="2">Total Untaxed Amount :</td>
				<td style="page-break-after: avoid" class="border_top hright" align="right">${ formatLang(o.amount_untaxed)}</td>
			</tr>
			<tr class="font11px">
				<td style="page-break-after: avoid" class="hright" colspan="2">Tax :</td>
				<td style="page-break-after: avoid" align="right">${ formatLang(o.amount_tax)}</td>
			</tr>
			<tr class="font12px">
				<td style="page-break-after: avoid" class="hright" colspan="2"><b>NET TOTAL :</b></td>
				<td style="page-break-after: avoid" class="border_top hright"><b>${ formatLang(o.amount_total) }</b></td>
			</tr>
			<tr class="font12px">
				<td style="page-break-after: avoid" class="hright" colspan="2"><b>PAID TO DATE :</b></td>
				<td style="page-break-after: avoid" align="right"><b>${ o.state in ('open','proforma','proforma2') and formatLang(o.amount_total-o.residual) or formatLang(0)}</b></td>
			</tr>
			<tr class="font12px ">
				<td style="page-break-after: avoid" class="font12px hright " colspan="2"><b>BALANCE :</b></td>
				<td style="page-break-after: avoid" class="font12px "align="right"><b>${ formatLang(o.residual) or formatLang(0)}</b></td>
			</tr>
			
			
		</table>
		
		<br></br>
		
		<table cellpadding="5px" width="100%" class="font11px one" style="page-break-inside: avoid">
			<tr>
				<td class="border_left_right border_top_bottom " width="20%"><b>Terbilang</b> <i>In Words</i></td>
				<td class="border_left_right border_top_bottom " width="30%">${o.amount_string or ''}</td>
				<td class="border_left_right border_top_bottom " colspan ="2" width="50%"><b>Pembayaran harap di transfer ke</b> <i>Please Make Your Payment To<i/> :</td>
				
			</tr>
			<tr>	
				<td class="border_left_right border_top_bottom " width="20%"><b>Termin Pembayaran</b> <i>Payment Terms</i></td>
				<td class="border_left_right border_top_bottom " width="30%">${o.payment_term.name or '2 Minggu'}</td>
				<td class="border_left_right border_top_bottom " width="15%"><b>Bank Account</b></td>
				<td class="border_left_right border_top_bottom " width="35%">: PT Bank Mandiri Tbk KCP Cicurug-Sukabumi</td>
			</tr>	
				<td class="border_left_right border_top_bottom " width="20%"><b>Due Date</b></td>
				<td class="border_left_right border_top_bottom " width="30%">${time.strftime('%d %B %Y', time.strptime( o.date_due,'%Y-%m-%d'))}</td>
				<td class="border_left_right border_top_bottom "><b>Account No</b></td>
				<td class="border_left_right border_top_bottom "><b>132-00-1342604-5</b></td>
			</tr>
			<tr>
				<td class="border_left_right border_top_bottom " colspan="2"></td>
				<td class="border_left_right border_top_bottom ">Account Name</td>
				<td class="border_left_right border_top_bottom ">CV Benda Karya Prima</td>
			</tr>
			
			
			
		</table>
		<table width="100%" class="font12px">
		<tr>
			<td><br/></td>
		</tr>
			<tr>
				<td>Validate By</td>
			</tr>
		<tr>
			<td><br/><br/><br/></td>
		</tr>
		<tr>
		<td>${ o.user_id and o.user_id.name or ''}</td>
		</tr>
		<tr>
		<td>Direktur</td>
		</tr>
		</table>	
		
	</body>
	%endfor
	
</html>