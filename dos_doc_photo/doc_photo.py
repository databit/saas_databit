# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO
    
import math
import re
import time
#from _common import ceiling


from openerp import api, tools, SUPERUSER_ID
from openerp.osv import osv, fields, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
import psycopg2

from . import image

from PIL import Image
from PIL.ExifTags import TAGS


class photo(osv.osv):
    _name ="photo"
    _description = "Photo Documents"
    
   
    def get_metadata(self, cr, uid, ids, name, context=None):
        res={}
        
        for obj in self.browse(cr, uid, ids, context=context):
            pic = obj.image_medium256
           
            image = StringIO.StringIO(pic.decode(encoding='base64'))
            print ">>>>>>>>>>>>>>>>>>>>>>>>",image
            i = Image.open(image)
            info = i._getexif()
            ret = {}
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", info
            for tag, value in info.items():
                    decoded = TAGS.get(tag, tag)
                    ret[decoded] = value
            
                    
            self.write(cr,uid,ids,{'exif': ret,
                         
                         })
        return res
    
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = image.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': image.image_resize_image_big(value)}, context=context)
    
    
    
    _columns = {
                'name'  : fields.char('Name'),
                'date'  : fields.datetime('Write Date'),
                'responsible' : fields.many2one('res.users','Responsible'),
                
                'image' : fields.binary('Image'),
                'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized photo", type="binary", multi="_get_image",
            store = {
                'photo': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized logo of the brand. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved. "\
                 "Use this field in form views or some kanban views."),
                
                
                'image_medium256': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized photo", type="binary", multi="_get_image",
            store = {
                'photo': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized logo of the brand. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved. "\
                 "Use this field in form views or some kanban views."),
                
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Smal-sized photo", type="binary", multi="_get_image",
            store = {
                'photo': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized photo of the brand. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
                
                'number'    : fields.char('Doc. Number'),
                'description'    : fields.char('Description', size=512),
                'category_id'   : fields.many2one('photo.category','Category'),
                'tag_ids' :fields.many2many('photo.tag', 'photo_tag_rel', 'photo_tag_id','tag_id', 'Tags', copy=False),
                'exif'   : fields.text('Exif'),
                }
    _defaults = {
                 'date' : fields.date.context_today,
                 'responsible' : lambda obj, cr, uid, context: uid,
                 }

#----------------------------------------------------------
# Categories
#----------------------------------------------------------
class photo_category(osv.osv):

#     @api.multi
#     def name_get(self):
#         def get_names(cat):
#             """ Return the list [cat.name, cat.parent_id.name, ...] """
#             res = []
#             while cat:
#                 res.append(cat.name)
#                 cat = cat.parent_id
#             return res
# 
#         return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]
# 
#     def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=100):
#         if not args:
#             args = []
#         if not context:
#             context = {}
#         if name:
#             # Be sure name_search is symetric to name_get
#             categories = name.split(' / ')
#             parents = list(categories)
#             child = parents.pop()
#             domain = [('name', operator, child)]
#             if parents:
#                 names_ids = self.name_search(cr, uid, ' / '.join(parents), args=args, operator='ilike', context=context, limit=limit)
#                 category_ids = [name_id[0] for name_id in names_ids]
#                 if operator in expression.NEGATIVE_TERM_OPERATORS:
#                     category_ids = self.search(cr, uid, [('id', 'not in', category_ids)])
#                     domain = expression.OR([[('parent_id', 'in', category_ids)], domain])
#                 else:
#                     domain = expression.AND([[('parent_id', 'in', category_ids)], domain])
#                 for i in range(1, len(categories)):
#                     domain = [[('name', operator, ' / '.join(categories[-1 - i:]))], domain]
#                     if operator in expression.NEGATIVE_TERM_OPERATORS:
#                         domain = expression.AND(domain)
#                     else:
#                         domain = expression.OR(domain)
#             ids = self.search(cr, uid, expression.AND([domain, args]), limit=limit, context=context)
#         else:
#             ids = self.search(cr, uid, args, limit=limit, context=context)
#         return self.name_get(cr, uid, ids, context)
# 
#     def _name_get_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
#         res = self.name_get(cr, uid, ids, context=context)
#         return dict(res)

    _name = "photo.category"
    _description = "Photo Category"
    _columns = {
        'name': fields.char('Name', required=True, translate=True, select=True),
        #'complete_name': fields.function(_name_get_fnc, type="char", string='Name'),
        'parent_id': fields.many2one('photo.category','Parent Category', select=True, ondelete='cascade'),
        'child_id': fields.one2many('photo.category', 'parent_id', string='Child Categories'),
        'sequence': fields.integer('Sequence', select=True, help="Gives the sequence order when displaying a list of product categories."),
        'type': fields.selection([('view','View'), ('normal','Normal')], 'Category Type', help="A category of the view type is a virtual category that can be used as the parent of another category to create a hierarchical structure."),
        'parent_left': fields.integer('Left Parent', select=1),
        'parent_right': fields.integer('Right Parent', select=1),
    }


    _defaults = {
        'type' : 'normal',
    }

    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'sequence, name'
    _order = 'parent_left'

    _constraints = [
        (osv.osv._check_recursion, 'Error ! You cannot create recursive categories.', ['parent_id'])
    ]
    
class photo_tag(osv.Model):
    _name = 'photo.tag'
    _columns = {
        'name': fields.char('Name', required=True, translate=True),
    }
    
    